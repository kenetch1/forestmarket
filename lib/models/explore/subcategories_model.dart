import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/entities/explore/subcategories_entity.dart';

@immutable
class SubCategories {
  final String id;
  final String name;
  final String code;

  const SubCategories({String? name, String? code, String? id})
      : name = name ?? '',
        code = code ?? '',
        id = id ?? '';

  SubCategories copyWith(
      {String? name,
      String? code,
      String? description,
      String? discount,
      String? id}) {
    return SubCategories(
        name: name ?? this.name, code: code ?? this.code, id: id ?? this.id);
  }

  @override
  int get hashCode => name.hashCode ^ code.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SubCategories &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code &&
          id == other.id;

  @override
  String toString() {
    return 'SubCategories { complete: $name, code: $code, id: $id }';
  }

  SubCategories toEntity() {
    String activo = "Activo";
    String creado = DateTool.getCurrentDate();
    String actualizado = DateTool.getCurrentDate();
    String usuario = "Gerardo";
    return SubCategories(name: name, code: code, id: id);
  }

  static SubCategories fromEntity(SubCategoriesEntity entity) {
    return SubCategories(
      name: entity.name,
      code: entity.code,
      id: entity.id,
    );
  }
}
