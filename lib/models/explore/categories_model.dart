import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/entities/explore/categories_entity.dart';

@immutable
class Categories {
  final String id;
  final String name;
  final String code;

  const Categories({String? name, String? code, String? id})
      : name = name ?? '',
        code = code ?? '',
        id = id ?? '';

  Categories copyWith(
      {String? name,
      String? code,
      String? description,
      String? discount,
      String? id}) {
    return Categories(
        name: name ?? this.name, code: code ?? this.code, id: id ?? this.id);
  }

  @override
  int get hashCode => name.hashCode ^ code.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Categories &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code &&
          id == other.id;

  @override
  String toString() {
    return 'Categories { complete: $name, code: $code, id: $id }';
  }

  Categories toEntity() {
    String activo = "Activo";
    String creado = DateTool.getCurrentDate();
    String actualizado = DateTool.getCurrentDate();
    String usuario = "Gerardo";
    return Categories(name: name, code: code, id: id);
  }

  static Categories fromEntity(CategoriesEntity entity) {
    return Categories(
      name: entity.name,
      code: entity.code,
      id: entity.id,
    );
  }
}
