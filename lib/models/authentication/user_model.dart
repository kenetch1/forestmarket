import 'dart:convert';

import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  const UserModel(
      {required this.id,
      required this.firstName,
      required this.lastName,
      required this.email,
      required this.imageUrl,
      required this.token});

  final String id;
  final String firstName;
  final String lastName;
  final String email;
  final String imageUrl;
  final String token;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json['id'] ?? "",
        firstName: json['firstName'] ?? "",
        lastName: json['lastName'] ?? "",
        email: json['email'] ?? "",
        imageUrl: json['imageUrl'] ?? "",
        token: json['token'] ?? "",
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'imageUrl': imageUrl,
        'token': token
      };

  toJsonString() => json.encode(toJson());

  factory UserModel.empty() => const UserModel(
      id: "", firstName: "", lastName: "", email: "", imageUrl: "", token: "");

  @override
  List<Object?> get props => [id, firstName, lastName, email, imageUrl, token];
}
