import 'package:equatable/equatable.dart';
import 'package:foreststore/entities/cart/cart_entity.dart';

class GroceryItem extends Equatable {
  final String id;
  final String name;
  final String description;
  final double price;
  final double offer;
  final double discount;
  final String imagePath;
  final double quantity;

  const GroceryItem(
      {required this.id,
      required this.name,
      required this.description,
      required this.price,
      this.offer = 0.0,
      this.discount = 0.0,
      required this.imagePath,
      this.quantity = 1.0});

  GroceryItem copyWith(
          {String? id,
          String? name,
          String? description,
          double? price,
          double? offer,
          double? discount,
          String? imagePath,
          double? quantity}) =>
      GroceryItem(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        price: price ?? this.price,
        offer: offer ?? this.offer,
        discount: discount ?? this.discount,
        imagePath: imagePath ?? this.imagePath,
        quantity: quantity ?? this.quantity,
      );

  // Convert a UsersEntity into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'price': price,
      'offer': offer,
      'discount': discount,
      'imagePath': imagePath,
      'quantity': quantity,
    };
  }

  factory GroceryItem.fromMap(Map<String, dynamic> map) {
    return GroceryItem(
        id: map['id'].toInt() ?? 0,
        name: map['name'] ?? '',
        description: map['description'] ?? '',
        price: map['price'] as double,
        offer: map['offer'] as double,
        discount: map['discount'] as double,
        imagePath: map['imagePath'] ?? '',
        quantity: map['quantity'] as double);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [id, name, description, price, quantity];
}

var orderItem = [
  CartEntity(
      id: 0,
      productId: "Producto",
      name: "Organic Bananas",
      description: "7pcs, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/banana.png")
];

var demoItems = [
  GroceryItem(
      id: "1",
      name: "Organic Bananas",
      description: "7pcs, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/banana.png"),
  GroceryItem(
      id: "2",
      name: "Red Apple",
      description: "1kg, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/apple.png"),
  GroceryItem(
      id: "3",
      name: "Bell Pepper Red",
      description: "1kg, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/pepper.png"),
  GroceryItem(
      id: "4",
      name: "Ginger",
      description: "250gm, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/ginger.png"),
  GroceryItem(
      id: "5",
      name: "Meat",
      description: "250gm, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/beef.png"),
  GroceryItem(
      id: "6",
      name: "Chikken",
      description: "250gm, Priceg",
      price: 4.99,
      imagePath: "assets/images/grocery_images/chicken.png"),
];

var exclusiveOffers = [demoItems[0], demoItems[1]];

var bestSelling = [demoItems[2], demoItems[3]];

var groceries = [demoItems[4], demoItems[5]];

var beverages = [
  GroceryItem(
      id: "7",
      name: "Dite Coke",
      description: "355ml, Price",
      price: 1.99,
      imagePath: "assets/images/beverages_images/diet_coke.png"),
  GroceryItem(
      id: "8",
      name: "Sprite Can",
      description: "325ml, Price",
      price: 1.50,
      imagePath: "assets/images/beverages_images/sprite.png"),
  GroceryItem(
      id: "8",
      name: "Apple Juice",
      description: "2L, Price",
      price: 15.99,
      imagePath: "assets/images/beverages_images/apple_and_grape_juice.png"),
  GroceryItem(
      id: "9",
      name: "Orange Juice",
      description: "2L, Price",
      price: 1.50,
      imagePath: "assets/images/beverages_images/orange_juice.png"),
  GroceryItem(
      id: "10",
      name: "Coca Cola Can",
      description: "325ml, Price",
      price: 4.99,
      imagePath: "assets/images/beverages_images/coca_cola.png"),
  GroceryItem(
      id: "11",
      name: "Pepsi Can",
      description: "330ml, Price",
      price: 4.99,
      imagePath: "assets/images/beverages_images/pepsi.png"),
];
