import 'package:flutter/cupertino.dart';
import 'package:foreststore/entities/home/solds_entity.dart';

@immutable
class Solds {
  final String id;
  final String soldId;
  final String subCategoryId;
  final String productId;

  const Solds(
      {String? soldId, String? subCategoryId, String? productId, String? id})
      : soldId = soldId ?? '',
        subCategoryId = subCategoryId ?? '',
        productId = productId ?? '',
        id = id ?? '';

  Solds copyWith(
      {String? soldId, String? subCategoryId, String? productId, String? id}) {
    return Solds(
        soldId: soldId ?? this.soldId,
        subCategoryId: subCategoryId ?? this.subCategoryId,
        productId: productId ?? this.productId,
        id: id ?? this.id);
  }

  @override
  int get hashCode =>
      soldId.hashCode ^
      subCategoryId.hashCode ^
      productId.hashCode ^
      id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Solds &&
          runtimeType == other.runtimeType &&
          soldId == other.soldId &&
          subCategoryId == other.subCategoryId &&
          productId == other.productId &&
          id == other.id;

  @override
  String toString() {
    return 'Solds { complete: $soldId, subCategoryId: $subCategoryId, id: $id }';
  }

  // Solds toEntity() {
  //   String activo = "Activo";
  //   String creado = DataTool.getCurrentDate();
  //   String actualizado = DataTool.getCurrentDate();
  //   String usuario = "Gerardo";
  //   return Solds(
  //       title: title,
  //       code: code,
  //       description: description,
  //       discount: discount,
  //       id: id);
  // }

  static Solds fromEntity(SoldsEntity entity) {
    return Solds(
      soldId: entity.soldId,
      subCategoryId: entity.subCategoryId,
      productId: entity.productId,
      id: entity.id,
    );
  }
}
