import 'package:flutter/cupertino.dart';
import 'package:foreststore/entities/home/news_entity.dart';

@immutable
class News {
  final String id;
  final String newId;
  final String subCategoryId;
  final String productId;

  const News(
      {String? newId, String? subCategoryId, String? productId, String? id})
      : newId = newId ?? '',
        subCategoryId = subCategoryId ?? '',
        productId = productId ?? '',
        id = id ?? '';

  News copyWith(
      {String? newId, String? subCategoryId, String? productId, String? id}) {
    return News(
        newId: newId ?? this.newId,
        subCategoryId: subCategoryId ?? this.subCategoryId,
        productId: productId ?? this.productId,
        id: id ?? this.id);
  }

  @override
  int get hashCode =>
      newId.hashCode ^
      subCategoryId.hashCode ^
      productId.hashCode ^
      id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is News &&
          runtimeType == other.runtimeType &&
          newId == other.newId &&
          subCategoryId == other.subCategoryId &&
          productId == other.productId &&
          id == other.id;

  @override
  String toString() {
    return 'News { complete: $newId, subCategoryId: $subCategoryId, id: $id }';
  }

  // News toEntity() {
  //   String activo = "Activo";
  //   String creado = DataTool.getCurrentDate();
  //   String actualizado = DataTool.getCurrentDate();
  //   String usuario = "Gerardo";
  //   return News(
  //       title: title,
  //       code: code,
  //       description: description,
  //       discount: discount,
  //       id: id);
  // }

  static News fromEntity(NewsEntity entity) {
    return News(
      newId: entity.newId,
      subCategoryId: entity.subCategoryId,
      productId: entity.productId,
      id: entity.id,
    );
  }
}
