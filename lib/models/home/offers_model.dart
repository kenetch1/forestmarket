import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/entities/home/offers_entity.dart';

@immutable
class Offers {
  final String id;
  final String title;
  final String code;
  final String description;
  final String discount;

  const Offers(
      {String? title,
      String? code,
      String? description,
      String? discount,
      String? id})
      : title = title ?? '',
        code = code ?? '',
        description = description ?? '',
        discount = discount ?? '0.0',
        id = id ?? '';

  Offers copyWith(
      {String? title,
      String? code,
      String? description,
      String? discount,
      String? id}) {
    return Offers(
        title: title ?? this.title,
        code: code ?? this.code,
        description: description ?? this.description,
        discount: discount ?? this.discount,
        id: id ?? this.id);
  }

  @override
  int get hashCode => title.hashCode ^ code.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Offers &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          code == other.code &&
          id == other.id;

  @override
  String toString() {
    return 'Offers { complete: $title, code: $code, id: $id }';
  }

  Offers toEntity() {
    String activo = "Activo";
    String creado = DateTool.getCurrentDate();
    String actualizado = DateTool.getCurrentDate();
    String usuario = "Gerardo";
    return Offers(
        title: title,
        code: code,
        description: description,
        discount: discount,
        id: id);
  }

  static Offers fromEntity(OffersEntity entity) {
    return Offers(
      title: entity.titles,
      code: entity.code,
      description: entity.description,
      discount: entity.discount,
      id: entity.id,
    );
  }
}
