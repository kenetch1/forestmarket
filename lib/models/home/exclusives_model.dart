import 'package:flutter/cupertino.dart';
import 'package:foreststore/entities/home/exclusives_entity.dart';

@immutable
class Exclusives {
  final String id;
  final String exclusiveId;
  final String subCategoryId;
  final String productId;

  const Exclusives(
      {String? exclusiveId,
      String? subCategoryId,
      String? productId,
      String? id})
      : exclusiveId = exclusiveId ?? '',
        subCategoryId = subCategoryId ?? '',
        productId = productId ?? '',
        id = id ?? '';

  Exclusives copyWith(
      {String? exclusiveId,
      String? subCategoryId,
      String? productId,
      String? id}) {
    return Exclusives(
        exclusiveId: exclusiveId ?? this.exclusiveId,
        subCategoryId: subCategoryId ?? this.subCategoryId,
        productId: productId ?? this.productId,
        id: id ?? this.id);
  }

  @override
  int get hashCode =>
      exclusiveId.hashCode ^
      subCategoryId.hashCode ^
      productId.hashCode ^
      id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Exclusives &&
          runtimeType == other.runtimeType &&
          exclusiveId == other.exclusiveId &&
          subCategoryId == other.subCategoryId &&
          productId == other.productId &&
          id == other.id;

  @override
  String toString() {
    return 'Exclusives { complete: $exclusiveId, subCategoryId: $subCategoryId, productId: $productId, id: $id }';
  }

  // Exclusives toEntity() {
  //   String activo = "Activo";
  //   String creado = DataTool.getCurrentDate();
  //   String actualizado = DataTool.getCurrentDate();
  //   String usuario = "Gerardo";
  //   return Exclusives(
  //       title: title,
  //       code: code,
  //       description: description,
  //       discount: discount,
  //       id: id);
  // }

  static Exclusives fromEntity(ExclusivesEntity entity) {
    return Exclusives(
      exclusiveId: entity.exclusiveId,
      subCategoryId: entity.subCategoryId,
      productId: entity.productId,
      id: entity.id,
    );
  }
}
