import 'package:flutter/cupertino.dart';
import 'package:foreststore/entities/product/products_entity.dart';

@immutable
class Products {
  final String id;
  final String foreignId;
  final String productId;
  final String product;
  final String description;
  final String regularPrice;
  final String offer;
  final String discount;

  const Products(
      {String? foreignId,
      String? productId,
      String? product,
      String? description,
      String? regularPrice,
      String? offer,
      String? discount,
      String? id})
      : foreignId = foreignId ?? '',
        productId = productId ?? '',
        product = product ?? '',
        description = description ?? '',
        regularPrice = regularPrice ?? '0.0',
        offer = offer ?? '0.0',
        discount = discount ?? '0.0',
        id = id ?? '';

  Products copyWith(
      {String? foreignId,
      String? productId,
      String? product,
      String? description,
      String? regularPrice,
      String? offer,
      String? discount,
      String? id}) {
    return Products(
        foreignId: foreignId ?? this.foreignId,
        productId: productId ?? this.productId,
        product: product ?? this.product,
        description: description ?? this.description,
        regularPrice: regularPrice ?? this.regularPrice,
        offer: offer ?? this.offer,
        discount: discount ?? this.discount,
        id: id ?? this.id);
  }

  @override
  int get hashCode => foreignId.hashCode ^ productId.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Products &&
          runtimeType == other.runtimeType &&
          foreignId == other.foreignId &&
          productId == other.productId &&
          id == other.id;

  @override
  String toString() {
    return 'Products { foreignId: $foreignId, productId: $productId, product: $product, description: $description }';
  }

  // Products toEntity() {
  //   String activo = "Activo";
  //   String creado = DataTool.getCurrentDate();
  //   String actualizado = DataTool.getCurrentDate();
  //   String usuario = "Gerardo";
  //   return Products(
  //       title: title,
  //       code: code,
  //       description: description,
  //       discount: discount,
  //       id: id);
  // }

  static Products fromEntity(ProductsEntity entity) {
    return Products(
      foreignId: entity.foreignId,
      productId: entity.productId,
      product: entity.product,
      description: entity.description,
      regularPrice: entity.regularPrice,
      offer: entity.offer,
      discount: entity.discount,
      id: entity.id,
    );
  }
}
