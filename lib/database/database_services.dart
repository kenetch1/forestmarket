import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/cart/cart_entity.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  // Singleton pattern
  static final DatabaseService _databaseService = DatabaseService._internal();
  factory DatabaseService() => _databaseService;
  DatabaseService._internal();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    // Initialize the DB first time it is accessed
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final databasePath = await getDatabasesPath();

    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    final path = join(databasePath, 'flutter_sqflite_database.db');

    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    return await openDatabase(
      path,
      onCreate: _onCreate,
      version: 1,
      onConfigure: (db) async => await db.execute('PRAGMA foreign_keys = ON'),
    );
  }

  // When the database is first created, create a table to store breeds
  // and a table to store dogs.
  Future<void> _onCreate(Database db, int version) async {
    // Run the CREATE {breeds} TABLE statement on the database.
    // await db.execute(
    //   'CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, description TEXT)',
    // );
    //here we execute a query to drop the table if exists which is called "tableName"
    //and could be given as method's input parameter too
    await db.execute("DROP TABLE IF EXISTS users");
    // Run the CREATE {dogs} TABLE statement on the database.
    String userTable = 'CREATE TABLE users(id INTEGER PRIMARY KEY,';
    userTable += 'name TEXT,';
    userTable += 'code TEXT,';
    userTable += 'username TEXT,';
    userTable += 'phoneNumber TEXT,';
    userTable += 'email TEXT,';
    userTable += 'image TEXT,';
    userTable += 'uid TEXT,';
    userTable += 'active TEXT,';
    userTable += 'createAt TEXT,';
    userTable += 'updateAt TEXT,';
    userTable += 'user TEXT,';
    userTable += 'type TEXT';
    userTable += ')';

    await db.execute(userTable);

    String cartTable = 'CREATE TABLE cart (id INTEGER PRIMARY KEY,';
    cartTable += 'productId TEXT,';
    cartTable += 'name TEXT,';
    cartTable += 'description TEXT,';
    cartTable += 'imagePath TEXT,';
    cartTable += 'price REAL,';
    cartTable += 'offer REAL,';
    cartTable += 'discount REAL,';
    cartTable += 'quantity INTEGER';
    cartTable += ')';

    await db.execute(cartTable);
  }

  // Define a function that inserts breeds into the database
  Future<void> insertUsers(UsersEntity user) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Insert the Breed into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same breed is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'users',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  //CartEntity
  // Define a function that inserts breeds into the database
  Future<void> insertCart(CartEntity cart) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Insert the Breed into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same breed is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'cart',
      cart.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<int> getCountCart() async {
    //database connection
    final db = await _databaseService.database;
    var x = await db.rawQuery('SELECT COUNT (id) from cart');
    int count = Sqflite.firstIntValue(x) ?? 0;
    return count;
  }
  // Future<void> insertDog(Dog dog) async {
  //   final db = await _databaseService.database;
  //   await db.insert(
  //     'dogs',
  //     dog.toMap(),
  //     conflictAlgorithm: ConflictAlgorithm.replace,
  //   );
  // }

  // A method that retrieves all the users from the users table.
  Future<List<UsersEntity>> users() async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Query the table for all the Users.
    final List<Map<String, dynamic>> maps = await db.query('users');

    // Convert the List<Map<String, dynamic> into a List<Breed>.
    return List.generate(
        maps.length, (index) => UsersEntity.fromMap(maps[index]));
  }

  // A method that retrieves all the users from the users table.
  Future<List<CartEntity>> carts() async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Query the table for all the Users.
    final List<Map<String, dynamic>> maps = await db.query('cart');

    // Convert the List<Map<String, dynamic> into a List<Breed>.
    return List.generate(
        maps.length, (index) => CartEntity.fromMap(maps[index]));
  }

  Future<UsersEntity> user(int id) async {
    final db = await _databaseService.database;
    final List<Map<String, dynamic>> maps =
        await db.query('users', where: 'id = ?', whereArgs: [id]);
    return maps.isNotEmpty
        ? UsersEntity.fromMap(maps[0])
        : const UsersEntity(
            id: 0,
            code: "",
            username: "",
            phoneNumber: "",
            email: "",
            user: "",
            type: "");
  }

  // Future<List<Dog>> dogs() async {
  //   final db = await _databaseService.database;
  //   final List<Map<String, dynamic>> maps = await db.query('dogs');
  //   return List.generate(maps.length, (index) => Dog.fromMap(maps[index]));
  // }

  // A method that updates a breed data from the breeds table.
  Future<void> updateUser(UsersEntity user) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Update the given user
    await db.update(
      'users',
      user.toMap(),
      // Ensure that the Breed has a matching id.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [user.id],
    );
  }

  // A method that updates a breed data from the breeds table.
  Future<void> updateCart(CartEntity cart) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Update the given user
    await db.update(
      'cart',
      cart.toMap(),
      // Ensure that the Breed has a matching id.
      where: 'productId = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [cart.productId],
    );
  }

  // Future<void> updateDog(Dog dog) async {
  //   final db = await _databaseService.database;
  //   await db.update('dogs', dog.toMap(), where: 'id = ?', whereArgs: [dog.id]);
  // }

  // A method that deletes a breed data from the breeds table.
  Future<void> deleteUser(int id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Remove the Breed from the database.
    await db.delete(
      'users',
      // Use a `where` clause to delete a specific breed.
      where: 'id = ?',
      // Pass the Breed's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> deleteCart(int id) async {
    final db = await _databaseService.database;
    await db.delete('cart', where: 'id = ?', whereArgs: [id]);
  }

  Future<void> deleteAllCart() async {
    final db = await _databaseService.database;
    await db.delete('cart');
  }
}
