import 'dart:convert';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInfo {
  static UserModel getUserInfo(SharedPreferences prefs) {
    return UserModel.fromJson(jsonDecode(prefs.getString("user") ?? ""));
  }
}
