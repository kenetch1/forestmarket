import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

/*active true
create_at "09/01/2023 06:09:00"
email "a@aa.com"
gender "M"
image "url"
phoneNumber "(55)55-55-55-55"
type "user"
uid "CTaUBqw9tXOTnQ4mgw1iQDUTEo93"
update_at "09/01/2023 06:09:00"
user "Gerardo"
userId "FV6ixPjxlba8X3RVRBbr"
username "Alfredo del Mazo"*/

class UsersEntity extends Equatable {
  final int? id;
  final String? doc;
  final String code;
  final String username;
  final String phoneNumber;
  final String email;
  final String? image;
  final String? uid;
  final String? active;
  final String? createAt;
  final String? updateAt;
  final String user;
  final String type;
  final String? token;

  const UsersEntity(
      {this.id,
      this.doc,
      required this.code,
      required this.username,
      required this.phoneNumber,
      required this.email,
      this.image,
      this.uid,
      this.active,
      this.createAt,
      this.updateAt,
      required this.user,
      required this.type,
      this.token});

  // Convert a UsersEntity into a Map. The keys must correspond to the names of the
  // columns in the database.

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'code': code,
      'username': username,
      'phoneNumber': phoneNumber,
      'email': email,
      'image': image,
      'uid': uid,
      'active': active,
      'createAt': createAt,
      'updateAt': updateAt,
      'user': user,
      'token': token
    };
  }

  Map<String, dynamic> toFirestoreMap() {
    return {
      'code': code,
      'username': username,
      'phoneNumber': phoneNumber,
      'email': email,
      'image': image,
      'uid': uid,
      'active': active,
      'createAt': createAt,
      'updateAt': updateAt,
      'user': user,
      'type': type,
      'token': token
    };
  }

  factory UsersEntity.fromMap(Map<String, dynamic> map) {
    return UsersEntity(
        id: map['id'].toInt() ?? 0,
        code: map['code'] ?? '',
        username: map['username'] ?? '',
        phoneNumber: map['phoneNumber'] ?? '',
        email: map['email'] ?? '',
        image: map['image'] ?? '',
        uid: map['uid'] ?? '',
        active: map['active'] ?? '',
        createAt: map['createAt'] ?? '',
        updateAt: map['updateAt'] ?? '',
        user: map['user'] ?? '',
        type: map['type'] ?? '',
        token: map['token'] ?? '');
  }

  static UsersEntity fromSnapshot(DocumentSnapshot snap) {
    return UsersEntity(
        id: 0,
        doc: snap.id,
        code: snap['code'],
        username: snap['username'],
        phoneNumber: snap['phoneNumber'],
        email: snap['email'],
        image: snap['image'],
        uid: snap['uid'],
        active: snap['active'],
        createAt: snap['createAt'],
        updateAt: snap['updateAt'],
        user: snap['user'],
        type: snap['type'],
        token: snap['token']);
  }

  String toJson() => json.encode(toMap());

  factory UsersEntity.fromJson(String source) =>
      UsersEntity.fromMap(json.decode(source));

  // Implement toString to make it easier to see information about
  // each UsersEntity when using the print statement.
  @override
  String toString() {
    return 'UsersEntity(id: $id, code: $code, username: $username, phoneNumber: $phoneNumber, uid: $uid)';
  }

  @override
  List<Object?> get props => [
        id,
        code,
        username,
        phoneNumber,
        email,
        image,
        uid,
        active,
        createAt,
        updateAt,
        user,
        type,
        token
      ];
}
