import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

// userId	shippingMethodId	shippingMethod	orderStatusId	orderStatus	grandTotal
// selectedTime	free_shipping	delivery_in	active	create_at	update_at	user

class OrdersEntity extends Equatable {
  final String id;
  final String foreignId;
  final String orderId;
  final String shippingMethodId;
  final String shippingMethod;
  final String orderStatusId;
  final String orderStatus;
  final String grandTotal;
  final String selectedTime;
  final String freeShipping;
  final String deliveryIn;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const OrdersEntity(
    this.id,
    this.foreignId,
    this.orderId,
    this.shippingMethodId,
    this.shippingMethod,
    this.orderStatusId,
    this.orderStatus,
    this.grandTotal,
    this.selectedTime,
    this.freeShipping,
    this.deliveryIn,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'foreignId': foreignId,
      'orderId': orderId,
      'shippingMethodId': shippingMethodId,
      'shippingMethod': shippingMethod,
      'orderStatusId': orderStatusId,
      'orderStatus': orderStatus,
      'grandTotal': grandTotal,
      'selectedTime': selectedTime,
      'free_shipping': freeShipping,
      'delivery_in': deliveryIn,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'OrdersEntity { active: $active, orderId: $orderId,  shippingMethodId: $shippingMethodId, id: $id }';
  }

  static OrdersEntity fromJson(Map<String, Object> json) {
    return OrdersEntity(
      json['id'] as String,
      json['foreignId'] as String,
      json['orderId'] as String,
      json['shippingMethodId'] as String,
      json['shippingMethod'] as String,
      json['orderStatusId'] as String,
      json['orderStatus'] as String,
      json['grandTotal'] as String,
      json['selectedTime'] as String,
      json['free_shipping'] as String,
      json['delivery_in'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static OrdersEntity fromSnapshot(DocumentSnapshot snap) {
    return OrdersEntity(
        snap.id,
        snap['foreignId'],
        snap['orderId'],
        snap['shippingMethodId'],
        snap['shippingMethod'],
        snap['orderStatusId'],
        snap['orderStatus'],
        snap['grandTotal'],
        snap['selectedTime'],
        snap['free_shipping'],
        snap['delivery_in'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'foreignId': foreignId,
      'orderId': orderId,
      'shippingMethodId': shippingMethodId,
      'shippingMethod': shippingMethod,
      'orderStatusId': orderStatusId,
      'orderStatus': orderStatus,
      'grandTotal': grandTotal,
      'selectedTime': selectedTime,
      'free_shipping': freeShipping,
      'delivery_in': deliveryIn,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        orderId,
        shippingMethodId,
        shippingMethod,
        orderStatusId,
        orderStatus,
        grandTotal,
        selectedTime,
        freeShipping,
        deliveryIn,
        active,
        createAt,
        updateAt,
        user
      ];
}
