import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

// userId	foreignId	offer	product	productId	quantity
// regularPrice	free_shipping	delivery_in	active	create_at	update_at	user

class OrdersProductsEntity extends Equatable {
  final String id;
  final String description;
  final String discount;
  final String foreignId;
  final String offer;
  final String product;
  final String productId;
  final String quantity;
  final String regularPrice;

  const OrdersProductsEntity(
      this.id,
      this.description,
      this.discount,
      this.foreignId,
      this.offer,
      this.product,
      this.productId,
      this.quantity,
      this.regularPrice);

  Map<String, Object> toJson() {
    return {
      'description': description,
      'discount': discount,
      'foreignId': foreignId,
      'offer': offer,
      'product': product,
      'productId': productId,
      'quantity': quantity,
      'regularPrice': regularPrice
    };
  }

  @override
  String toString() {
    return 'OrdersProductsEntity { description: $description, discount: $discount,  foreignId: $foreignId, id: $id }';
  }

  static OrdersProductsEntity fromJson(Map<String, Object> json) {
    return OrdersProductsEntity(
        json['id'] as String,
        json['description'] as String,
        json['discount'] as String,
        json['foreignId'] as String,
        json['offer'] as String,
        json['product'] as String,
        json['productId'] as String,
        json['quantity'] as String,
        json['regularPrice'] as String);
  }

  static OrdersProductsEntity fromSnapshot(DocumentSnapshot snap) {
    return OrdersProductsEntity(
        snap.id,
        snap['description'],
        snap['discount'],
        snap['foreignId'],
        snap['offer'],
        snap['product'],
        snap['productId'],
        snap['quantity'],
        snap['regularPrice']);
  }

  Map<String, Object> toDocument() {
    return {
      'description': description,
      'discount': discount,
      'foreignId': foreignId,
      'offer': offer,
      'product': product,
      'productId': productId,
      'quantity': quantity,
      'regularPrice': regularPrice
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        discount,
        foreignId,
        offer,
        product,
        productId,
        quantity,
        regularPrice
      ];
}
