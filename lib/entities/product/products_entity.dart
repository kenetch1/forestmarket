import 'dart:ffi';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ProductsEntity extends Equatable {
  final String id;
  final String productId;
  final String foreignId;
  final String product;
  final String image;
  final List<dynamic> images;
  final String regularPrice;
  final String pricePerQuantity;
  final String offer;
  final String discount;
  final String description;
  final String instruction;
  final String pin;
  final String tags;
  final String like;
  final String instock;
  final String freeShipping;
  final String deliveryIn;
  final String cod;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const ProductsEntity(
      this.id,
      this.productId,
      this.foreignId,
      this.product,
      this.image,
      this.images,
      this.regularPrice,
      this.pricePerQuantity,
      this.offer,
      this.discount,
      this.description,
      this.instruction,
      this.pin,
      this.tags,
      this.like,
      this.instock,
      this.freeShipping,
      this.deliveryIn,
      this.cod,
      this.active,
      this.createAt,
      this.updateAt,
      this.user);

  Map<String, Object> toJson() {
    return {
      'id': id,
      'productId': productId,
      'foreignId': foreignId,
      'product': product,
      'image': image,
      'images': images,
      'regularPrice': regularPrice,
      'pricePerQuantity': pricePerQuantity,
      'offer': offer,
      'discount': discount,
      'description': description,
      'instruction': instruction,
      'pin': pin,
      'tags': tags,
      'like': like,
      'instock': instock,
      'freeShipping': freeShipping,
      'deliveryIn': deliveryIn,
      'cod': cod,
      'active': active
    };
  }

  @override
  String toString() {
    return 'ProductsEntity { active: $active,  id: $id }';
  }

  static ProductsEntity fromJson(Map<String, Object> json) {
    return ProductsEntity(
      json['id'] as String,
      json['productId'] as String,
      json['foreignId'] as String,
      json['product'] as String,
      json['image'] as String,
      json['images'] as List<dynamic>,
      json['regularPrice'] as String,
      json['pricePerQuantity'] as String,
      json['offer'] as String,
      json['discount'] as String,
      json['description'] as String,
      json['instruction'] as String,
      json['pin'] as String,
      json['tags'] as String,
      json['like'] as String,
      json['instock'] as String,
      json['freeShipping'] as String,
      json['deliveryIn'] as String,
      json['cod'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  //snap['discount'].toDouble(),

  static ProductsEntity fromSnapshot(DocumentSnapshot snap) {
    return ProductsEntity(
        snap.id,
        snap['productId'],
        snap['foreignId'],
        snap['product'],
        snap['image'],
        snap['images'],
        snap['regularPrice'],
        snap['pricePerQuantity'],
        snap['offer'],
        snap['discount'],
        snap['description'],
        snap['instruction'],
        snap['pin'],
        snap['tags'],
        snap['Like'],
        snap['instock'],
        snap['free_shipping'],
        snap['delivery_in'],
        snap['cod'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'foreignId': foreignId,
      'product': product,
      'images': images,
      'regularPrice': regularPrice,
      'pricePerQuantity': pricePerQuantity,
      'offer': offer,
      'discount': discount,
      'description': description,
      'instruction': instruction,
      'pin': pin,
      'tags': tags,
      'like': like,
      'instock': instock,
      'free_shipping': freeShipping,
      'delivery_in': deliveryIn,
      'cod': cod,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  List<Object?> get props => [
        id,
        productId,
        foreignId,
        product,
        image,
        images,
        regularPrice,
        pricePerQuantity,
        offer,
        discount,
        description,
        instruction,
        pin,
        tags,
        like,
        instock,
        freeShipping,
        deliveryIn,
        cod,
        active,
        createAt,
        updateAt,
        user
      ];
}
