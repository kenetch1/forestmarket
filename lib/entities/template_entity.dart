import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class TemplateEntity extends Equatable {
  final String id;
  final bool active;
  final String createAt;
  final String updateAt;
  final String user;

  const TemplateEntity(
    this.id,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'active': active,
      'id': id,
    };
  }

  @override
  String toString() {
    return 'TemplateEntity { active: $active,  id: $id }';
  }

  static TemplateEntity fromJson(Map<String, Object> json) {
    return TemplateEntity(
      json['id'] as String,
      json['active'] as bool,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static TemplateEntity fromSnapshot(DocumentSnapshot snap) {
    return TemplateEntity(snap.id, snap['active'], snap['create_at'],
        snap['update_at'], snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [id, active, createAt, updateAt, user];
}
