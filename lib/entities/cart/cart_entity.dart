import 'package:equatable/equatable.dart';

class CartEntity extends Equatable {
  final int? id;
  final String productId;
  final String name;
  final String description;
  final String imagePath;
  final double price;
  final double offer;
  final double discount;
  final int quantity;

  const CartEntity(
      {this.id,
      required this.productId,
      required this.name,
      required this.description,
      required this.imagePath,
      required this.price,
      this.offer = 0.0,
      this.discount = 0.0,
      this.quantity = 1});

  CartEntity copyWith(
          {int? id,
          String? productId,
          String? name,
          String? description,
          String? imagePath,
          double? price,
          double? offer,
          double? discount,
          int? quantity}) =>
      CartEntity(
        id: id ?? this.id,
        productId: imagePath ?? this.productId,
        name: name ?? this.name,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        price: price ?? this.price,
        offer: offer ?? this.offer,
        discount: discount ?? this.discount,
        quantity: quantity ?? this.quantity,
      );

  // Convert a UsersEntity into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productId': productId,
      'name': name,
      'description': description,
      'imagePath': imagePath,
      'price': price,
      'offer': offer,
      'discount': discount,
      'quantity': quantity,
    };
  }

  factory CartEntity.fromMap(Map<String, dynamic> map) {
    return CartEntity(
        id: map['id'].toInt() ?? 0,
        productId: map['productId'] ?? '',
        name: map['name'] ?? '',
        description: map['description'] ?? '',
        imagePath: map['imagePath'] ?? '',
        price: map['price'] as double,
        offer: map['offer'] as double,
        discount: map['discount'] as double,
        quantity: map['quantity'] as int);
  }

  @override
  // TODO: implement props
  List<Object?> get props =>
      [id, productId, name, description, imagePath, price, quantity];
}
