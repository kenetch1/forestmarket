import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class NewsEntity extends Equatable {
  final String id;
  final String newId;
  final String subCategoryId;
  final String productId;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const NewsEntity(
    this.id,
    this.newId,
    this.subCategoryId,
    this.productId,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'newId': newId,
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'NewsEntity { active: $active, newId: $newId, subCategoryId: $subCategoryId productId: $productId, id: $id }';
  }

  static NewsEntity fromJson(Map<String, Object> json) {
    return NewsEntity(
      json['id'] as String,
      json['newId'] as String,
      json['subCategoryId'] as String,
      json['productId'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static NewsEntity fromSnapshot(DocumentSnapshot snap) {
    return NewsEntity(
        snap.id,
        snap['newId'],
        snap['subCategoryId'],
        snap['productId'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props =>
      [id, newId, subCategoryId, productId, active, createAt, updateAt, user];
}
