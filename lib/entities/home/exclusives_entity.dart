import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ExclusivesEntity extends Equatable {
  final String id;
  final String exclusiveId;
  final String subCategoryId;
  final String productId;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const ExclusivesEntity(
    this.id,
    this.exclusiveId,
    this.subCategoryId,
    this.productId,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'exclusiveId': exclusiveId,
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'ExclusivesEntity { active: $active, exclusiveId: $exclusiveId, subCategoryId: $subCategoryId, productId: $productId, id: $id }';
  }

  static ExclusivesEntity fromJson(Map<String, Object> json) {
    return ExclusivesEntity(
      json['id'] as String,
      json['exclusiveId'] as String,
      json['subCategoryId'] as String,
      json['productId'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static ExclusivesEntity fromSnapshot(DocumentSnapshot snap) {
    return ExclusivesEntity(
        snap.id,
        snap['exclusiveId'],
        snap['subCategoryId'],
        snap['productId'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        exclusiveId,
        subCategoryId,
        productId,
        active,
        createAt,
        updateAt,
        user
      ];
}
