import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class SoldsEntity extends Equatable {
  final String id;
  final String soldId;
  final String subCategoryId;
  final String productId;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const SoldsEntity(
    this.id,
    this.soldId,
    this.subCategoryId,
    this.productId,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'soldId': soldId,
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'SoldsEntity { active: $active, soldId: $soldId, productId: $productId, id: $id }';
  }

  static SoldsEntity fromJson(Map<String, Object> json) {
    return SoldsEntity(
      json['id'] as String,
      json['soldId'] as String,
      json['subCategoryId'] as String,
      json['productId'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static SoldsEntity fromSnapshot(DocumentSnapshot snap) {
    return SoldsEntity(
        snap.id,
        snap['soldId'],
        snap['subCategoryId'],
        snap['productId'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'subCategoryId': subCategoryId,
      'productId': productId,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props =>
      [id, soldId, subCategoryId, productId, active, createAt, updateAt, user];
}
