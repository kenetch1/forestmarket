import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class OffersEntity extends Equatable {
  final String id;
  final String offerId;
  final String titles;
  final String description;
  final String code;
  final List<dynamic> images;
  final String discount;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const OffersEntity(
    this.id,
    this.offerId,
    this.titles,
    this.description,
    this.code,
    this.images,
    this.discount,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'offerId': offerId,
      'titles': titles,
      'description': description,
      'code': code,
      'images': images,
      'discount': discount,
      'active': active,
    };
  }

  @override
  String toString() {
    return 'OffersEntity { active: $active,  id: $id }';
  }

  static OffersEntity fromJson(Map<String, Object> json) {
    return OffersEntity(
      json['id'] as String,
      json['offerId'] as String,
      json['titles'] as String,
      json['description'] as String,
      json['code'] as String,
      json['images'] as List<dynamic>,
      json['discount'] as String,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static OffersEntity fromSnapshot(DocumentSnapshot snap) {
    return OffersEntity(
        snap.id,
        snap['offerId'],
        snap['titles'],
        snap['description'],
        snap['code'],
        snap['images'],
        snap['discount'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        offerId,
        titles,
        description,
        code,
        images,
        discount,
        active,
        createAt,
        updateAt,
        user
      ];
}
