import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class SubCategoriesEntity extends Equatable {
  final String id;
  final String foreignId;
  final String subcategoryId;
  final String code;
  final String name;
  final String description;
  final String discount;
  final String homePage;
  final String image;
  final List<dynamic> images;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const SubCategoriesEntity(
    this.id,
    this.foreignId,
    this.subcategoryId,
    this.code,
    this.name,
    this.description,
    this.discount,
    this.homePage,
    this.image,
    this.images,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'foreignId': foreignId,
      'subcategorieId': subcategoryId,
      'code': code,
      'name': name,
      'description': description,
      'discount': discount,
      'homePage': homePage,
      'image': image,
      'images': images,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'SubCategoriesEntity { active: $active, categoryId: $subcategoryId,  code: $code, id: $id }';
  }

  static SubCategoriesEntity fromJson(Map<String, Object> json) {
    return SubCategoriesEntity(
      json['id'] as String,
      json['foreignId'] as String,
      json['subcategorieId'] as String,
      json['code'] as String,
      json['name'] as String,
      json['description'] as String,
      json['discount'] as String,
      json['homePage'] as String,
      json['image'] as String,
      json['images'] as List<String>,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static SubCategoriesEntity fromSnapshot(DocumentSnapshot snap) {
    return SubCategoriesEntity(
        snap.id,
        snap['foreignId'],
        snap['subcategorieId'],
        snap['code'],
        snap['name'],
        snap['description'],
        snap['discount'],
        snap['home_page'],
        snap['image'],
        snap['images'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'foreignId': foreignId,
      'code': code,
      'name': name,
      'description': description,
      'discount': discount,
      'homePage': homePage,
      'image': image,
      'images': images,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        foreignId,
        subcategoryId,
        code,
        name,
        description,
        discount,
        homePage,
        image,
        images,
        active,
        createAt,
        updateAt,
        user
      ];
}
