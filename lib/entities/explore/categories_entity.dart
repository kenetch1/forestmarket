import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class CategoriesEntity extends Equatable {
  final String id;
  final String categorieId;
  final String code;
  final String name;
  final String description;
  final String discount;
  final String homePage;
  final String image;
  final List<dynamic> images;
  final String active;
  final String createAt;
  final String updateAt;
  final String user;

  const CategoriesEntity(
    this.id,
    this.categorieId,
    this.code,
    this.name,
    this.description,
    this.discount,
    this.homePage,
    this.image,
    this.images,
    this.active,
    this.createAt,
    this.updateAt,
    this.user,
  );

  Map<String, Object> toJson() {
    return {
      'id': id,
      'categorieId': categorieId,
      'code': code,
      'name': name,
      'description': description,
      'discount': discount,
      'homePage': homePage,
      'image': image,
      'images': images,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  String toString() {
    return 'CategoriesEntity { active: $active, categorieId: $categorieId,  code: $code, id: $id }';
  }

  static CategoriesEntity fromJson(Map<String, Object> json) {
    return CategoriesEntity(
      json['id'] as String,
      json['categorieId'] as String,
      json['code'] as String,
      json['name'] as String,
      json['description'] as String,
      json['discount'] as String,
      json['homePage'] as String,
      json['image'] as String,
      json['images'] as List<String>,
      json['active'] as String,
      json['create_at'] as String,
      json['update_at'] as String,
      json['user'] as String,
    );
  }

  static CategoriesEntity fromSnapshot(DocumentSnapshot snap) {
    return CategoriesEntity(
        snap.id,
        snap['categorieId'],
        snap['code'],
        snap['name'],
        snap['description'],
        snap['discount'],
        snap['home_page'],
        snap['image'],
        snap['images'],
        snap['active'],
        snap['create_at'],
        snap['update_at'],
        snap['user']);
  }

  Map<String, Object> toDocument() {
    return {
      'code': code,
      'name': name,
      'description': description,
      'discount': discount,
      'homePage': homePage,
      'image': image,
      'images': images,
      'active': active,
      'create_at': createAt,
      'update_at': updateAt,
      'user': user
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        categorieId,
        code,
        name,
        description,
        discount,
        homePage,
        image,
        images,
        active,
        createAt,
        updateAt,
        user
      ];
}
