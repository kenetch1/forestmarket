import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/cart/cart_entity.dart';
import 'package:foreststore/styles/colors.dart';

import 'item_counter_widget.dart';

class ChartItemWidget extends StatefulWidget {
  final Function? onAmountChanged;
  final Function? onRemoveItem;
  CartEntity item;

  ChartItemWidget(
      {super.key, required this.item, this.onAmountChanged, this.onRemoveItem});

  @override
  _ChartItemWidgetState createState() => _ChartItemWidgetState();
}

class _ChartItemWidgetState extends State<ChartItemWidget> {
  final double height = 110;

  final Color borderColor = const Color(0xffE2E2E2);

  final double borderRadius = 18;

  double amount = 1.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      margin: const EdgeInsets.symmetric(
        vertical: 30,
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            imageWidget(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: AppText(
                    text: widget.item.name.length > 15
                        ? widget.item.name.substring(0, 15)
                        : widget.item.name,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Expanded(
                  child: AppText(
                      text: widget.item.description,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: AppColors.darkGrey),
                ),
                const SizedBox(
                  height: 12,
                ),
                const Spacer(),
                ItemCounterWidget(
                  onAmountChanged: (newAmount) {
                    setState(() {
                      amount = newAmount;
                      widget.item =
                          widget.item.copyWith(quantity: newAmount as int);
                      updateParent();
                    });
                  },
                  quantity: widget.item.quantity,
                )
              ],
            ),
            Expanded(
              child: Column(
                children: [
                  IconButton(
                    icon: const Icon(
                      Icons.close,
                      color: AppColors.darkGrey,
                      size: 25,
                    ),
                    onPressed: () {
                      removeItem();
                    },
                  ),
                  const Spacer(
                    flex: 5,
                  ),
                  SizedBox(
                    width: 70,
                    child: AppText(
                      text: "\$${getPrice().toStringAsFixed(2)}",
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.right,
                    ),
                  ),
                  const Spacer(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget imageWidget() {
    return SizedBox(
      width: 100,
      child: Image.asset(widget.item.imagePath),
    );
  }

  double getPrice() {
    return widget.item.price * amount;
  }

  void updateParent() {
    if (widget.onAmountChanged != null) {
      widget.onAmountChanged!(widget.item);
    }
  }

  void removeItem() {
    if (widget.onRemoveItem != null) {
      widget.onRemoveItem!(widget.item);
    }
  }
}
