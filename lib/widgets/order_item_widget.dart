import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/repositories/cart/cart_repository.dart';
import 'package:foreststore/styles/colors.dart';

import 'item_counter_widget.dart';

class OrderItemWidget extends StatefulWidget {
  final CartRepository cartRepository;
  final Function(bool)? onRemoveOrder;
  OrdersEntity item;

  OrderItemWidget(
      {super.key,
      required this.item,
      this.onRemoveOrder,
      required this.cartRepository});

  @override
  _OrderItemWidgetState createState() => _OrderItemWidgetState();
}

class _OrderItemWidgetState extends State<OrderItemWidget> {
  final double height = 110;

  final Color borderColor = const Color(0xffE2E2E2);

  final double borderRadius = 18;

  double amount = 1.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      margin: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            imageWidget(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(children: [
                    const TextSpan(
                        text: 'Estatus: ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text: widget.item.orderStatus,
                        style: const TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: Colors.blue))
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(children: [
                    const TextSpan(
                        text: 'Total: ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text: '\$${widget.item.grandTotal}',
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                            color: AppColors.primaryColor))
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(children: [
                    const TextSpan(
                        text: 'Entrega: ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text: widget.item.shippingMethod,
                        style: const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                            color: AppColors.primaryColor))
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(children: [
                    const TextSpan(
                        text: 'Ord: ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text: widget.item.orderId,
                        style: const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                            color: AppColors.primaryColor))
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(children: [
                    const TextSpan(
                        text: 'Recoger en: ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text: widget.item.selectedTime,
                        style: const TextStyle(
                            fontSize: 17,
                            fontStyle: FontStyle.normal,
                            color: AppColors.orangeColor))
                  ]),
                ),
                const SizedBox(
                  height: 2,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: AppColors.primaryColor,
                    foregroundColor: Colors.white,
                    elevation: 0.0,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    visualDensity: VisualDensity.compact,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    //minimumSize: Size(100, 20), //////// HERE
                  ),
                  onPressed: () {
                    widget.cartRepository
                        .removerOrder(widget.item.orderId)
                        .then((value) => removeOrder(value));
                  },
                  child: const Text('Remover'),
                )
                //const Spacer(),
                // ItemCounterWidget(
                //   onAmountChanged: (newAmount) {
                //     setState(() {
                //       amount = newAmount;
                //       widget.item =
                //           widget.item.copyWith(quantity: newAmount as double);
                //       updateParent();
                //     });
                //   },
                //   quantity: widget.item.quantity,
                // )
              ],
            ),
            // Expanded(
            //   child: Column(
            //     children: [
            //       const Icon(
            //         Icons.close,
            //         color: AppColors.darkGrey,
            //         size: 25,
            //       ),
            //       const Spacer(
            //         flex: 5,
            //       ),
            //       SizedBox(
            //         width: 70,
            //         child: AppText(
            //           text: "\$${getPrice().toStringAsFixed(2)}",
            //           fontSize: 18,
            //           fontWeight: FontWeight.bold,
            //           textAlign: TextAlign.right,
            //         ),
            //       ),
            //       const Spacer(),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  Widget imageWidget() {
    return const SizedBox(
      width: 40,
      child:
          Icon(Icons.shopping_basket, color: AppColors.orangeColor, size: 30),
    );
  }

  // double getPrice() {
  //   return widget.item.price * amount;
  // }

  void removeOrder(bool value) {
    if (widget.onRemoveOrder != null) {
      widget.onRemoveOrder!(value);
    }
  }
}
