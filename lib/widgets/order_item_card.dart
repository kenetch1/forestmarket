import 'package:flutter/material.dart';

class OrderItemCard extends StatefulWidget {
  const OrderItemCard({super.key});

  @override
  _OrderItemCardState createState() => _OrderItemCardState();
}

class _OrderItemCardState extends State<OrderItemCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      margin: const EdgeInsets.symmetric(
        vertical: 30,
      ),
      child: Center(
        child: Card(
          elevation: 50,
          shadowColor: Colors.black,
          color: Colors.yellow,
          child: SizedBox(
            width: 300,
            height: 275,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ), //SizedBox
                  Text(
                    '92',
                    style: TextStyle(
                      fontSize: 50,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ), //Textstyle
                  ), //Text
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Cristiano Ronaldo',
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ), //Textstyle
                  ), //Text
                  SizedBox(
                    height: 15,
                  ), //SizedBox
                  Row(
                    children: [
                      Column(
                        children: [
                          Container(
                            child: Text(
                              '89 PAC',
                              style: TextStyle(
                                fontSize: 23,
                                color: Colors.black,
                              ), //Textstyle
                            ),
                          ),
                          Text(
                            '93 SHO',
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                            ), //Textstyle
                          ),
                          Text(
                            '81 PAS',
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                            ), //Textstyle
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      Column(
                        children: [
                          Text(
                            '89 DRI',
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                            ), //Textstyle
                          ),
                          Text(
                            '35 DEF',
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                            ), //Textstyle
                          ),
                          Text(
                            '77 PHY',
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.black,
                            ), //Textstyle
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ), //SizedBox
                ],
              ), //Column
            ), //Padding
          ), //SizedBox
        ), //Card
      ),
    );
  }
}
