import 'package:flutter/material.dart';
import 'package:foreststore/styles/colors.dart';

// class ItemCounterWidget extends StatefulWidget {
//   const ItemCounterWidget({super.key});

//   @override
//   State<ItemCounterWidget> createState() => _ItemCounterWidgetState();
// }

// class _ItemCounterWidgetState extends State<ItemCounterWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }

class ItemCounterWidget extends StatefulWidget {
  final int quantity;
  final Function? onAmountChanged;

  const ItemCounterWidget(
      {super.key, this.onAmountChanged, required this.quantity});

  @override
  _ItemCounterWidgetState createState() => _ItemCounterWidgetState();
}

class _ItemCounterWidgetState extends State<ItemCounterWidget> {
  int amount = 1;

  @override
  void initState() {
    amount = widget.quantity;

    // Do some other stuff
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        iconWidget(Icons.remove,
            iconColor: AppColors.darkGrey, onPressed: decrementAmount),
        const SizedBox(width: 13),
        SizedBox(
            width: 20,
            child: Center(
                child: getText(
                    text: amount.toString(), fontSize: 15, isBold: true))),
        const SizedBox(width: 13),
        iconWidget(Icons.add,
            iconColor: AppColors.primaryColor, onPressed: incrementAmount)
      ],
    );
  }

  void incrementAmount() {
    setState(() {
      amount = amount + 1;
      updateParent();
    });
  }

  void decrementAmount() {
    if (amount <= 0) return;
    setState(() {
      amount = amount - 1;
      updateParent();
    });
  }

  void updateParent() {
    if (widget.onAmountChanged != null) {
      widget.onAmountChanged!(amount);
    }
  }

  Widget iconWidget(IconData iconData, {required Color iconColor, onPressed}) {
    return GestureDetector(
      onTap: () {
        if (onPressed != null) {
          onPressed();
        }
      },
      child: Container(
        height: 35,
        width: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(13),
          border: Border.all(
            color: const Color(0xffE2E2E2),
          ),
        ),
        child: Center(
          child: Icon(
            iconData,
            color: iconColor,
            size: 20,
          ),
        ),
      ),
    );
  }

  Widget getText(
      {required String text,
      required double fontSize,
      bool isBold = false,
      color = Colors.black}) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
        color: color,
      ),
    );
  }
}
