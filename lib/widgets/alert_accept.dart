import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:foreststore/styles/colors.dart';

class AlertAccept extends StatelessWidget {
  final String title;
  final String message;
  const AlertAccept({super.key, required this.title, required this.message});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: Colors.transparent,
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Center(
          child: Container(
              width: MediaQuery.of(context).size.width - 20,
              height: 200,
              color: Colors.transparent,
              child: Center(
                child: miCard(context, title, message),
              )),
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       backgroundColor: Colors.transparent,
  //     ),
  //     backgroundColor: Colors.transparent,
  //     body: BackdropFilter(
  //         filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
  //         child: Center(
  //           child: Container(
  //             width: MediaQuery.of(context).size.width - 20,
  //             height: 200,
  //             color: Colors.white,
  //             child: Center(
  //                 child: ElevatedButton(
  //               onPressed: () => Popup(
  //                 title: 'Oops!',
  //                 message:
  //                     "Looks like there has been a mistake please check later!",
  //                 leftButton: 'Cancel',
  //                 rightButton: 'OK',
  //                 onTapLeftButton: () {},
  //                 onTapRightButton: () {},
  //               ).show(context),
  //               child: const Text('SHOW POPUP'),
  //             )),
  //           ),
  //         )),
  //   );
  // }
}

Card miCard(BuildContext context, String title, String message) {
  return Card(
    // Con esta propiedad modificamos la forma de nuestro card
    // Aqui utilizo RoundedRectangleBorder para proporcionarle esquinas circulares al Card
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),

    // Con esta propiedad agregamos margen a nuestro Card
    // El margen es la separación entre widgets o entre los bordes del widget padre e hijo
    margin: const EdgeInsets.all(15),

    // Con esta propiedad agregamos elevación a nuestro card
    // La sombra que tiene el Card aumentará
    elevation: 10,

    // La propiedad child anida un widget en su interior
    // Usamos columna para ordenar un ListTile y una fila con botones
    child: Column(
      children: <Widget>[
        // Usamos ListTile para ordenar la información del card como titulo, subtitulo e icono
        ListTile(
          contentPadding: const EdgeInsets.fromLTRB(15, 10, 25, 0),
          title: Text(title,
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
          subtitle: Text(message,
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
          //leading: Icon(Icons.home),
        ),

        // Usamos una fila para ordenar los botones del card
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                  onPressed: () => {Navigator.of(context).pop()},
                  style: ElevatedButton.styleFrom(
                    backgroundColor: AppColors.primaryColor,
                    foregroundColor: Colors.white,
                    elevation: 0.0,
                    padding: const EdgeInsets.all(18),
                    visualDensity: VisualDensity.compact,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  child: const Text('Aceptar'))
            ],
          ),
        )
      ],
    ),
  );
}

class Popup {
  final String title;
  final String message;
  final String rightButton;
  final VoidCallback onTapRightButton;
  final String leftButton;
  final VoidCallback onTapLeftButton;

  Popup({
    required this.title,
    required this.message,
    required this.rightButton,
    required this.onTapRightButton,
    required this.leftButton,
    required this.onTapLeftButton,
  });

  show(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return _PopupCall(
            title: title,
            message: message,
            leftButton: leftButton,
            rightButton: rightButton,
            onTapLeftButton: onTapLeftButton,
            onTapRightButton: onTapRightButton);
      },
    );
  }
}

class _PopupCall extends StatefulWidget {
  final String title;
  final String message;
  final String rightButton;
  final VoidCallback onTapRightButton;
  final String leftButton;
  final VoidCallback onTapLeftButton;

  const _PopupCall(
      {required this.title,
      required this.message,
      required this.rightButton,
      required this.onTapRightButton,
      required this.leftButton,
      required this.onTapLeftButton});
  @override
  _PopupCallState createState() => _PopupCallState();
}

class _PopupCallState extends State<_PopupCall> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(24),
        ),
      ),
      backgroundColor: Colors.black87,
      titlePadding: const EdgeInsets.all(0),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              icon: const Icon(
                Icons.close,
                color: Colors.red,
                size: 25,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          Center(
            child: Text(widget.title,
                style: const TextStyle(
                  fontFamily: 'TTNorms',
                  fontWeight: FontWeight.bold,
                  wordSpacing: 0,
                  letterSpacing: 0,
                  fontSize: 25,
                  color: Colors.yellow,
                )),
          ),
        ],
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.message,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontFamily: 'TTNorms',
              fontWeight: FontWeight.w400,
              wordSpacing: 0,
              letterSpacing: 0,
              fontSize: 15,
              color: Colors.yellow,
            ),
          ),
        ],
      ),
      actions: [
        if (widget.leftButton != null)
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Center(
              child: GestureDetector(
                onTap: () {
                  widget.onTapLeftButton?.call();
                  Navigator.pop(context);
                },
                child: Container(
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.yellow, width: 2.0),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25),
                    ),
                    color: Colors.blue,
                  ),
                  child: const Center(
                    child: Text(
                      'Okay',
                      style: TextStyle(
                        fontFamily: 'TTNorms',
                        fontWeight: FontWeight.bold,
                        wordSpacing: 0,
                        letterSpacing: 0,
                        fontSize: 15,
                        color: Colors.yellow,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        Padding(
          padding: const EdgeInsets.only(
            bottom: 10.0,
            left: 10,
          ),
          child: Center(
            child: GestureDetector(
              onTap: () {
                widget.onTapRightButton?.call();
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                child: Center(
                  child: Text(
                    widget.rightButton.toUpperCase(),
                    style: const TextStyle(
                      fontFamily: 'TTNorms',
                      fontWeight: FontWeight.bold,
                      wordSpacing: 0,
                      letterSpacing: 0,
                      fontSize: 15,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
