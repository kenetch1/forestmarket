import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:foreststore/database/database_services.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/models/explore/categories_model.dart';
import 'package:foreststore/models/product/products_model.dart';
import 'package:foreststore/notification/notification.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:foreststore/repositories/explore/explore_repository_implement.dart';
import 'package:foreststore/repositories/home/categories_repository.dart';
import 'package:foreststore/repositories/home/exclusives_repository.dart';
import 'package:foreststore/repositories/home/firebase_categories_repository.dart';
import 'package:foreststore/repositories/home/firebase_exclusives_repository.dart';
import 'package:foreststore/repositories/home/firebase_products_repository.dart';
import 'package:foreststore/repositories/home/home_repository.dart';
import 'package:foreststore/repositories/home/home_repository_implement.dart';
import 'package:foreststore/repositories/home/products_repository.dart';
import 'package:foreststore/repositories/orders/firebase_orders_repository.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';
import 'package:foreststore/repositories/users/firebase_users_repository.dart';
import 'package:foreststore/repositories/users/users_repository.dart';
import 'package:foreststore/screens/account/details/details_screen.dart';
import 'package:foreststore/screens/clients/client/client_screen.dart';
import 'package:foreststore/screens/clients/order/order_screen.dart';
import 'package:foreststore/screens/clients/product/product_delivery_screen.dart';
import 'package:foreststore/screens/login/login_screen.dart';
import 'package:foreststore/screens/register/register_screen.dart';
import 'package:foreststore/screens/splash_screen.dart';
import 'package:foreststore/styles/theme.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:foreststore/widgets/dialog_yes_or_no.dart';
import 'package:foreststore/widgets/alert_accept.dart';

// @pragma('vm:entry-point')
// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   await Firebase.initializeApp(
//       options: const FirebaseOptions(
//           apiKey: "AIzaSyDp60cLPP5yY769kaq5EN9biP4dkuSsLdI",
//           appId: "1:101762174137:web:30c271ae6c995fad5fdf68",
//           messagingSenderId: "101762174137",
//           projectId: "headbartender-9f04e",
//           storageBucket: "gs://headbartender-9f04e.appspot.com"));
//   await setupFlutterNotifications();
//   showFlutterNotification(message);
//   // If you're going to use other Firebase services in the background, such as Firestore,
//   // make sure you call `initializeApp` before using other Firebase services.
//   debugPrint('Handling a background message ${message.messageId}');
// }

// /// Create a [AndroidNotificationChannel] for heads up notifications
// late AndroidNotificationChannel channel;

// bool isFlutterLocalNotificationsInitialized = false;

// Future<void> setupFlutterNotifications() async {
//   if (isFlutterLocalNotificationsInitialized) {
//     return;
//   }
//   channel = const AndroidNotificationChannel(
//     'high_importance_channel', // id
//     'High Importance Notifications', // title
//     description:
//         'This channel is used for important notifications.', // description
//     importance: Importance.high,
//   );

//   flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

//   /// Create an Android Notification Channel.
//   ///
//   /// We use this channel in the `AndroidManifest.xml` file to override the
//   /// default FCM channel to enable heads up notifications.
//   await flutterLocalNotificationsPlugin
//       .resolvePlatformSpecificImplementation<
//           AndroidFlutterLocalNotificationsPlugin>()
//       ?.createNotificationChannel(channel);

//   /// Update the iOS foreground notification presentation options to allow
//   /// heads up notifications.
//   await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
//     alert: true,
//     badge: true,
//     sound: true,
//   );
//   isFlutterLocalNotificationsInitialized = true;
// }

// void showFlutterNotification(RemoteMessage message) {
//   RemoteNotification? notification = message.notification;
//   AndroidNotification? android = message.notification?.android;
//   if (notification != null && android != null && !kIsWeb) {
//     debugPrint(message.notification!.body);
//     flutterLocalNotificationsPlugin.show(
//       notification.hashCode,
//       notification.title,
//       notification.body,
//       NotificationDetails(
//         android: AndroidNotificationDetails(
//           channel.id,
//           channel.name,
//           channelDescription: channel.description,
//           // TODO add a proper drawable resource to android, for now using
//           //      one that already exists in example app.
//           icon: 'launch_background',
//         ),
//       ),
//     );
//   }
// }

// /// Initialize the [FlutterLocalNotificationsPlugin] package.
// late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyDp60cLPP5yY769kaq5EN9biP4dkuSsLdI",
          appId: "1:101762174137:web:30c271ae6c995fad5fdf68",
          messagingSenderId: "101762174137",
          projectId: "headbartender-9f04e",
          storageBucket: "gs://headbartender-9f04e.appspot.com"));
  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // if (!kIsWeb) {
  //   await setupFlutterNotifications();
  // }
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(const MyApp());
  });

  //runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bosques Market',
      theme: themeData,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      //home: const MyHomePage(title: "Test")
      home: const SplashScreen(),
      //home: const LoginScreen(),
      //home: const AlertAccept(title: 'Noficacion', message: 'Valores')
      //home: const ProductDeliveryScreen(client: 'UserName')
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  //ProductsRepository repository = FirebaseProductsRepository();
  //ExclusivesRepository repository = FirebaseExclusivesRepository();
  //HomeRepository repository = HomeRepositoryImplement();
  //CategoriesRepository repository = FirebaseCategoriesRepository();
  //ExploreRepository repository = ExploreRepositoryImplement();
  //final DatabaseService _databaseService = DatabaseService();
  //OrdersRepository repository = FirebaseOrdersRepository();
  UsersRepository repository = FirebaseUsersRepository();

  Future<void> _incrementCounter() async {
    //Future<Stream<List<Products>>> stream = repository
    //    .productsById(['GkvQiIrZIN5XqXXnHumg'], ['DrP2033B5ptfWtmK6Y3o']);
    // Stream<List<CategoryItem>> stream =
    //     repository.subcategories("Duy02A2yafQgewcvrpet");
    // //Future<Stream<List<String>>> stream = repository.stream.then((value) {

    // stream.listen((value) {
    //   for (var element in value) {
    //     debugPrint(element.name);
    //     debugPrint(element.imagePath);
    //     debugPrint(element.id);
    //     setState(() {
    //       // This call to setState tells the Flutter framework that something has
    //       // changed in this State, which causes it to rerun the build method below
    //       // so that the display can reflect the updated values. If we changed
    //       // _counter without calling setState(), then the build method would not be
    //       // called again, and so nothing would appear to happen.
    //       _counter++;
    //     });
    //   }
    // });

    // stream.forEach((offer) {
    //   debugPrint('${offer.toList()}');
    // });

    // repository.offers().then((value) => {
    //       value.listen((x) {
    //         for (var element in x) {
    //           ControllerHome controller = ControllerHome(offers: element);
    //           debugPrint(controller.offers.title);
    //         }
    //       }),
    //       setState(() {
    //         // This call to setState tells the Flutter framework that something has
    //         // changed in this State, which causes it to rerun the build method below
    //         // so that the display can reflect the updated values. If we changed
    //         // _counter without calling setState(), then the build method would not be
    //         // called again, and so nothing would appear to happen.
    //         _counter++;
    //       })
    //     });

    // repository.exclusive().listen((event) {
    //   for (var element in event) {
    //     element.then((value) {
    //       debugPrint(value.name);
    //     });
    //   }
    // });
    //Seccion de base de datos
    //_onUserInsert();
    // _onSelectUsers().then((value) {
    //   for (var element in value) {
    //     debugPrint("${element.id} - ${element.username}");
    //   }
    // });
    // OrdersEntity entity = const OrdersEntity(
    //     "",
    //     "FV6ixPjxlba8X3RVRBbr",
    //     "Order",
    //     "1",
    //     "En tienda",
    //     "1",
    //     "Ordenado",
    //     "23.0",
    //     "30 min",
    //     "true",
    //     "1",
    //     "true",
    //     "20/01/2023",
    //     "20/01/2023",
    //     "Gerardo");

    // repository.transactionOrder(entity).then((value) {
    //   debugPrint("Salvado");
    // });

    // repository.orders("FV6ixPjxlba8X3RVRBbr").listen((event) {
    //   for (var element in event) {
    //     debugPrint(element.id);
    //   }
    // });
    // var ages = {"John": 26, "Krishna": 34, "Rahul": 67, "Maichel": 33};
    // String jsonstringmap = json.encode(ages);
    // debugPrint(jsonstringmap);

    // _databaseService.carts().then((value) {
    //   for (var item in value) {
    //     debugPrint('$item');
    //   }
    // });

    //int value = await _databaseService.getCountCart();
    //debugPrint('$value');
    // repository.orderByUser().then((value) {
    //   for (var item in value) {
    //     debugPrint('$item');
    //   }
    // });

    // repository.orders('2fGb5rFMOROqPZStwRge2sb1SW32').listen((value) {
    //   for (var item in value) {
    //     debugPrint('$item');
    //   }
    // });

    // repository.orderProduct('vzDXQiSgGpGauO2OcJM8').listen((value) {
    //   debugPrint('$value');
    // });
    //RIh53NGsMtvUbT7BOqNm
    repository.userDeliveries().listen((event) {
      for (var item in event) {
        debugPrint('${item.token}');
      }
    });
  }

  // Call this function to insert a new users
  // Future<void> _onUserInsert() async {
  //   await _databaseService.insertUsers(const UsersEntity(
  //       code: "User0001",
  //       username: "Alberto Quintana",
  //       phoneNumber: "44555555",
  //       email: "a@aa.com",
  //       user: "Gerardo"));
  // }

  // Call this function to delete a breed
  // Future<List<UsersEntity>> _onSelectUsers() async {
  //   return await _databaseService.users();
  // }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
