import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class NotificationContext {
  final BuildContext context;
  final String title;
  final String message;

  NotificationContext(
    this.context,
    this.title,
    this.message,
  );
}

Future<void> onBackgroundMessage(RemoteMessage message) async {
  await Firebase.initializeApp();

  if (message.data.containsKey('data')) {
    // Handle data message
    final data = message.data['data'];
  }

  if (message.data.containsKey('notification')) {
    // Handle notification message
    final notification = message.data['notification'];
  }
  // Or do other work.
}

class FCM {
  static BuildContext? context;
  final _firebaseMessaging = FirebaseMessaging.instance;

  final streamCtlr = StreamController<NotificationContext>.broadcast();
  final titleCtlr = StreamController<NotificationContext>.broadcast();
  final bodyCtlr = StreamController<NotificationContext>.broadcast();

  static void setContext(BuildContext context) => FCM.context = context;

  setNotifications() {
    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);

    // handle when app in active state
    forgroundNotification();

    // handle when app running in background state
    backgroundNotification();

    // handle when app completely closed by the user
    terminateNotification();

    // With this token you can test it easily on your phone
    final token =
        _firebaseMessaging.getToken().then((value) => print('Token: $value'));
  }

  forgroundNotification() {
    FirebaseMessaging.onMessage.listen(
      (message) async {
        if (message.data.containsKey('data')) {
          // Handle data message
          debugPrint(message.data['data']);
          streamCtlr.sink
              .add(NotificationContext(context!, 'Data', message.data['data']));
        }
        if (message.data.containsKey('notification')) {
          // Handle notification message
          streamCtlr.sink.add(NotificationContext(
              context!, 'Notificacion', message.data['notification']));
        }
        // Or do other work.
        titleCtlr.sink.add(NotificationContext(context!,
            message.notification!.title!, message.notification!.body!));
        bodyCtlr.sink.add(NotificationContext(context!,
            message.notification!.title!, message.notification!.body!));
      },
    );
  }

  backgroundNotification() {
    FirebaseMessaging.onMessageOpenedApp.listen(
      (message) async {
        if (message.data.containsKey('data')) {
          // Handle data message
          streamCtlr.sink
              .add(NotificationContext(context!, 'Data', message.data['data']));
        }
        if (message.data.containsKey('notification')) {
          // Handle notification message
          streamCtlr.sink.add(NotificationContext(
              context!, 'Notificacion', message.data['notification']));
        }
        // Or do other work.
        titleCtlr.sink.add(NotificationContext(context!,
            message.notification!.title!, message.notification!.body!));
        bodyCtlr.sink.add(NotificationContext(context!,
            message.notification!.title!, message.notification!.body!));
      },
    );
  }

  terminateNotification() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      if (initialMessage.data.containsKey('data')) {
        // Handle data message
        streamCtlr.sink.add(
            NotificationContext(context!, 'Data', initialMessage.data['data']));
      }
      if (initialMessage.data.containsKey('notification')) {
        // Handle notification message
        streamCtlr.sink.add(NotificationContext(
            context!, 'Notificacion', initialMessage.data['notification']));
      }
      // Or do other work.
      titleCtlr.sink.add(NotificationContext(
          context!,
          initialMessage.notification!.title!,
          initialMessage.notification!.body!));
      bodyCtlr.sink.add(NotificationContext(
          context!,
          initialMessage.notification!.title!,
          initialMessage.notification!.body!));
    }
  }

  dispose() {
    streamCtlr.close();
    bodyCtlr.close();
    titleCtlr.close();
  }
}
