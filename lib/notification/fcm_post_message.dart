import 'dart:async';
import 'dart:convert' show Encoding, json;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

enum Eventos {
  ordenado,
  empaquetado,
  entregado,
}

class FcmPostMessage {
  final postUrl = 'https://fcm.googleapis.com/fcm/send';

  Future<bool> sendOrdenado(String name, String total, String token) async {
    var ordenado = {
      "notification": {"body": "$name \$$total", "title": "Pedido"},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
      },
      "to": token,
      "collapse_key": "type_a"
    };
    return sendNotification(ordenado);
  }

  Future<bool> sendEmpaquetado(String token) async {
    var empaquetado = {
      "notification": {
        "body": "Puede recoger en tienda",
        "title": "Empaquetado"
      },
      "priority": "high",
      "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK"},
      "to": token,
      "collapse_key": "type_a"
    };
    return sendNotification(empaquetado);
  }

  Future<bool> sendEntregado(String token) async {
    var entregado = {
      "notification": {
        "body": "Esperamos verlo pronto, gracias",
        "title": "Entregado"
      },
      "priority": "high",
      "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK"},
      "to": "token",
      "collapse_key": "type_a"
    };
    return sendNotification(entregado);
  }

  Future<bool> sendNotification(Map<String, Object> notification) async {
    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAF7F_jLk:APA91bE_qAtyWWT62a1GEDQbCKTM2w8WtpZgJ9I3XBuDdQGKRfgvsKdkiiqujgYWK-_dldu4gHx3Rw6ZdcVKsxNO9t5u_C9TzHVCBjJXXi28MntRc2jQvHdQGEMmZEFp8zMjPD8PEtM6'
    };

    // debugPrint('$notification');

    // return true;
    final response = await http.post(Uri.parse(postUrl),
        body: json.encode(notification),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do sth
      return true;
    } else {
      // on failure do sth
      return false;
    }
  }
}
