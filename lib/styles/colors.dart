import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  //One instance, needs factory
  static final AppColors _instance = AppColors();
  factory AppColors() => _instance;

  AppColors._();

  static const primaryColor = Color(0xff53B175);
  static const darkGrey = Color(0xff7C7C7C);
  static const blueColor = Colors.blue;
  static const orangeColor = Color.fromARGB(255, 237, 145, 8);
}
