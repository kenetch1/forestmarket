import 'dart:async';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/repositories/explore/explore_repository_implement.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/explore/filter_screen.dart';
import 'package:foreststore/screens/products/category_items_screen.dart';
import 'package:foreststore/screens/products/product_item_screen.dart';
import 'package:foreststore/screens/subcategories/bloc/subcategories_bloc.dart';
import 'package:foreststore/widgets/category_item_card_widget.dart';
import 'package:foreststore/widgets/search_bar_widget.dart';

List<Color> gridColors = [
  const Color(0xff53B175),
  const Color(0xffF8A44C),
  const Color(0xffF7A593),
  const Color(0xffD3B0E0),
  const Color(0xffFDE598),
  const Color(0xffB7DFF5),
  const Color(0xff836AF6),
  const Color(0xffD73B77),
];

class SubcategoriesScreen extends StatefulWidget {
  final CategoryItem categoryItem;
  const SubcategoriesScreen({super.key, required this.categoryItem});

  @override
  State<SubcategoriesScreen> createState() => _SubcategoriesScreenState();
}

class _SubcategoriesScreenState extends State<SubcategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SubcategoriesBloc>(create: (context) {
      return SubcategoriesBloc(ExploreRepositoryImplement())
        ..add(SubcategoriesEvent.started(widget.categoryItem.id));
    }, child: BlocBuilder<SubcategoriesBloc, SubcategoriesState>(
      builder: (contextBloc, state) {
        if (state.isLoading) {
          return const Center(
              child: CircularProgressIndicator(
            backgroundColor: Colors.black26,
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.black, //<-- SEE HERE
            ),
          ));
        } else {
          return buildPage(contextBloc, state);
        }
      },
    ));
  }

  Widget buildPage(BuildContext context, SubcategoriesState state) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.only(left: 25),
            child: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        title: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          child: AppText(
            text: widget.categoryItem.name,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        actions: <Widget>[
          Badge(
              position: BadgePosition.topEnd(top: 7, end: 7),
              badgeContent: Text(
                '${state.count}',
                style: const TextStyle(color: Colors.white, fontSize: 10),
              ),
              child: IconButton(
                  icon: const Icon(Icons.shopping_cart, color: Colors.black),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const DashboardScreen(index: 2);
                      },
                    ));
                  }))
        ],
      ),
      body: SafeArea(
          child: Column(
        children: [
          getHeader(context),
          Expanded(
            child: getStaggeredGridView(context, state),
          ),
        ],
      )),
    );
  }

  Widget getHeader(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: GestureDetector(
              child: const SearchBarWidget(),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (contextFilter) => FilterScreen(
                            back: false,
                            onPressedForProd: (prod) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (contextProd) => ProductItemsScreen(
                                    product: prod,
                                  ),
                                ),
                              );
                            },
                          )),
                );
              }),
        ),
      ],
    );
  }

  Widget getStaggeredGridView(BuildContext context, SubcategoriesState state) {
    return StaggeredGridView.count(
      crossAxisCount: 4,
      //Here is the place that we are getting flexible/ dynamic card for various images
      staggeredTiles: state.subcategories
          .map<StaggeredTile>((_) => const StaggeredTile.fit(2))
          .toList(),
      mainAxisSpacing: 3.0,
      crossAxisSpacing: 4.0,
      children: state.subcategories.asMap().entries.map<Widget>((e) {
        int index = e.key;
        CategoryItem categoryItem = e.value;
        return GestureDetector(
          onTap: () {
            onCategoryItemClicked(context, categoryItem);
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            child: CategoryItemCardWidget(
              item: categoryItem,
              color: gridColors[index % gridColors.length],
            ),
          ),
        );
      }).toList(), // add some space
    );
  }

  void onCategoryItemClicked(
      BuildContext contextBloc, CategoryItem categoryItem) {
    Navigator.of(contextBloc).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return CategoryItemsScreen(categoryItem: categoryItem);
      },
    )).then((value) => onGoBack(contextBloc));
  }

  void onGoBack(BuildContext context) {
    context
        .read<SubcategoriesBloc>()
        .add(const SubcategoriesEvent.countBadge());
  }
}
