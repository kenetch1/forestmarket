part of 'subcategories_bloc.dart';

@freezed
class SubcategoriesEvent with _$SubcategoriesEvent {
  const factory SubcategoriesEvent.started(String categoryId) = _Started;
  const factory SubcategoriesEvent.countBadge() = _Count;
}
