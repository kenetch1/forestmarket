part of 'subcategories_bloc.dart';

@freezed
class SubcategoriesState with _$SubcategoriesState {
  const factory SubcategoriesState(
      {required List<CategoryItem> subcategories,
      required int count,
      @Default(true) isLoading,
      @Default(false) showErrorMessage}) = _SubCategoriesStateBinding;

  factory SubcategoriesState.initial() => _SubCategoriesStateBinding(
      subcategories: [CategoryItem(id: '', name: '', imagePath: '')], count: 0);
}
