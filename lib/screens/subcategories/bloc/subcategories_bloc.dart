import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:foreststore/database/database_services.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'subcategories_event.dart';
part 'subcategories_state.dart';
part 'subcategories_bloc.freezed.dart';

class SubcategoriesBloc extends Bloc<SubcategoriesEvent, SubcategoriesState> {
  final DatabaseService _databaseService = DatabaseService();
  final ExploreRepository exploreRepository;

  SubcategoriesBloc(this.exploreRepository)
      : super(SubcategoriesState.initial()) {
    on<SubcategoriesEvent>((event, emit) async {
      await event.when<FutureOr<void>>(
          started: (categoryId) => _onStarted(emit, categoryId),
          countBadge: () => _onCount(emit));
    });
  }

  void _onStarted(Emitter<SubcategoriesState> emit, String categoryId) async {
    int value = await _databaseService.getCountCart();
    await emit.forEach(exploreRepository.subcategories(categoryId),
        onData: (List<CategoryItem> subcategories) {
      return state.copyWith(
          subcategories: subcategories, count: value, isLoading: false);
    });
  }

  void _onCount(Emitter<SubcategoriesState> emit) async {
    int value = await _databaseService.getCountCart();
    emit(
      state.copyWith(count: value),
    );
  }

  // void _onFetchDataEvent(
  //   FetchDataEvent event,
  //   Emitter<HomeState> emitter,
  // ) async {
  //   emitter(const HomeLoadingState());
  //   await Future.delayed(const Duration(seconds: 2));
  //   bool isSucceed = Random().nextBool();
  //   if (isSucceed) {
  //     List<Food> _dummyFoods = FoodGenerator.generateDummyFoods();
  //     emitter(HomeSuccessFetchDataState(foods: _dummyFoods));
  //   } else {
  //     emitter(const HomeErrorFetchDataState(
  //       errorMessage: "something went very wrong :(",
  //     ));
  //   }
  // }
}
