// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'subcategories_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SubcategoriesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String categoryId) started,
    required TResult Function() countBadge,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String categoryId)? started,
    TResult? Function()? countBadge,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String categoryId)? started,
    TResult Function()? countBadge,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_Count value) countBadge,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_Count value)? countBadge,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_Count value)? countBadge,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SubcategoriesEventCopyWith<$Res> {
  factory $SubcategoriesEventCopyWith(
          SubcategoriesEvent value, $Res Function(SubcategoriesEvent) then) =
      _$SubcategoriesEventCopyWithImpl<$Res, SubcategoriesEvent>;
}

/// @nodoc
class _$SubcategoriesEventCopyWithImpl<$Res, $Val extends SubcategoriesEvent>
    implements $SubcategoriesEventCopyWith<$Res> {
  _$SubcategoriesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
  @useResult
  $Res call({String categoryId});
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$SubcategoriesEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categoryId = null,
  }) {
    return _then(_$_Started(
      null == categoryId
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started(this.categoryId);

  @override
  final String categoryId;

  @override
  String toString() {
    return 'SubcategoriesEvent.started(categoryId: $categoryId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Started &&
            (identical(other.categoryId, categoryId) ||
                other.categoryId == categoryId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, categoryId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      __$$_StartedCopyWithImpl<_$_Started>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String categoryId) started,
    required TResult Function() countBadge,
  }) {
    return started(categoryId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String categoryId)? started,
    TResult? Function()? countBadge,
  }) {
    return started?.call(categoryId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String categoryId)? started,
    TResult Function()? countBadge,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(categoryId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_Count value) countBadge,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_Count value)? countBadge,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_Count value)? countBadge,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements SubcategoriesEvent {
  const factory _Started(final String categoryId) = _$_Started;

  String get categoryId;
  @JsonKey(ignore: true)
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CountCopyWith<$Res> {
  factory _$$_CountCopyWith(_$_Count value, $Res Function(_$_Count) then) =
      __$$_CountCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CountCopyWithImpl<$Res>
    extends _$SubcategoriesEventCopyWithImpl<$Res, _$_Count>
    implements _$$_CountCopyWith<$Res> {
  __$$_CountCopyWithImpl(_$_Count _value, $Res Function(_$_Count) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Count implements _Count {
  const _$_Count();

  @override
  String toString() {
    return 'SubcategoriesEvent.countBadge()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Count);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String categoryId) started,
    required TResult Function() countBadge,
  }) {
    return countBadge();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String categoryId)? started,
    TResult? Function()? countBadge,
  }) {
    return countBadge?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String categoryId)? started,
    TResult Function()? countBadge,
    required TResult orElse(),
  }) {
    if (countBadge != null) {
      return countBadge();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_Count value) countBadge,
  }) {
    return countBadge(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_Count value)? countBadge,
  }) {
    return countBadge?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_Count value)? countBadge,
    required TResult orElse(),
  }) {
    if (countBadge != null) {
      return countBadge(this);
    }
    return orElse();
  }
}

abstract class _Count implements SubcategoriesEvent {
  const factory _Count() = _$_Count;
}

/// @nodoc
mixin _$SubcategoriesState {
  List<CategoryItem> get subcategories => throw _privateConstructorUsedError;
  int get count => throw _privateConstructorUsedError;
  dynamic get isLoading => throw _privateConstructorUsedError;
  dynamic get showErrorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SubcategoriesStateCopyWith<SubcategoriesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SubcategoriesStateCopyWith<$Res> {
  factory $SubcategoriesStateCopyWith(
          SubcategoriesState value, $Res Function(SubcategoriesState) then) =
      _$SubcategoriesStateCopyWithImpl<$Res, SubcategoriesState>;
  @useResult
  $Res call(
      {List<CategoryItem> subcategories,
      int count,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class _$SubcategoriesStateCopyWithImpl<$Res, $Val extends SubcategoriesState>
    implements $SubcategoriesStateCopyWith<$Res> {
  _$SubcategoriesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subcategories = null,
    Object? count = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      subcategories: null == subcategories
          ? _value.subcategories
          : subcategories // ignore: cast_nullable_to_non_nullable
              as List<CategoryItem>,
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      isLoading: freezed == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as dynamic,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SubCategoriesStateBindingCopyWith<$Res>
    implements $SubcategoriesStateCopyWith<$Res> {
  factory _$$_SubCategoriesStateBindingCopyWith(
          _$_SubCategoriesStateBinding value,
          $Res Function(_$_SubCategoriesStateBinding) then) =
      __$$_SubCategoriesStateBindingCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<CategoryItem> subcategories,
      int count,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class __$$_SubCategoriesStateBindingCopyWithImpl<$Res>
    extends _$SubcategoriesStateCopyWithImpl<$Res, _$_SubCategoriesStateBinding>
    implements _$$_SubCategoriesStateBindingCopyWith<$Res> {
  __$$_SubCategoriesStateBindingCopyWithImpl(
      _$_SubCategoriesStateBinding _value,
      $Res Function(_$_SubCategoriesStateBinding) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subcategories = null,
    Object? count = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_$_SubCategoriesStateBinding(
      subcategories: null == subcategories
          ? _value._subcategories
          : subcategories // ignore: cast_nullable_to_non_nullable
              as List<CategoryItem>,
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      isLoading: freezed == isLoading ? _value.isLoading! : isLoading,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage!
          : showErrorMessage,
    ));
  }
}

/// @nodoc

class _$_SubCategoriesStateBinding implements _SubCategoriesStateBinding {
  const _$_SubCategoriesStateBinding(
      {required final List<CategoryItem> subcategories,
      required this.count,
      this.isLoading = true,
      this.showErrorMessage = false})
      : _subcategories = subcategories;

  final List<CategoryItem> _subcategories;
  @override
  List<CategoryItem> get subcategories {
    if (_subcategories is EqualUnmodifiableListView) return _subcategories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_subcategories);
  }

  @override
  final int count;
  @override
  @JsonKey()
  final dynamic isLoading;
  @override
  @JsonKey()
  final dynamic showErrorMessage;

  @override
  String toString() {
    return 'SubcategoriesState(subcategories: $subcategories, count: $count, isLoading: $isLoading, showErrorMessage: $showErrorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SubCategoriesStateBinding &&
            const DeepCollectionEquality()
                .equals(other._subcategories, _subcategories) &&
            (identical(other.count, count) || other.count == count) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.showErrorMessage, showErrorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_subcategories),
      count,
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(showErrorMessage));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SubCategoriesStateBindingCopyWith<_$_SubCategoriesStateBinding>
      get copyWith => __$$_SubCategoriesStateBindingCopyWithImpl<
          _$_SubCategoriesStateBinding>(this, _$identity);
}

abstract class _SubCategoriesStateBinding implements SubcategoriesState {
  const factory _SubCategoriesStateBinding(
      {required final List<CategoryItem> subcategories,
      required final int count,
      final dynamic isLoading,
      final dynamic showErrorMessage}) = _$_SubCategoriesStateBinding;

  @override
  List<CategoryItem> get subcategories;
  @override
  int get count;
  @override
  dynamic get isLoading;
  @override
  dynamic get showErrorMessage;
  @override
  @JsonKey(ignore: true)
  _$$_SubCategoriesStateBindingCopyWith<_$_SubCategoriesStateBinding>
      get copyWith => throw _privateConstructorUsedError;
}
