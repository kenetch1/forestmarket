import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/helpers/column_with_seprator.dart';
import 'package:foreststore/screens/account/details/details_screen.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/welcome_screen.dart';
import 'package:foreststore/styles/colors.dart';
import 'package:foreststore/widgets/CustomDialogBox.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'account_item.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            ListTile(
              leading: SizedBox(width: 65, height: 65, child: getImageHeader()),
              title: const AppText(
                text: "Mohammed Hashim",
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
              subtitle: const AppText(
                text: "github.com/mohammedhashim44",
                color: Color(0xff7C7C7C),
                fontWeight: FontWeight.normal,
                fontSize: 16,
              ),
            ),
            Column(
              children: getChildrenWithSeperator(
                widgets: accountItems.map((e) {
                  return getAccountItemWidget(context, e);
                }).toList(),
                seperator: const Divider(
                  thickness: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            logoutButton(context),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget logoutButton(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.symmetric(horizontal: 25),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color(0xffF2F3F2),
          foregroundColor: Colors.white,
          elevation: 0.0,
          padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 25),
          visualDensity: VisualDensity.compact,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 20,
              height: 20,
              child: SvgPicture.asset(
                "assets/icons/account_icons/logout_icon.svg",
              ),
            ),
            GestureDetector(
              onTap: () {
                SharedPreferences.getInstance().then((pref) {
                  pref.remove("user");
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (BuildContext context) {
                    return WelcomeScreen();
                  }));
                });
              },
              child: const Text(
                "Log Out",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: AppColors.primaryColor),
              ),
            ),
            Container()
          ],
        ),
        onPressed: () {},
      ),
    );
  }

  Widget getImageHeader() {
    //String imagePath = "assets/images/account_image.jpg";
    return const Icon(Icons.person_rounded,
        size: 60, color: AppColors.orangeColor);
    // CircleAvatar(
    //   radius: 5.0,
    //   backgroundImage: AssetImage(imagePath),
    //   backgroundColor: AppColors.primaryColor.withOpacity(0.7),
    // );
  }

  Widget getAccountItemWidget(BuildContext context, AccountItem accountItem) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: SvgPicture.asset(
              accountItem.iconPath,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {
              switch (accountItem.id) {
                case 0:
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) {
                      return DashboardScreen(index: 3);
                    },
                  ));
                  break;
                case 1:
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) {
                      return const DetailScreen();
                    },
                  ));
                  break;
                case 2:
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomDialogBox(
                          title: AppLocalizations.of(context)!.account_delivery,
                          descriptions: AppLocalizations.of(context)!
                              .account_delivery_msg,
                          text: AppLocalizations.of(context)!.title_accept,
                        );
                      });
                  break;
                case 3:
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomDialogBox(
                          title: AppLocalizations.of(context)!.account_payment,
                          descriptions:
                              AppLocalizations.of(context)!.account_payment_msg,
                          text: AppLocalizations.of(context)!.title_accept,
                        );
                      });
                  break;
                case 4:
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomDialogBox(
                          title: AppLocalizations.of(context)!.account_help,
                          descriptions:
                              AppLocalizations.of(context)!.account_help_msg,
                          text: AppLocalizations.of(context)!.title_accept,
                        );
                      });
                  break;
                default:
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomDialogBox(
                          title: AppLocalizations.of(context)!.account_about,
                          descriptions:
                              AppLocalizations.of(context)!.account_about_msg,
                          text: AppLocalizations.of(context)!.title_accept,
                        );
                      });
              }
            },
            child: Text(
              accountItem.label,
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          const Spacer(),
          const Icon(Icons.arrow_forward_ios)
        ],
      ),
    );
  }
}
