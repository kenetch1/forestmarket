class AccountItem {
  final int id;
  final String label;
  final String iconPath;

  AccountItem(this.label, this.iconPath, this.id);
}

List<AccountItem> accountItems = [
  AccountItem("Ordenes", "assets/icons/account_icons/orders_icon.svg", 0),
  AccountItem("Perfil", "assets/icons/account_icons/details_icon.svg", 1),
  AccountItem("Entrega", "assets/icons/account_icons/delivery_icon.svg", 2),
  AccountItem("Método pago", "assets/icons/account_icons/payment_icon.svg", 3),
  //AccountItem("Promo Card", "assets/icons/account_icons/promo_icon.svg", 4),
  //AccountItem("Notifications", "assets/icons/account_icons/notification_icon.svg", 5),
  AccountItem("Ayuda", "assets/icons/account_icons/help_icon.svg", 4),
  AccountItem("Acerca de", "assets/icons/account_icons/about_icon.svg", 5),
];
