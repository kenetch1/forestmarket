import 'package:freezed_annotation/freezed_annotation.dart';

part 'value_failure.freezed.dart';

@freezed
class ValueFailure with _$ValueFailure {
  const factory ValueFailure.invalidUserName({
    required String failedValue,
  }) = _InvalidUserName;

  const factory ValueFailure.invalidEmail({
    required String failedValue,
  }) = _InvalidEmail;

  const factory ValueFailure.invalidPhone({
    required String failedValue,
  }) = _InvalidPhone;
}
