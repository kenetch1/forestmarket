// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'value_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ValueFailure {
  String get failedValue => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidUserName,
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) invalidPhone,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidUserName,
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? invalidPhone,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidUserName,
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? invalidPhone,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidUserName value) invalidUserName,
    required TResult Function(_InvalidEmail value) invalidEmail,
    required TResult Function(_InvalidPhone value) invalidPhone,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidUserName value)? invalidUserName,
    TResult? Function(_InvalidEmail value)? invalidEmail,
    TResult? Function(_InvalidPhone value)? invalidPhone,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidUserName value)? invalidUserName,
    TResult Function(_InvalidEmail value)? invalidEmail,
    TResult Function(_InvalidPhone value)? invalidPhone,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<ValueFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<$Res> {
  factory $ValueFailureCopyWith(
          ValueFailure value, $Res Function(ValueFailure) then) =
      _$ValueFailureCopyWithImpl<$Res, ValueFailure>;
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class _$ValueFailureCopyWithImpl<$Res, $Val extends ValueFailure>
    implements $ValueFailureCopyWith<$Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_value.copyWith(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InvalidUserNameCopyWith<$Res>
    implements $ValueFailureCopyWith<$Res> {
  factory _$$_InvalidUserNameCopyWith(
          _$_InvalidUserName value, $Res Function(_$_InvalidUserName) then) =
      __$$_InvalidUserNameCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$_InvalidUserNameCopyWithImpl<$Res>
    extends _$ValueFailureCopyWithImpl<$Res, _$_InvalidUserName>
    implements _$$_InvalidUserNameCopyWith<$Res> {
  __$$_InvalidUserNameCopyWithImpl(
      _$_InvalidUserName _value, $Res Function(_$_InvalidUserName) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$_InvalidUserName(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InvalidUserName implements _InvalidUserName {
  const _$_InvalidUserName({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueFailure.invalidUserName(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvalidUserName &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InvalidUserNameCopyWith<_$_InvalidUserName> get copyWith =>
      __$$_InvalidUserNameCopyWithImpl<_$_InvalidUserName>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidUserName,
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) invalidPhone,
  }) {
    return invalidUserName(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidUserName,
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? invalidPhone,
  }) {
    return invalidUserName?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidUserName,
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidUserName != null) {
      return invalidUserName(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidUserName value) invalidUserName,
    required TResult Function(_InvalidEmail value) invalidEmail,
    required TResult Function(_InvalidPhone value) invalidPhone,
  }) {
    return invalidUserName(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidUserName value)? invalidUserName,
    TResult? Function(_InvalidEmail value)? invalidEmail,
    TResult? Function(_InvalidPhone value)? invalidPhone,
  }) {
    return invalidUserName?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidUserName value)? invalidUserName,
    TResult Function(_InvalidEmail value)? invalidEmail,
    TResult Function(_InvalidPhone value)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidUserName != null) {
      return invalidUserName(this);
    }
    return orElse();
  }
}

abstract class _InvalidUserName implements ValueFailure {
  const factory _InvalidUserName({required final String failedValue}) =
      _$_InvalidUserName;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$_InvalidUserNameCopyWith<_$_InvalidUserName> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_InvalidEmailCopyWith<$Res>
    implements $ValueFailureCopyWith<$Res> {
  factory _$$_InvalidEmailCopyWith(
          _$_InvalidEmail value, $Res Function(_$_InvalidEmail) then) =
      __$$_InvalidEmailCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$_InvalidEmailCopyWithImpl<$Res>
    extends _$ValueFailureCopyWithImpl<$Res, _$_InvalidEmail>
    implements _$$_InvalidEmailCopyWith<$Res> {
  __$$_InvalidEmailCopyWithImpl(
      _$_InvalidEmail _value, $Res Function(_$_InvalidEmail) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$_InvalidEmail(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InvalidEmail implements _InvalidEmail {
  const _$_InvalidEmail({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueFailure.invalidEmail(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvalidEmail &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InvalidEmailCopyWith<_$_InvalidEmail> get copyWith =>
      __$$_InvalidEmailCopyWithImpl<_$_InvalidEmail>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidUserName,
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) invalidPhone,
  }) {
    return invalidEmail(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidUserName,
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? invalidPhone,
  }) {
    return invalidEmail?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidUserName,
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidUserName value) invalidUserName,
    required TResult Function(_InvalidEmail value) invalidEmail,
    required TResult Function(_InvalidPhone value) invalidPhone,
  }) {
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidUserName value)? invalidUserName,
    TResult? Function(_InvalidEmail value)? invalidEmail,
    TResult? Function(_InvalidPhone value)? invalidPhone,
  }) {
    return invalidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidUserName value)? invalidUserName,
    TResult Function(_InvalidEmail value)? invalidEmail,
    TResult Function(_InvalidPhone value)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }
}

abstract class _InvalidEmail implements ValueFailure {
  const factory _InvalidEmail({required final String failedValue}) =
      _$_InvalidEmail;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$_InvalidEmailCopyWith<_$_InvalidEmail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_InvalidPhoneCopyWith<$Res>
    implements $ValueFailureCopyWith<$Res> {
  factory _$$_InvalidPhoneCopyWith(
          _$_InvalidPhone value, $Res Function(_$_InvalidPhone) then) =
      __$$_InvalidPhoneCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$_InvalidPhoneCopyWithImpl<$Res>
    extends _$ValueFailureCopyWithImpl<$Res, _$_InvalidPhone>
    implements _$$_InvalidPhoneCopyWith<$Res> {
  __$$_InvalidPhoneCopyWithImpl(
      _$_InvalidPhone _value, $Res Function(_$_InvalidPhone) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$_InvalidPhone(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InvalidPhone implements _InvalidPhone {
  const _$_InvalidPhone({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueFailure.invalidPhone(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvalidPhone &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InvalidPhoneCopyWith<_$_InvalidPhone> get copyWith =>
      __$$_InvalidPhoneCopyWithImpl<_$_InvalidPhone>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidUserName,
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) invalidPhone,
  }) {
    return invalidPhone(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidUserName,
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? invalidPhone,
  }) {
    return invalidPhone?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidUserName,
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidPhone != null) {
      return invalidPhone(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidUserName value) invalidUserName,
    required TResult Function(_InvalidEmail value) invalidEmail,
    required TResult Function(_InvalidPhone value) invalidPhone,
  }) {
    return invalidPhone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidUserName value)? invalidUserName,
    TResult? Function(_InvalidEmail value)? invalidEmail,
    TResult? Function(_InvalidPhone value)? invalidPhone,
  }) {
    return invalidPhone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidUserName value)? invalidUserName,
    TResult Function(_InvalidEmail value)? invalidEmail,
    TResult Function(_InvalidPhone value)? invalidPhone,
    required TResult orElse(),
  }) {
    if (invalidPhone != null) {
      return invalidPhone(this);
    }
    return orElse();
  }
}

abstract class _InvalidPhone implements ValueFailure {
  const factory _InvalidPhone({required final String failedValue}) =
      _$_InvalidPhone;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$_InvalidPhoneCopyWith<_$_InvalidPhone> get copyWith =>
      throw _privateConstructorUsedError;
}
