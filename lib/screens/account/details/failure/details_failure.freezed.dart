// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'details_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DetailsFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidSubmitCombination,
    required TResult Function() serverError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidSubmitCombination,
    TResult? Function()? serverError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidSubmitCombination,
    TResult Function()? serverError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidSubmitCombination value)
        invalidSubmitCombination,
    required TResult Function(_ServerError value) serverError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidSubmitCombination value)?
        invalidSubmitCombination,
    TResult? Function(_ServerError value)? serverError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidSubmitCombination value)? invalidSubmitCombination,
    TResult Function(_ServerError value)? serverError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetailsFailureCopyWith<$Res> {
  factory $DetailsFailureCopyWith(
          DetailsFailure value, $Res Function(DetailsFailure) then) =
      _$DetailsFailureCopyWithImpl<$Res, DetailsFailure>;
}

/// @nodoc
class _$DetailsFailureCopyWithImpl<$Res, $Val extends DetailsFailure>
    implements $DetailsFailureCopyWith<$Res> {
  _$DetailsFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InvalidSubmitCombinationCopyWith<$Res> {
  factory _$$_InvalidSubmitCombinationCopyWith(
          _$_InvalidSubmitCombination value,
          $Res Function(_$_InvalidSubmitCombination) then) =
      __$$_InvalidSubmitCombinationCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InvalidSubmitCombinationCopyWithImpl<$Res>
    extends _$DetailsFailureCopyWithImpl<$Res, _$_InvalidSubmitCombination>
    implements _$$_InvalidSubmitCombinationCopyWith<$Res> {
  __$$_InvalidSubmitCombinationCopyWithImpl(_$_InvalidSubmitCombination _value,
      $Res Function(_$_InvalidSubmitCombination) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_InvalidSubmitCombination implements _InvalidSubmitCombination {
  const _$_InvalidSubmitCombination();

  @override
  String toString() {
    return 'DetailsFailure.invalidSubmitCombination()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InvalidSubmitCombination);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidSubmitCombination,
    required TResult Function() serverError,
  }) {
    return invalidSubmitCombination();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidSubmitCombination,
    TResult? Function()? serverError,
  }) {
    return invalidSubmitCombination?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidSubmitCombination,
    TResult Function()? serverError,
    required TResult orElse(),
  }) {
    if (invalidSubmitCombination != null) {
      return invalidSubmitCombination();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidSubmitCombination value)
        invalidSubmitCombination,
    required TResult Function(_ServerError value) serverError,
  }) {
    return invalidSubmitCombination(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidSubmitCombination value)?
        invalidSubmitCombination,
    TResult? Function(_ServerError value)? serverError,
  }) {
    return invalidSubmitCombination?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidSubmitCombination value)? invalidSubmitCombination,
    TResult Function(_ServerError value)? serverError,
    required TResult orElse(),
  }) {
    if (invalidSubmitCombination != null) {
      return invalidSubmitCombination(this);
    }
    return orElse();
  }
}

abstract class _InvalidSubmitCombination implements DetailsFailure {
  const factory _InvalidSubmitCombination() = _$_InvalidSubmitCombination;
}

/// @nodoc
abstract class _$$_ServerErrorCopyWith<$Res> {
  factory _$$_ServerErrorCopyWith(
          _$_ServerError value, $Res Function(_$_ServerError) then) =
      __$$_ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ServerErrorCopyWithImpl<$Res>
    extends _$DetailsFailureCopyWithImpl<$Res, _$_ServerError>
    implements _$$_ServerErrorCopyWith<$Res> {
  __$$_ServerErrorCopyWithImpl(
      _$_ServerError _value, $Res Function(_$_ServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ServerError implements _ServerError {
  const _$_ServerError();

  @override
  String toString() {
    return 'DetailsFailure.serverError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidSubmitCombination,
    required TResult Function() serverError,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidSubmitCombination,
    TResult? Function()? serverError,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidSubmitCombination,
    TResult Function()? serverError,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidSubmitCombination value)
        invalidSubmitCombination,
    required TResult Function(_ServerError value) serverError,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidSubmitCombination value)?
        invalidSubmitCombination,
    TResult? Function(_ServerError value)? serverError,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidSubmitCombination value)? invalidSubmitCombination,
    TResult Function(_ServerError value)? serverError,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class _ServerError implements DetailsFailure {
  const factory _ServerError() = _$_ServerError;
}
