import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:foreststore/screens/account/details/failure/value_failure.dart';

class UserName extends Equatable {
  factory UserName(String input) => UserName._(_validateUserName(input));

  const UserName._(this.value);

  final Either<ValueFailure, String> value;

  @override
  List<Object?> get props => [value];
}

Either<ValueFailure, String> _validateUserName(String input) {
  const userRegex = r"""^[a-zA-Z0-9 ]+$""";
  if (RegExp(userRegex).hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.invalidUserName(failedValue: input),
    );
  }
}
