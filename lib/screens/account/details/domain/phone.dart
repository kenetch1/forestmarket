import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:foreststore/screens/account/details/failure/value_failure.dart';

class Phone extends Equatable {
  factory Phone(String input) => Phone._(_validatePhone(input));

  const Phone._(this.value);

  final Either<ValueFailure, String> value;

  @override
  List<Object?> get props => [value];
}

Either<ValueFailure, String> _validatePhone(String input) {
  const phoneRegex = r"""^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$""";
  if (RegExp(phoneRegex).hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.invalidPhone(failedValue: input),
    );
  }
}
