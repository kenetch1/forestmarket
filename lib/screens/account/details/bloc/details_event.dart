part of 'details_bloc.dart';

@freezed
class DetailsEvent with _$DetailsEvent {
  const factory DetailsEvent.started() = _Started;
  const factory DetailsEvent.userNameChanged(String emailString) =
      _UserNameChanged;
  const factory DetailsEvent.emailChanged(String emailString) = _EmailChanged;
  const factory DetailsEvent.phoneChanged(String emailString) = _PhoneChanged;

  const factory DetailsEvent.detailsSubmitted() = _DetailSubmitted;
}
