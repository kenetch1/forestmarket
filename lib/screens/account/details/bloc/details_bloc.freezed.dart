// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'details_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DetailsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetailsEventCopyWith<$Res> {
  factory $DetailsEventCopyWith(
          DetailsEvent value, $Res Function(DetailsEvent) then) =
      _$DetailsEventCopyWithImpl<$Res, DetailsEvent>;
}

/// @nodoc
class _$DetailsEventCopyWithImpl<$Res, $Val extends DetailsEvent>
    implements $DetailsEventCopyWith<$Res> {
  _$DetailsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$DetailsEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'DetailsEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements DetailsEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$$_UserNameChangedCopyWith<$Res> {
  factory _$$_UserNameChangedCopyWith(
          _$_UserNameChanged value, $Res Function(_$_UserNameChanged) then) =
      __$$_UserNameChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String emailString});
}

/// @nodoc
class __$$_UserNameChangedCopyWithImpl<$Res>
    extends _$DetailsEventCopyWithImpl<$Res, _$_UserNameChanged>
    implements _$$_UserNameChangedCopyWith<$Res> {
  __$$_UserNameChangedCopyWithImpl(
      _$_UserNameChanged _value, $Res Function(_$_UserNameChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailString = null,
  }) {
    return _then(_$_UserNameChanged(
      null == emailString
          ? _value.emailString
          : emailString // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_UserNameChanged implements _UserNameChanged {
  const _$_UserNameChanged(this.emailString);

  @override
  final String emailString;

  @override
  String toString() {
    return 'DetailsEvent.userNameChanged(emailString: $emailString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserNameChanged &&
            (identical(other.emailString, emailString) ||
                other.emailString == emailString));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailString);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserNameChangedCopyWith<_$_UserNameChanged> get copyWith =>
      __$$_UserNameChangedCopyWithImpl<_$_UserNameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) {
    return userNameChanged(emailString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) {
    return userNameChanged?.call(emailString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (userNameChanged != null) {
      return userNameChanged(emailString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) {
    return userNameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) {
    return userNameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (userNameChanged != null) {
      return userNameChanged(this);
    }
    return orElse();
  }
}

abstract class _UserNameChanged implements DetailsEvent {
  const factory _UserNameChanged(final String emailString) = _$_UserNameChanged;

  String get emailString;
  @JsonKey(ignore: true)
  _$$_UserNameChangedCopyWith<_$_UserNameChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmailChangedCopyWith<$Res> {
  factory _$$_EmailChangedCopyWith(
          _$_EmailChanged value, $Res Function(_$_EmailChanged) then) =
      __$$_EmailChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String emailString});
}

/// @nodoc
class __$$_EmailChangedCopyWithImpl<$Res>
    extends _$DetailsEventCopyWithImpl<$Res, _$_EmailChanged>
    implements _$$_EmailChangedCopyWith<$Res> {
  __$$_EmailChangedCopyWithImpl(
      _$_EmailChanged _value, $Res Function(_$_EmailChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailString = null,
  }) {
    return _then(_$_EmailChanged(
      null == emailString
          ? _value.emailString
          : emailString // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_EmailChanged implements _EmailChanged {
  const _$_EmailChanged(this.emailString);

  @override
  final String emailString;

  @override
  String toString() {
    return 'DetailsEvent.emailChanged(emailString: $emailString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EmailChanged &&
            (identical(other.emailString, emailString) ||
                other.emailString == emailString));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailString);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EmailChangedCopyWith<_$_EmailChanged> get copyWith =>
      __$$_EmailChangedCopyWithImpl<_$_EmailChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) {
    return emailChanged(emailString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) {
    return emailChanged?.call(emailString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(emailString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) {
    return emailChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) {
    return emailChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(this);
    }
    return orElse();
  }
}

abstract class _EmailChanged implements DetailsEvent {
  const factory _EmailChanged(final String emailString) = _$_EmailChanged;

  String get emailString;
  @JsonKey(ignore: true)
  _$$_EmailChangedCopyWith<_$_EmailChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_PhoneChangedCopyWith<$Res> {
  factory _$$_PhoneChangedCopyWith(
          _$_PhoneChanged value, $Res Function(_$_PhoneChanged) then) =
      __$$_PhoneChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String emailString});
}

/// @nodoc
class __$$_PhoneChangedCopyWithImpl<$Res>
    extends _$DetailsEventCopyWithImpl<$Res, _$_PhoneChanged>
    implements _$$_PhoneChangedCopyWith<$Res> {
  __$$_PhoneChangedCopyWithImpl(
      _$_PhoneChanged _value, $Res Function(_$_PhoneChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailString = null,
  }) {
    return _then(_$_PhoneChanged(
      null == emailString
          ? _value.emailString
          : emailString // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PhoneChanged implements _PhoneChanged {
  const _$_PhoneChanged(this.emailString);

  @override
  final String emailString;

  @override
  String toString() {
    return 'DetailsEvent.phoneChanged(emailString: $emailString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PhoneChanged &&
            (identical(other.emailString, emailString) ||
                other.emailString == emailString));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailString);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PhoneChangedCopyWith<_$_PhoneChanged> get copyWith =>
      __$$_PhoneChangedCopyWithImpl<_$_PhoneChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) {
    return phoneChanged(emailString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) {
    return phoneChanged?.call(emailString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (phoneChanged != null) {
      return phoneChanged(emailString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) {
    return phoneChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) {
    return phoneChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (phoneChanged != null) {
      return phoneChanged(this);
    }
    return orElse();
  }
}

abstract class _PhoneChanged implements DetailsEvent {
  const factory _PhoneChanged(final String emailString) = _$_PhoneChanged;

  String get emailString;
  @JsonKey(ignore: true)
  _$$_PhoneChangedCopyWith<_$_PhoneChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DetailSubmittedCopyWith<$Res> {
  factory _$$_DetailSubmittedCopyWith(
          _$_DetailSubmitted value, $Res Function(_$_DetailSubmitted) then) =
      __$$_DetailSubmittedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_DetailSubmittedCopyWithImpl<$Res>
    extends _$DetailsEventCopyWithImpl<$Res, _$_DetailSubmitted>
    implements _$$_DetailSubmittedCopyWith<$Res> {
  __$$_DetailSubmittedCopyWithImpl(
      _$_DetailSubmitted _value, $Res Function(_$_DetailSubmitted) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_DetailSubmitted implements _DetailSubmitted {
  const _$_DetailSubmitted();

  @override
  String toString() {
    return 'DetailsEvent.detailsSubmitted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_DetailSubmitted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String emailString) userNameChanged,
    required TResult Function(String emailString) emailChanged,
    required TResult Function(String emailString) phoneChanged,
    required TResult Function() detailsSubmitted,
  }) {
    return detailsSubmitted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(String emailString)? userNameChanged,
    TResult? Function(String emailString)? emailChanged,
    TResult? Function(String emailString)? phoneChanged,
    TResult? Function()? detailsSubmitted,
  }) {
    return detailsSubmitted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String emailString)? userNameChanged,
    TResult Function(String emailString)? emailChanged,
    TResult Function(String emailString)? phoneChanged,
    TResult Function()? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (detailsSubmitted != null) {
      return detailsSubmitted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PhoneChanged value) phoneChanged,
    required TResult Function(_DetailSubmitted value) detailsSubmitted,
  }) {
    return detailsSubmitted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_UserNameChanged value)? userNameChanged,
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PhoneChanged value)? phoneChanged,
    TResult? Function(_DetailSubmitted value)? detailsSubmitted,
  }) {
    return detailsSubmitted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PhoneChanged value)? phoneChanged,
    TResult Function(_DetailSubmitted value)? detailsSubmitted,
    required TResult orElse(),
  }) {
    if (detailsSubmitted != null) {
      return detailsSubmitted(this);
    }
    return orElse();
  }
}

abstract class _DetailSubmitted implements DetailsEvent {
  const factory _DetailSubmitted() = _$_DetailSubmitted;
}

/// @nodoc
mixin _$DetailsState {
  UserName get userName => throw _privateConstructorUsedError;
  EmailAddress get emailAddress => throw _privateConstructorUsedError;
  Phone get phone => throw _privateConstructorUsedError;
  String get initialUserName => throw _privateConstructorUsedError;
  String get initialEmail => throw _privateConstructorUsedError;
  String get initialphone => throw _privateConstructorUsedError;
  bool get isSubmitting => throw _privateConstructorUsedError;
  bool get showErrorMessage => throw _privateConstructorUsedError;
  Either<DetailsFailure, Unit>? get authFailureOrSuccess =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DetailsStateCopyWith<DetailsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetailsStateCopyWith<$Res> {
  factory $DetailsStateCopyWith(
          DetailsState value, $Res Function(DetailsState) then) =
      _$DetailsStateCopyWithImpl<$Res, DetailsState>;
  @useResult
  $Res call(
      {UserName userName,
      EmailAddress emailAddress,
      Phone phone,
      String initialUserName,
      String initialEmail,
      String initialphone,
      bool isSubmitting,
      bool showErrorMessage,
      Either<DetailsFailure, Unit>? authFailureOrSuccess});
}

/// @nodoc
class _$DetailsStateCopyWithImpl<$Res, $Val extends DetailsState>
    implements $DetailsStateCopyWith<$Res> {
  _$DetailsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
    Object? emailAddress = null,
    Object? phone = null,
    Object? initialUserName = null,
    Object? initialEmail = null,
    Object? initialphone = null,
    Object? isSubmitting = null,
    Object? showErrorMessage = null,
    Object? authFailureOrSuccess = freezed,
  }) {
    return _then(_value.copyWith(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as UserName,
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as Phone,
      initialUserName: null == initialUserName
          ? _value.initialUserName
          : initialUserName // ignore: cast_nullable_to_non_nullable
              as String,
      initialEmail: null == initialEmail
          ? _value.initialEmail
          : initialEmail // ignore: cast_nullable_to_non_nullable
              as String,
      initialphone: null == initialphone
          ? _value.initialphone
          : initialphone // ignore: cast_nullable_to_non_nullable
              as String,
      isSubmitting: null == isSubmitting
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessage: null == showErrorMessage
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccess: freezed == authFailureOrSuccess
          ? _value.authFailureOrSuccess
          : authFailureOrSuccess // ignore: cast_nullable_to_non_nullable
              as Either<DetailsFailure, Unit>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DetailsFormStateCopyWith<$Res>
    implements $DetailsStateCopyWith<$Res> {
  factory _$$_DetailsFormStateCopyWith(
          _$_DetailsFormState value, $Res Function(_$_DetailsFormState) then) =
      __$$_DetailsFormStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {UserName userName,
      EmailAddress emailAddress,
      Phone phone,
      String initialUserName,
      String initialEmail,
      String initialphone,
      bool isSubmitting,
      bool showErrorMessage,
      Either<DetailsFailure, Unit>? authFailureOrSuccess});
}

/// @nodoc
class __$$_DetailsFormStateCopyWithImpl<$Res>
    extends _$DetailsStateCopyWithImpl<$Res, _$_DetailsFormState>
    implements _$$_DetailsFormStateCopyWith<$Res> {
  __$$_DetailsFormStateCopyWithImpl(
      _$_DetailsFormState _value, $Res Function(_$_DetailsFormState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userName = null,
    Object? emailAddress = null,
    Object? phone = null,
    Object? initialUserName = null,
    Object? initialEmail = null,
    Object? initialphone = null,
    Object? isSubmitting = null,
    Object? showErrorMessage = null,
    Object? authFailureOrSuccess = freezed,
  }) {
    return _then(_$_DetailsFormState(
      userName: null == userName
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as UserName,
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as Phone,
      initialUserName: null == initialUserName
          ? _value.initialUserName
          : initialUserName // ignore: cast_nullable_to_non_nullable
              as String,
      initialEmail: null == initialEmail
          ? _value.initialEmail
          : initialEmail // ignore: cast_nullable_to_non_nullable
              as String,
      initialphone: null == initialphone
          ? _value.initialphone
          : initialphone // ignore: cast_nullable_to_non_nullable
              as String,
      isSubmitting: null == isSubmitting
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessage: null == showErrorMessage
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccess: freezed == authFailureOrSuccess
          ? _value.authFailureOrSuccess
          : authFailureOrSuccess // ignore: cast_nullable_to_non_nullable
              as Either<DetailsFailure, Unit>?,
    ));
  }
}

/// @nodoc

class _$_DetailsFormState implements _DetailsFormState {
  const _$_DetailsFormState(
      {required this.userName,
      required this.emailAddress,
      required this.phone,
      this.initialUserName = '',
      this.initialEmail = '',
      this.initialphone = '',
      this.isSubmitting = false,
      this.showErrorMessage = false,
      this.authFailureOrSuccess});

  @override
  final UserName userName;
  @override
  final EmailAddress emailAddress;
  @override
  final Phone phone;
  @override
  @JsonKey()
  final String initialUserName;
  @override
  @JsonKey()
  final String initialEmail;
  @override
  @JsonKey()
  final String initialphone;
  @override
  @JsonKey()
  final bool isSubmitting;
  @override
  @JsonKey()
  final bool showErrorMessage;
  @override
  final Either<DetailsFailure, Unit>? authFailureOrSuccess;

  @override
  String toString() {
    return 'DetailsState(userName: $userName, emailAddress: $emailAddress, phone: $phone, initialUserName: $initialUserName, initialEmail: $initialEmail, initialphone: $initialphone, isSubmitting: $isSubmitting, showErrorMessage: $showErrorMessage, authFailureOrSuccess: $authFailureOrSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DetailsFormState &&
            (identical(other.userName, userName) ||
                other.userName == userName) &&
            (identical(other.emailAddress, emailAddress) ||
                other.emailAddress == emailAddress) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.initialUserName, initialUserName) ||
                other.initialUserName == initialUserName) &&
            (identical(other.initialEmail, initialEmail) ||
                other.initialEmail == initialEmail) &&
            (identical(other.initialphone, initialphone) ||
                other.initialphone == initialphone) &&
            (identical(other.isSubmitting, isSubmitting) ||
                other.isSubmitting == isSubmitting) &&
            (identical(other.showErrorMessage, showErrorMessage) ||
                other.showErrorMessage == showErrorMessage) &&
            (identical(other.authFailureOrSuccess, authFailureOrSuccess) ||
                other.authFailureOrSuccess == authFailureOrSuccess));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      userName,
      emailAddress,
      phone,
      initialUserName,
      initialEmail,
      initialphone,
      isSubmitting,
      showErrorMessage,
      authFailureOrSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DetailsFormStateCopyWith<_$_DetailsFormState> get copyWith =>
      __$$_DetailsFormStateCopyWithImpl<_$_DetailsFormState>(this, _$identity);
}

abstract class _DetailsFormState implements DetailsState {
  const factory _DetailsFormState(
          {required final UserName userName,
          required final EmailAddress emailAddress,
          required final Phone phone,
          final String initialUserName,
          final String initialEmail,
          final String initialphone,
          final bool isSubmitting,
          final bool showErrorMessage,
          final Either<DetailsFailure, Unit>? authFailureOrSuccess}) =
      _$_DetailsFormState;

  @override
  UserName get userName;
  @override
  EmailAddress get emailAddress;
  @override
  Phone get phone;
  @override
  String get initialUserName;
  @override
  String get initialEmail;
  @override
  String get initialphone;
  @override
  bool get isSubmitting;
  @override
  bool get showErrorMessage;
  @override
  Either<DetailsFailure, Unit>? get authFailureOrSuccess;
  @override
  @JsonKey(ignore: true)
  _$$_DetailsFormStateCopyWith<_$_DetailsFormState> get copyWith =>
      throw _privateConstructorUsedError;
}
