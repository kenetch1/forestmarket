import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/database/database_services.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/screens/account/details/domain/email_address.dart';
import 'package:foreststore/screens/account/details/domain/phone.dart';
import 'package:foreststore/screens/account/details/domain/user_name.dart';
import 'package:foreststore/screens/account/details/failure/details_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

part 'details_event.dart';
part 'details_state.dart';
part 'details_bloc.freezed.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  final DatabaseService _databaseService = DatabaseService();
  DetailsBloc() : super(DetailsState.initial()) {
    on<DetailsEvent>((event, emit) async {
      await event.when<FutureOr<void>>(
        started: () async => await _onStarted(emit).whenComplete(() => emit),
        userNameChanged: (userNameString) =>
            _onUserNameChanged(emit, userNameString),
        emailChanged: (emailString) => _onEmailChanged(emit, emailString),
        phoneChanged: (phoneString) => _onPhoneChanged(emit, phoneString),
        detailsSubmitted: () => _onDetailsSubmitted(emit),
      );
    });
  }

  Future<void> _onStarted(Emitter<DetailsState> emit) async {
    DetailsState newState = state;
    Completer<DetailsState> c = Completer<DetailsState>();
    _onSelectUser(1).then((value) {
      newState = state.copyWith(
        userName: UserName(value.username),
        emailAddress: EmailAddress(value.email),
        phone: Phone(value.phoneNumber),
        initialUserName: value.username,
        initialEmail: value.email,
        initialphone: value.phoneNumber,
        authFailureOrSuccess: null,
      );
      c.complete(newState);
    });
    var stateToReturn = await c.future;
    emit(stateToReturn);
  }

  void _onUserNameChanged(Emitter<DetailsState> emit, String userNameString) {
    emit(
      state.copyWith(
        userName: UserName(userNameString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onEmailChanged(Emitter<DetailsState> emit, String emailString) {
    emit(
      state.copyWith(
        emailAddress: EmailAddress(emailString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onPhoneChanged(Emitter<DetailsState> emit, String phoneString) {
    emit(
      state.copyWith(
        phone: Phone(phoneString),
        authFailureOrSuccess: null,
      ),
    );
  }

  // Call this function to delete a breed
  Future<UsersEntity> _onSelectUser(int id) async {
    return await _databaseService.user(id);
  }

  Future<void> _onDetailsSubmitted(Emitter<DetailsState> emit) async {
    final isUserNameValid = state.userName.value.isRight();
    final isEmailValid = state.emailAddress.value.isRight();
    final isPhoneValid = state.phone.value.isRight();

    if (isEmailValid && isUserNameValid && isPhoneValid) {
      emit(
        state.copyWith(
          isSubmitting: true,
          authFailureOrSuccess: null,
        ),
      );

      // Perform network request to get a token.
      String valueUserName = state.userName.value.fold((l) => '', (r) => r);
      String valueEmail = state.emailAddress.value.fold((l) => '', (r) => r);
      String valuePhone = state.phone.value.fold((l) => '', (r) => r);

      _onSelectUser(1).then((value) async {
        if (value.id != 0) {
          debugPrint("${value.id} - ${value.username}");
          await _databaseService.updateUser(UsersEntity(
              id: value.id,
              code: value.code,
              username: valueUserName,
              phoneNumber: valuePhone,
              email: valueEmail,
              updateAt: DateTool.getCurrentDate(),
              user: "Gerardo",
              type: "user"));
        } else {
          SharedPreferences.getInstance().then((prefs) async {
            if (prefs.getString("user") != null) {
              UserModel user =
                  UserModel.fromJson(jsonDecode(prefs.getString("user") ?? ""));
              debugPrint(user.id);
              await _databaseService.insertUsers(UsersEntity(
                  code: user.id,
                  username: valueUserName,
                  phoneNumber: valuePhone,
                  email: valueEmail,
                  image: "image1",
                  uid: user.id, //Al recuperar el login
                  active: "true",
                  createAt: DateTool.getCurrentDate(),
                  updateAt: DateTool.getCurrentDate(),
                  user: "Gerardo",
                  type: "user"));
            }
          });
        }
      });
    }
    emit(
      state.copyWith(
        isSubmitting: false,
        showErrorMessage: true,

        // Depending on the response received from the server after loggin in,
        // emit proper authFailureOrSuccess.

        // For now we will just see if the email and password were valid or not
        // and accordingly set authFailureOrSuccess' value.

        authFailureOrSuccess: (isEmailValid && isUserNameValid && isPhoneValid)
            ? right(unit)
            : null,
      ),
    );
  }
}
