part of 'details_bloc.dart';

@freezed
class DetailsState with _$DetailsState {
  const factory DetailsState({
    required UserName userName,
    required EmailAddress emailAddress,
    required Phone phone,
    @Default('') String initialUserName,
    @Default('') String initialEmail,
    @Default('') String initialphone,
    @Default(false) bool isSubmitting,
    @Default(false) bool showErrorMessage,
    Either<DetailsFailure, Unit>? authFailureOrSuccess,
    // Unit comes from Dartz package and is equivalent to void.
  }) = _DetailsFormState;

  // ignore: no_leading_underscores_for_local_identifiers
  factory DetailsState.initial() {
    return DetailsState(
      userName: UserName(''),
      emailAddress: EmailAddress(''),
      phone: Phone(''),
    );
  }
}
