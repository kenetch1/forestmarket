import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/screens/account/details/bloc/details_bloc.dart';

class UserNameInput extends StatelessWidget {
  const UserNameInput({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController userNameInput = TextEditingController();

    return BlocConsumer<DetailsBloc, DetailsState>(
      listener: (context, state) {
        userNameInput.text = state.initialUserName;
      },
      builder: (context, state) {
        userNameInput.text = state.initialUserName;
        return TextFormField(
          controller: userNameInput,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.account_circle),
            labelText: 'Nombre',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          keyboardType: TextInputType.name,
          autocorrect: false,
          onChanged: (value) => context
              .read<DetailsBloc>()
              .add(DetailsEvent.userNameChanged(value)),
          validator: (_) => state.emailAddress.value.fold<String?>(
            (f) => f.maybeMap<String?>(
              invalidEmail: (_) => 'Invalid Name',
              orElse: () => null,
            ),
            (_) => null,
          ),
          autovalidateMode: state.showErrorMessage == true
              ? AutovalidateMode.always
              : AutovalidateMode.disabled,
        );
      },
    );
  }
}
