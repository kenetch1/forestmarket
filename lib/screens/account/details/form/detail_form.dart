import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/screens/account/details/bloc/details_bloc.dart';
import 'package:foreststore/screens/account/details/form/details_button.dart';
import 'package:foreststore/screens/account/details/form/email_input.dart';
import 'package:foreststore/screens/account/details/form/loading_indicator.dart';
import 'package:foreststore/screens/account/details/form/phone_input.dart';
import 'package:foreststore/screens/account/details/form/username_input.dart';
import 'package:foreststore/styles/colors.dart';

class DetailForm extends StatelessWidget {
  const DetailForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocListener<DetailsBloc, DetailsState>(
        listener: (context, state) {
          final authFailureOrSuccess = state.authFailureOrSuccess;

          if (authFailureOrSuccess != null) {
            authFailureOrSuccess.fold(
              (failure) {
                // Do something to handle failure. For example, show a
                // snackbar saying "Invalid Email and Password Combination" or
                // "Server Error" depending on the failure.

                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      failure.when<String>(
                        invalidSubmitCombination: () =>
                            'Invaid email and password combination!',
                        serverError: () => 'Server Error!',
                      ),
                    ),
                  ),
                );
              },
              (success) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Registro exitoso...'),
                    backgroundColor: AppColors.primaryColor,
                  ),
                );
              },
            );
          }
        },
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: UserNameInput(),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
                  child: EmailInput(),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: PhoneInput(),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: AppButton(
                    label: "Guardar",
                    fontWeight: FontWeight.w600,
                    padding: const EdgeInsets.symmetric(vertical: 25),
                    onPressed: () {
                      context.read<DetailsBloc>().add(
                            const DetailsEvent.detailsSubmitted(),
                          );
                    },
                  ),
                ),
                const SizedBox(height: 15),
                const LoadingIndicator(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
