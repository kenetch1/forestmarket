import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/screens/account/details/bloc/details_bloc.dart';

class PhoneInput extends StatelessWidget {
  const PhoneInput({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController phoneInput = TextEditingController();
    return BlocConsumer<DetailsBloc, DetailsState>(
      listener: (context, state) {
        phoneInput.text = state.initialphone;
      },
      builder: (context, state) {
        phoneInput.text = state.initialphone;
        return TextFormField(
          controller: phoneInput,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.phone),
            labelText: 'Teléfono',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          keyboardType: TextInputType.emailAddress,
          autocorrect: false,
          onChanged: (value) =>
              context.read<DetailsBloc>().add(DetailsEvent.phoneChanged(value)),
          validator: (_) => state.emailAddress.value.fold<String?>(
            (f) => f.maybeMap<String?>(
              invalidEmail: (_) => 'Invalid Phone',
              orElse: () => null,
            ),
            (_) => null,
          ),
          autovalidateMode: state.showErrorMessage == true
              ? AutovalidateMode.always
              : AutovalidateMode.disabled,
        );
      },
    );
  }
}
