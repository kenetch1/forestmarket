import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/screens/account/details/bloc/details_bloc.dart';

class EmailInput extends StatelessWidget {
  const EmailInput({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController emailInput = TextEditingController();
    return BlocConsumer<DetailsBloc, DetailsState>(
      listener: (context, state) {
        emailInput.text = state.initialEmail;
      },
      builder: (context, state) {
        emailInput.text = state.initialEmail;
        return TextFormField(
          controller: emailInput,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.email),
            labelText: 'Email',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          keyboardType: TextInputType.emailAddress,
          autocorrect: false,
          onChanged: (value) =>
              context.read<DetailsBloc>().add(DetailsEvent.emailChanged(value)),
          validator: (_) => state.emailAddress.value.fold<String?>(
            (f) => f.maybeMap<String?>(
              invalidEmail: (_) => 'Invalid Email',
              orElse: () => null,
            ),
            (_) => null,
          ),
          autovalidateMode: state.showErrorMessage == true
              ? AutovalidateMode.always
              : AutovalidateMode.disabled,
        );
      },
    );
  }
}
