import 'package:flutter/material.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/styles/colors.dart';

class OrderCard extends StatelessWidget {
  final OrdersEntity order;
  final String client;

  const OrderCard({super.key, required this.order, required this.client});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey.shade500,
            width: 0.5,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Container(
            //   padding: const EdgeInsets.only(top: 10, right: 15, bottom: 10),
            //   child: ClipOval(
            //     child: Image.network(
            //       contact.image,
            //       width: 50,
            //       height: 50,
            //     ),
            //   ),
            // ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(client,
                      style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          color: AppColors.darkGrey)),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'OrderId: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: order.orderId,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Estatus: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: order.orderStatus,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Metodo: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: order.shippingMethod,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.italic,
                              color: AppColors.orangeColor))
                    ]),
                  ),
                ],
              ),
            ),
            // Row(
            //   mainAxisSize: MainAxisSize.min,
            //   children: [
            //     // Icon(
            //     //   Icons.create,
            //     //   color: Colors.grey[600],
            //     // ),
            //     const SizedBox(width: 15.0),
            //     Icon(
            //       Icons.message,
            //       color: Colors.grey[600],
            //     ),
            //     const SizedBox(width: 15.0),
            //     Icon(
            //       Icons.call,
            //       color: Colors.grey[600],
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
