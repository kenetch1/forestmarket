import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/repositories/orders/firebase_orders_repository.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';
import 'package:foreststore/screens/clients/client/client_card.dart';
import 'package:foreststore/screens/clients/order/order_card.dart';
import 'package:foreststore/screens/clients/product/product_delivery_screen.dart';

class OrderScreen extends StatefulWidget {
  final UsersEntity client;
  const OrderScreen({super.key, required this.client});

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  OrdersRepository repository = FirebaseOrdersRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: const EdgeInsets.only(left: 25),
              child: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
            ),
          ),
          title: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: AppText(
              text: widget.client.username,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          )),
      body: StreamBuilder<List<OrdersEntity>>(
        stream: repository.orders(widget.client.uid ?? ''),
        builder: (ctx, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              List<OrdersEntity> order = snapshot.data!;
              return _buildListView(order);
            case ConnectionState.active:
              List<OrdersEntity> order = snapshot.data!;
              return _buildListView(order);
            default:
              return _buildLoadingScreen();
          }
        },
      ),
    );
  }

  Widget _buildListView(List<OrdersEntity> contacts) {
    return ListView.builder(
      itemBuilder: (ctx, idx) {
        return GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) {
                  return ProductDeliveryScreen(
                      client: widget.client, order: contacts[idx]);
                },
              ));
            },
            child: OrderCard(
                order: contacts[idx], client: widget.client.username));
      },
      itemCount: contacts.length,
    );
  }

  Widget _buildLoadingScreen() {
    return const Center(
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
