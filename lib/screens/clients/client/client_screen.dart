import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/repositories/orders/firebase_orders_repository.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';
import 'package:foreststore/screens/clients/client/client_card.dart';

class ClientScreen extends StatefulWidget {
  const ClientScreen({super.key});

  @override
  _ClientScreenState createState() => _ClientScreenState();
}

class _ClientScreenState extends State<ClientScreen> {
  OrdersRepository repository = FirebaseOrdersRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          title: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: const AppText(
              text: 'Clientes',
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          )),
      body: FutureBuilder<Set<UsersEntity>>(
        future: repository.orderByUser(),
        builder: (ctx, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              List<UsersEntity> contacts = snapshot.data!.toList();
              return _buildListView(contacts);
            default:
              return _buildLoadingScreen();
          }
        },
      ),
    );
  }

  Widget _buildListView(List<UsersEntity> contacts) {
    return ListView.builder(
      itemBuilder: (ctx, idx) {
        return ClientCard(contact: contacts[idx]);
      },
      itemCount: contacts.length,
    );
  }

  Widget _buildLoadingScreen() {
    return const Center(
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
