import 'package:flutter/material.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/screens/clients/order/order_screen.dart';
import 'package:foreststore/styles/colors.dart';

class ClientCard extends StatelessWidget {
  final UsersEntity contact;

  const ClientCard({super.key, required this.contact});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey.shade500,
            width: 0.5,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Container(
            //   padding: const EdgeInsets.only(top: 10, right: 15, bottom: 10),
            //   child: ClipOval(
            //     child: Image.network(
            //       contact.image,
            //       width: 50,
            //       height: 50,
            //     ),
            //   ),
            // ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Cliente: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: contact.username,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Email: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: contact.email,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Tel: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: contact.phoneNumber,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              color: AppColors.orangeColor))
                    ]),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.list_rounded,
                    color: Colors.grey[600],
                  ),
                  onPressed: () {
                    //action coe when button is pressed
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return OrderScreen(client: contact);
                      },
                    ));
                  },
                ),
                const SizedBox(width: 15.0),
                Icon(
                  Icons.message,
                  color: Colors.grey[600],
                ),
                // const SizedBox(width: 15.0),
                // Icon(
                //   Icons.call,
                //   color: Colors.grey[600],
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
