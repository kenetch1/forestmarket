import 'dart:async';

import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/entities/orders/ordersproducts_entity.dart';
import 'package:foreststore/repositories/orders/firebase_orders_repository.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';
import 'package:foreststore/screens/clients/product/product_delivery_card.dart';
import 'package:foreststore/screens/dashboard/dashboard_delivery_screen.dart';

class ProductDeliveryScreen extends StatefulWidget {
  final UsersEntity client;
  final OrdersEntity order;
  const ProductDeliveryScreen(
      {super.key, required this.client, required this.order});

  @override
  _ProductDeliveryScreenState createState() => _ProductDeliveryScreenState();
}

class _ProductDeliveryScreenState extends State<ProductDeliveryScreen> {
  OrdersRepository repository = FirebaseOrdersRepository();
  late Stream<List<OrdersProductsEntity>> userStream;
  StreamController<List<OrdersProductsEntity>> streamController =
      StreamController();

  @override
  void initState() {
    streamController.addStream(repository.orderProduct(widget.order.orderId));
    userStream = streamController.stream.asBroadcastStream();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: const EdgeInsets.only(left: 25),
              child: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) {
                    return const DashboardDeliveryScreen(index: 0);
                  },
                ));
              },
              child: Container(
                padding: const EdgeInsets.only(right: 25),
                child: const Icon(
                  Icons.start_rounded,
                  color: Colors.black,
                ),
              ),
            ),
          ],
          title: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: AppText(
              text: widget.client.username,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          )),
      body: StreamBuilder<List<OrdersProductsEntity>>(
        stream: userStream, //repository.orderProduct(widget.order.orderId),
        builder: (ctx, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              List<OrdersProductsEntity> order = snapshot.data!;
              return _buildListView(order);
            case ConnectionState.active:
              List<OrdersProductsEntity> order = snapshot.data!;
              return _buildListView(order);
            default:
              return _buildLoadingScreen();
          }
        },
      ),
    );
  }

  Widget _buildListView(List<OrdersProductsEntity> orderProduct) {
    return Column(
      children: [
        RichText(
          text: TextSpan(children: [
            const TextSpan(
                text: 'Gran Total: ',
                style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.normal,
                    color: Colors.black)),
            TextSpan(
                text: '\$${widget.order.grandTotal}',
                style: const TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.normal,
                    color: Colors.red))
          ]),
        ),
        widget.order.orderStatusId.compareTo('1') == 0
            ? Padding(
                padding: const EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
                child: Row(
                  children: [
                    Expanded(
                        child: AppButton(
                            label: 'Empaquetar',
                            fontWeight: FontWeight.w600,
                            padding: const EdgeInsets.symmetric(
                              vertical: 25,
                            ),
                            onPressed: _empaquetar)),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
                child: Row(
                  children: [
                    Expanded(
                        child: AppButton(
                            label: 'Entregar',
                            fontWeight: FontWeight.w600,
                            padding: const EdgeInsets.symmetric(
                              vertical: 25,
                            ),
                            onPressed: () async {
                              bool response = await repository.updateState(
                                  widget.client,
                                  widget.order,
                                  '3',
                                  'Entregado');
                              if (response) {
                                repository
                                    .updateInStock(orderProduct)
                                    .then((value) {
                                  Navigator.pop(context);
                                });
                              }
                            })),
                  ],
                ),
              ),
        const SizedBox(height: 2),
        Expanded(
          child: ListView.builder(
            itemBuilder: (ctx, idx) {
              return ProductDeliveryCard(product: orderProduct[idx]);
            },
            itemCount: orderProduct.length,
          ),
        ),
      ],
    );
  }

  _empaquetar() {
    repository
        .updateState(widget.client, widget.order, '2', 'Empaquetado')
        .then((value) {
      Navigator.pop(context);
    });
  }

  Widget _buildLoadingScreen() {
    return const Center(
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
