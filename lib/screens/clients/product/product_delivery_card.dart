import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/orders/ordersproducts_entity.dart';
import 'package:foreststore/styles/colors.dart';

class ProductDeliveryCard extends StatelessWidget {
  final OrdersProductsEntity product;

  const ProductDeliveryCard({super.key, required this.product});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey.shade500,
            width: 0.5,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Producto: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: product.product,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Desc: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: product.description,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.italic,
                              color: AppColors.primaryColor))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Cantidad: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: product.quantity,
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                              color: Colors.red))
                    ]),
                  ),
                  const SizedBox(height: 2),
                  RichText(
                    text: TextSpan(children: [
                      const TextSpan(
                          text: 'Precio: ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      TextSpan(
                          text: '\$${product.regularPrice}',
                          style: const TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                              color: Colors.red))
                    ]),
                  ),
                ],
              ),
            ),
            // Row(
            //   mainAxisSize: MainAxisSize.min,
            //   children: [
            //     // Icon(
            //     //   Icons.create,
            //     //   color: Colors.grey[600],
            //     // ),
            //     const SizedBox(width: 15.0),
            //     Icon(
            //       Icons.message,
            //       color: Colors.grey[600],
            //     ),
            //     const SizedBox(width: 15.0),
            //     Icon(
            //       Icons.call,
            //       color: Colors.grey[600],
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
