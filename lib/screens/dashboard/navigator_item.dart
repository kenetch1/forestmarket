import 'package:flutter/material.dart';
import 'package:foreststore/screens/account/account_screen.dart';
import 'package:foreststore/screens/cart/cart_screen.dart';
import 'package:foreststore/screens/clients/client/client_screen.dart';
import 'package:foreststore/screens/dummy_screen.dart';
import 'package:foreststore/screens/explore/explore_screen.dart';
import 'package:foreststore/screens/orders/orders_screen.dart';
import 'package:foreststore/screens/home/home_screen.dart';

class NavigatorItem {
  final String label;
  final String iconPath;
  final int index;
  final Widget screen;

  NavigatorItem(this.label, this.iconPath, this.index, this.screen);
}

List<NavigatorItem> navigatorItems = [
  NavigatorItem("Tienda", "assets/icons/tienda.svg", 0, HomeScreen()),
  NavigatorItem(
      "Explorar", "assets/icons/explorer.svg", 1, const ExploreScreen()),
  NavigatorItem("Carrito", "assets/icons/carrito.svg", 2, const CartScreen()),
  NavigatorItem("Pedidos", "assets/icons/pedido.svg", 3, const OrdersScreen()),
  NavigatorItem(
      "Cuentas", "assets/icons/account.svg", 4, const AccountScreen()),
];

List<NavigatorItem> navigatorItems2 = [
  NavigatorItem(
      "Cliente", "assets/icons/shop_icon.svg", 0, const ClientScreen()),
  NavigatorItem(
      "Explorar", "assets/icons/explore_icon.svg", 1, const ExploreScreen()),
];

List<List<NavigatorItem>> tabItem = [navigatorItems, navigatorItems2];
