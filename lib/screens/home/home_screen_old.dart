import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/repositories/home/home_repository_implement.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/home/bloc/home_bloc.dart';
import 'package:foreststore/screens/product_details/product_details_screen.dart';
import 'package:foreststore/styles/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foreststore/widgets/grocery_item_card_widget.dart';
import 'package:foreststore/widgets/search_bar_widget.dart';
import 'package:uuid/uuid.dart';

import 'grocery_featured_Item_widget.dart';
import 'home_banner_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
        create: (context) {
          return HomeBloc(HomeRepositoryImplement())
            ..add(const HomeEvent.started());
          // ..add(const HomeEvent.exclusives())
          // ..add(const HomeEvent.solds())
          // ..add(const HomeEvent.news());
        },
        child: Builder(builder: (context) => buildPage(context)));
  }

  Widget buildPage(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) async {
            if (state.isLoading) {
              WidgetsBinding.instance.endOfFrame.then(
                (_) {
                  if (state.exclusives.first.id.isEmpty) {
                    debugPrint('Termino');
                  }
                },
              );
            }
          },
          builder: (context, state) {
            if (state.isLoading) {
              return const Center(
                  child: CircularProgressIndicator(
                backgroundColor: Colors.black26,
                valueColor: AlwaysStoppedAnimation<Color>(
                  Colors.black, //<-- SEE HERE
                ),
              ));
            } else {
              return SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      SvgPicture.asset("assets/icons/app_icon_color.svg"),
                      const SizedBox(
                        height: 5,
                      ),
                      padded(locationWidget()),
                      const SizedBox(
                        height: 15,
                      ),
                      padded(const SearchBarWidget()),
                      const SizedBox(
                        height: 25,
                      ),
                      padded(HomeBanner(offers: state.offers)),
                      const SizedBox(
                        height: 25,
                      ),
                      padded(subTitle(
                          context,
                          AppLocalizations.of(context)!.home_exclusive,
                          "Ver todo")),
                      getHorizontalItemSlider(state.exclusives),
                      const SizedBox(
                        height: 15,
                      ),
                      padded(subTitle(context,
                          AppLocalizations.of(context)!.home_sell, "Ver todo")),
                      getHorizontalItemSlider(state.solds),
                      const SizedBox(
                        height: 15,
                      ),
                      padded(subTitle(
                          context,
                          AppLocalizations.of(context)!.home_groceries,
                          "Ver todo")),
                      const SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        height: 105,
                        child: ListView(
                          padding: EdgeInsets.zero,
                          scrollDirection: Axis.horizontal,
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            GroceryFeaturedCard(
                              groceryFeaturedItems[0],
                              color: const Color(0xffF8A44C),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            GroceryFeaturedCard(
                              groceryFeaturedItems[1],
                              color: AppColors.primaryColor,
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      getHorizontalItemSlider(state.news),
                      const SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget padded(Widget widget) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: widget,
    );
  }

  Widget getHorizontalItemSlider(List<GroceryItem> items) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: 250,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemCount: items.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              onItemClicked(context, items[index]);
            },
            child: GroceryItemCardWidget(
              item: items[index],
              heroSuffix: const Uuid().v1(),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            width: 20,
          );
        },
      ),
    );
  }

  void onItemClicked(BuildContext context, GroceryItem groceryItem) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ProductDetailsScreen(
                groceryItem,
                heroSuffix: "home_screen",
              )),
    );
  }

  Widget subTitle(BuildContext context, String text, String see) {
    return Row(
      children: [
        Text(
          text,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        const Spacer(),
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) {
                return const DashboardScreen(index: 1);
              },
            ));
          },
          child: Text(
            see,
            style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppColors.primaryColor),
          ),
        ),
      ],
    );
  }

  Widget locationWidget() {
    String locationIconPath = "assets/icons/location_icon.svg";
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          locationIconPath,
        ),
        const SizedBox(
          width: 8,
        ),
        const Text(
          "Bosque Market",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
