import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/notification/notification.dart';
import 'package:foreststore/repositories/home/home_repository.dart';
import 'package:foreststore/repositories/home/home_repository_implement.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/home/bloc/home_bloc.dart';
import 'package:foreststore/screens/product_details/product_details_screen.dart';
import 'package:foreststore/styles/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foreststore/widgets/grocery_item_card_widget.dart';
import 'package:foreststore/widgets/message_arguments.dart';
import 'package:foreststore/widgets/search_bar_widget.dart';
import 'package:multiple_stream_builder/multiple_stream_builder.dart';
import 'package:uuid/uuid.dart';

import 'grocery_featured_Item_widget.dart';
import 'home_banner_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:http/http.dart' as http;

// Crude counter to make messages unique
int _messageCount = 0;

/// The API endpoint here accepts a raw FCM payload for demonstration purposes.
String constructFCMPayload(String? token) {
  _messageCount++;
  return jsonEncode({
    'token': token,
    'data': {
      'via': 'FlutterFire Cloud Messaging!!!',
      'count': _messageCount.toString(),
    },
    'notification': {
      'title': 'Hello FlutterFire!',
      'body': 'This notification (#$_messageCount) was created via FCM!',
    },
  });
}

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeRepository homeRepository = HomeRepositoryImplement();
  List<Offers> offers = [
    const Offers(title: '', code: '', description: '', discount: '0.0', id: '')
  ];

  List<Future<GroceryItem>> initial = [
    Future.value(const GroceryItem(
        id: '',
        name: 'producto',
        description: 'producto',
        price: 10,
        imagePath: 'assets/images/grocery_images/banana.png'))
  ];

  // String? _token;
  // String? initialMessage;
  // bool _resolved = false;

  // @override
  // void initState() {
  //   super.initState();

  //   FirebaseMessaging.instance.getInitialMessage().then(
  //         (value) => setState(
  //           () {
  //             _resolved = true;
  //             initialMessage = value?.data.toString();
  //           },
  //         ),
  //       );

  //   FirebaseMessaging.onMessage.listen(showFlutterNotification);

  //   FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
  //     debugPrint('A new onMessageOpenedApp event was published!');
  //     // Navigator.pushNamed(
  //     //   context,
  //     //   '/message',
  //     //   arguments: MessageArguments(message, true),
  //     // );
  //   });
  // }

  // Future<void> sendPushMessage() async {
  //   if (_token == null) {
  //     debugPrint('Unable to send FCM message, no token exists.');
  //     return;
  //   }

  //   try {
  //     await http.post(
  //       Uri.parse('https://api.rnfirebase.io/messaging/send'),
  //       headers: <String, String>{
  //         'Content-Type': 'application/json; charset=UTF-8',
  //       },
  //       body: constructFCMPayload(_token),
  //     );
  //     debugPrint('FCM request for device sent!');
  //   } catch (e) {
  //     debugPrint(e.toString());
  //   }
  // }

  // Future<void> onActionSelected(String value) async {
  //   switch (value) {
  //     case 'subscribe':
  //       {
  //         debugPrint(
  //           'FlutterFire Messaging Example: Subscribing to topic "fcm_test".',
  //         );
  //         await FirebaseMessaging.instance.subscribeToTopic('fcm_test');
  //         debugPrint(
  //           'FlutterFire Messaging Example: Subscribing to topic "fcm_test" successful.',
  //         );
  //       }
  //       break;
  //     case 'unsubscribe':
  //       {
  //         debugPrint(
  //           'FlutterFire Messaging Example: Unsubscribing from topic "fcm_test".',
  //         );
  //         await FirebaseMessaging.instance.unsubscribeFromTopic('fcm_test');
  //         debugPrint(
  //           'FlutterFire Messaging Example: Unsubscribing from topic "fcm_test" successful.',
  //         );
  //       }
  //       break;
  //     case 'get_apns_token':
  //       {
  //         if (defaultTargetPlatform == TargetPlatform.iOS ||
  //             defaultTargetPlatform == TargetPlatform.macOS) {
  //           debugPrint('FlutterFire Messaging Example: Getting APNs token...');
  //           String? token = await FirebaseMessaging.instance.getAPNSToken();
  //           debugPrint('FlutterFire Messaging Example: Got APNs token: $token');
  //         } else {
  //           debugPrint(
  //             'FlutterFire Messaging Example: Getting an APNs token is only supported on iOS and macOS platforms.',
  //           );
  //         }
  //       }
  //       break;
  //     default:
  //       break;
  //   }
  // }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      FCM.setContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder4<List<Offers>, List<Future<GroceryItem>>,
            List<Future<GroceryItem>>, List<Future<GroceryItem>>>(
        streams: StreamTuple4(
            homeRepository.offer(),
            homeRepository.exclusive(),
            homeRepository.sold(),
            homeRepository.newp()),
        initialData: InitialDataTuple4(offers, initial, initial, initial),
        builder: (context, snapshots) {
          if (snapshots.snapshot1.connectionState == ConnectionState.waiting) {
            return const Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.black26,
              valueColor: AlwaysStoppedAnimation<Color>(
                Colors.black, //<-- SEE HERE
              ),
            ));
          } else if (snapshots.snapshot1.connectionState ==
                  ConnectionState.active ||
              snapshots.snapshot1.connectionState == ConnectionState.done) {
            if (snapshots.snapshot1.hasError) {
              return const Center(child: Text("Ocurrio un error"));
            }
            if (snapshots.snapshot1.hasData) {
              return buildPage(context, snapshots);
            }
            return const Center(child: Text("No hay datos"));
          }
          return Center(
              child: Text(snapshots.snapshot1.connectionState.toString()));
        });
  }

  Widget buildPage(
      BuildContext context,
      SnapshotTuple4<List<Offers>, List<Future<GroceryItem>>,
              List<Future<GroceryItem>>, List<Future<GroceryItem>>>
          snapshots) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              SvgPicture.asset("assets/icons/manzana.svg"),
              const SizedBox(
                height: 5,
              ),
              padded(locationWidget()),
              const SizedBox(
                height: 15,
              ),
              padded(tone()),
              const SizedBox(
                height: 25,
              ),
              padded(HomeBanner(offers: snapshots.snapshot1.data!.first)),
              const SizedBox(
                height: 25,
              ),
              padded(subTitle(context,
                  AppLocalizations.of(context)!.home_exclusive, "Ver todo")),
              getHorizontalItemSlider(snapshots.snapshot2.data!),
              const SizedBox(
                height: 15,
              ),
              padded(subTitle(context, AppLocalizations.of(context)!.home_sell,
                  "Ver todo")),
              getHorizontalItemSlider(snapshots.snapshot3.data!),
              const SizedBox(
                height: 15,
              ),
              padded(subTitle(context,
                  AppLocalizations.of(context)!.home_groceries, "Ver todo")),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 105,
                child: ListView(
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.horizontal,
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    GroceryFeaturedCard(
                      groceryFeaturedItems[0],
                      color: const Color(0xffF8A44C),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    GroceryFeaturedCard(
                      groceryFeaturedItems[1],
                      color: AppColors.primaryColor,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              getHorizontalItemSlider(snapshots.snapshot4.data!),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      )),
    );
  }

  Widget padded(Widget widget) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: widget,
    );
  }

  Widget getHorizontalItemSlider(List<Future<GroceryItem>> items) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: 250,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemCount: items.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return FutureBuilder(
              future: items[index],
              builder: (context, AsyncSnapshot<GroceryItem> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        '${snapshot.error} occurred',
                        style: const TextStyle(fontSize: 18),
                      ),
                    );

                    // if we got our data
                  } else if (snapshot.hasData) {
                    GroceryItem item = snapshot.data!;
                    return GestureDetector(
                      onTap: () {
                        // items[index]
                        //     .then((value) => onItemClicked(context, value));
                        onItemClicked(context, item);
                      },
                      child: GroceryItemCardWidget(
                        item: item,
                        heroSuffix: const Uuid().v1(),
                      ),
                    );
                  }
                }

                // Displaying LoadingSpinner to indicate waiting state
                return const Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.black26,
                  valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.black, //<-- SEE HERE
                  ),
                ));
              });

          // GroceryItem item = const GroceryItem(
          //     id: "1",
          //     name: "Organic Bananas",
          //     description: "7pcs, Priceg",
          //     price: 4.99,
          //     imagePath: "assets/images/grocery_images/banana.png");
          // items[index].then((value) => item = value);
          // return GestureDetector(
          //   onTap: () {
          //     items[index].then((value) => onItemClicked(context, value));
          //   },
          //   child: GroceryItemCardWidget(
          //     item: item,
          //     heroSuffix: const Uuid().v1(),
          //   ),
          // );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            width: 20,
          );
        },
      ),
    );
  }

  void onItemClicked(BuildContext context, GroceryItem groceryItem) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ProductDetailsScreen(
                groceryItem,
                heroSuffix: "home_screen",
              )),
    );
  }

  Widget subTitle(BuildContext context, String text, String see) {
    return Row(
      children: [
        Text(
          text,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        const Spacer(),
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) {
                return const DashboardScreen(index: 1);
              },
            ));
          },
          child: Text(
            see,
            style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppColors.primaryColor),
          ),
        ),
      ],
    );
  }

  Widget locationWidget() {
    String locationIconPath = "assets/icons/location_icon.svg";
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          locationIconPath,
        ),
        const SizedBox(
          width: 8,
        ),
        const Text(
          "Bosque Market",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  Widget tone() {
    return SizedBox.fromSize(
      size: const Size(60, 60), // button width and height
      child: ClipOval(
        child: Material(
          color: AppColors.primaryColor, // button color
          child: InkWell(
            splashColor: Colors.white, // splash color
            onTap: () {}, // button pressed
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(Icons.notifications_rounded), // icon
                Text("Timbre"), // text
              ],
            ),
          ),
        ),
      ),
    );
  }
}
