import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/repositories/home/home_repository_implement.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeRepositoryImplement homeRepository;

  HomeBloc(this.homeRepository) : super(HomeState.initial()) {
    // on<HomeEvent>((event, emit) async {
    //   await event.when<FutureOr<void>>(
    //       started: () async =>
    //           await _onStartedEvente(emit).whenComplete(() => emit));
    // });
    on<HomeEvent>((event, emit) async {
      await event.when<FutureOr<void>>(started: () => _onStartedEvente(emit));
      // exclusives: () => _onExclusives(emit),
      // solds: () => _onSolds(emit),
      // news: () => _onNews(emit));
    });
  }

  void _onStartedEvente(Emitter<HomeState> emit) async {
    await emit.forEach(homeRepository.offer(), onData: (List<Offers> offer) {
      return state.copyWith(offers: offer.first, isLoading: true);
    });

    // HomeState newState = state;
    // Completer<HomeState> c = Completer<HomeState>();

    // Stream<List<Offers>> streamOffers = await homeRepository.offers();
    // streamOffers.listen((x) async {
    //   newState = newState.copyWith(offers: x.first);
    //   //Exclusive
    //   Stream<List<GroceryItem>> streamExclusives =
    //       await homeRepository.exclusives();
    //   streamExclusives.listen((x) async {
    //     newState = newState.copyWith(exclusives: x);
    //     //Sold
    //     Stream<List<GroceryItem>> streamSolds = await homeRepository.solds();
    //     streamSolds.listen((x) async {
    //       newState = newState.copyWith(solds: x);
    //       //New
    //       Stream<List<GroceryItem>> streamNews = await homeRepository.news();
    //       streamNews.listen((x) async {
    //         newState = newState.copyWith(news: x, isLoading: false);
    //         c.complete(newState);
    //       });
    //     });
    //   });
    // });

    // var stateToReturn = await c.future;
    // emit(stateToReturn);
  }

  // void _onExclusives(Emitter<HomeState> emit) async {
  //   await emit.forEach(homeRepository.exclusive(),
  //       onData: (List<GroceryItem> exclusive) {
  //     return state.copyWith(exclusives: exclusive, isLoading: false);
  //   });
  // }

  // void _onSolds(Emitter<HomeState> emit) async {
  //   await emit.forEach(homeRepository.sold(), onData: (List<GroceryItem> sold) {
  //     return state.copyWith(solds: sold, isLoading: true);
  //   });
  // }

  // void _onNews(Emitter<HomeState> emit) async {
  //   await emit.forEach(homeRepository.newp(),
  //       onData: (List<GroceryItem> grocery) {
  //     return state.copyWith(news: grocery, isLoading: false);
  //   });
  // }

  void _onStartedEvent(Emitter<HomeState> emit) async {
    //HomeState newState = state;
    // homeRepository.offer().listen((offer) {
    //   emit(state.copyWith(offers: offer.first));
    // }, onDone: () {
    //   homeRepository.exclusive().listen((exclusive) {
    //     newState = newState.copyWith(exclusives: exclusive);
    //   }, onDone: () {
    //     homeRepository.sold().listen((sold) {
    //       newState = newState.copyWith(solds: sold);
    //     }, onDone: () {
    //       homeRepository.newp().listen((newp) {
    //         newState = newState.copyWith(news: newp, isLoading: false);
    //       }, onDone: () {
    //         emit.isDone;
    //         emit(newState);
    //       }, onError: (error) {});
    //     }, onError: (error) {});
    //   }, onError: (error) {});
    // }, onError: (error) {});
  }
}
