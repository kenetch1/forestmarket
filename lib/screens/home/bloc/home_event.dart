part of 'home_bloc.dart';

@freezed
class HomeEvent with _$HomeEvent {
  const factory HomeEvent.started() = _Started;
  // const factory HomeEvent.exclusives() = _Exclusives;
  // const factory HomeEvent.solds() = _Solds;
  // const factory HomeEvent.news() = _News;
}
