// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'HomeEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements HomeEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
mixin _$HomeState {
  Offers get offers => throw _privateConstructorUsedError;
  List<GroceryItem> get exclusives => throw _privateConstructorUsedError;
  List<GroceryItem> get solds => throw _privateConstructorUsedError;
  List<GroceryItem> get news => throw _privateConstructorUsedError;
  dynamic get isLoading => throw _privateConstructorUsedError;
  dynamic get showErrorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
  @useResult
  $Res call(
      {Offers offers,
      List<GroceryItem> exclusives,
      List<GroceryItem> solds,
      List<GroceryItem> news,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? offers = null,
    Object? exclusives = null,
    Object? solds = null,
    Object? news = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      offers: null == offers
          ? _value.offers
          : offers // ignore: cast_nullable_to_non_nullable
              as Offers,
      exclusives: null == exclusives
          ? _value.exclusives
          : exclusives // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      solds: null == solds
          ? _value.solds
          : solds // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      news: null == news
          ? _value.news
          : news // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      isLoading: freezed == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as dynamic,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_HomeStateBindingCopyWith<$Res>
    implements $HomeStateCopyWith<$Res> {
  factory _$$_HomeStateBindingCopyWith(
          _$_HomeStateBinding value, $Res Function(_$_HomeStateBinding) then) =
      __$$_HomeStateBindingCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Offers offers,
      List<GroceryItem> exclusives,
      List<GroceryItem> solds,
      List<GroceryItem> news,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class __$$_HomeStateBindingCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$_HomeStateBinding>
    implements _$$_HomeStateBindingCopyWith<$Res> {
  __$$_HomeStateBindingCopyWithImpl(
      _$_HomeStateBinding _value, $Res Function(_$_HomeStateBinding) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? offers = null,
    Object? exclusives = null,
    Object? solds = null,
    Object? news = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_$_HomeStateBinding(
      offers: null == offers
          ? _value.offers
          : offers // ignore: cast_nullable_to_non_nullable
              as Offers,
      exclusives: null == exclusives
          ? _value._exclusives
          : exclusives // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      solds: null == solds
          ? _value._solds
          : solds // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      news: null == news
          ? _value._news
          : news // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      isLoading: freezed == isLoading ? _value.isLoading! : isLoading,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage!
          : showErrorMessage,
    ));
  }
}

/// @nodoc

class _$_HomeStateBinding implements _HomeStateBinding {
  const _$_HomeStateBinding(
      {required this.offers,
      required final List<GroceryItem> exclusives,
      required final List<GroceryItem> solds,
      required final List<GroceryItem> news,
      this.isLoading = true,
      this.showErrorMessage = false})
      : _exclusives = exclusives,
        _solds = solds,
        _news = news;

  @override
  final Offers offers;
  final List<GroceryItem> _exclusives;
  @override
  List<GroceryItem> get exclusives {
    if (_exclusives is EqualUnmodifiableListView) return _exclusives;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_exclusives);
  }

  final List<GroceryItem> _solds;
  @override
  List<GroceryItem> get solds {
    if (_solds is EqualUnmodifiableListView) return _solds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_solds);
  }

  final List<GroceryItem> _news;
  @override
  List<GroceryItem> get news {
    if (_news is EqualUnmodifiableListView) return _news;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_news);
  }

  @override
  @JsonKey()
  final dynamic isLoading;
  @override
  @JsonKey()
  final dynamic showErrorMessage;

  @override
  String toString() {
    return 'HomeState(offers: $offers, exclusives: $exclusives, solds: $solds, news: $news, isLoading: $isLoading, showErrorMessage: $showErrorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HomeStateBinding &&
            (identical(other.offers, offers) || other.offers == offers) &&
            const DeepCollectionEquality()
                .equals(other._exclusives, _exclusives) &&
            const DeepCollectionEquality().equals(other._solds, _solds) &&
            const DeepCollectionEquality().equals(other._news, _news) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.showErrorMessage, showErrorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      offers,
      const DeepCollectionEquality().hash(_exclusives),
      const DeepCollectionEquality().hash(_solds),
      const DeepCollectionEquality().hash(_news),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(showErrorMessage));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_HomeStateBindingCopyWith<_$_HomeStateBinding> get copyWith =>
      __$$_HomeStateBindingCopyWithImpl<_$_HomeStateBinding>(this, _$identity);
}

abstract class _HomeStateBinding implements HomeState {
  const factory _HomeStateBinding(
      {required final Offers offers,
      required final List<GroceryItem> exclusives,
      required final List<GroceryItem> solds,
      required final List<GroceryItem> news,
      final dynamic isLoading,
      final dynamic showErrorMessage}) = _$_HomeStateBinding;

  @override
  Offers get offers;
  @override
  List<GroceryItem> get exclusives;
  @override
  List<GroceryItem> get solds;
  @override
  List<GroceryItem> get news;
  @override
  dynamic get isLoading;
  @override
  dynamic get showErrorMessage;
  @override
  @JsonKey(ignore: true)
  _$$_HomeStateBindingCopyWith<_$_HomeStateBinding> get copyWith =>
      throw _privateConstructorUsedError;
}
