part of 'home_bloc.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState(
      {required Offers offers,
      required List<GroceryItem> exclusives,
      required List<GroceryItem> solds,
      required List<GroceryItem> news,
      @Default(true) isLoading,
      @Default(false) showErrorMessage}) = _HomeStateBinding;

  factory HomeState.initial() => const _HomeStateBinding(
          isLoading: true,
          offers: Offers(
              title: '', code: '', description: '', discount: '0.0', id: ''),
          exclusives: [
            GroceryItem(
                id: '',
                name: 'producto',
                description: 'producto',
                price: 10,
                imagePath: 'assets/images/grocery_images/banana.png')
          ],
          solds: [
            GroceryItem(
                id: '',
                name: 'producto',
                description: 'producto',
                price: 10,
                imagePath: 'assets/images/grocery_images/banana.png')
          ],
          news: [
            GroceryItem(
                id: '',
                name: 'producto',
                description: 'producto',
                price: 10,
                imagePath: 'assets/images/grocery_images/banana.png')
          ]);
}
