import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/screens/home/bloc/home_bloc.dart';
import 'package:foreststore/styles/colors.dart';

class HomeBanner extends StatelessWidget {
  final Offers offers;
  const HomeBanner({super.key, required this.offers});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 500,
      height: 115,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          image: const DecorationImage(
              image: AssetImage(
                "assets/images/banner_background.png",
              ),
              fit: BoxFit.cover)),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Image.asset(
                "assets/images/banner_image.png",
              ),
            ),
          ),
          const Spacer(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: AppText(
                    text: offers.title,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Flexible(
                  child: AppText(
                    text: offers.description,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: AppColors.primaryColor,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
    );
  }
}
