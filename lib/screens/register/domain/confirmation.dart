import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:foreststore/screens/login/failure/value_failure.dart';

class Confirmation extends Equatable {
  factory Confirmation(String input) =>
      Confirmation._(_validatePassword(input));

  const Confirmation._(this.value);

  final Either<ValueFailure, String> value;

  @override
  List<Object?> get props => [value];
}

Either<ValueFailure, String> _validatePassword(String input) {
  if (input.length >= 5) {
    return right(input);
  } else {
    return left(
      ValueFailure.shortPassword(failedValue: input),
    );
  }
}
