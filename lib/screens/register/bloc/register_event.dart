part of 'register_bloc.dart';

@freezed
class RegisterEvent with _$RegisterEvent {
  //const factory LoginEvent.started() = _Started;
  const factory RegisterEvent.emailChanged(String emailString) = _EmailChanged;

  const factory RegisterEvent.passwordChanged(String passwordString) =
      _PasswordChanged;

  const factory RegisterEvent.confirmationChanged(String confirmationString) =
      _ConfirmationChanged;

  const factory RegisterEvent.obscurePasswordToggled() =
      _ObscurePasswordToggled;

  const factory RegisterEvent.registerSubmitted() = _RegisterSubmitted;
}
