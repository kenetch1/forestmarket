import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/repositories/authentication/auth_repository.dart';
import 'package:foreststore/screens/register/domain/confirmation.dart';
import 'package:foreststore/screens/register/domain/email_address.dart';
import 'package:foreststore/screens/register/domain/password.dart';
import 'package:foreststore/screens/register/failure/auth_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'register_event.dart';
part 'register_state.dart';
part 'register_bloc.freezed.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository authRepository;
  RegisterBloc(this.authRepository) : super(RegisterState.initial()) {
    on<RegisterEvent>(
      (event, emit) async {
        await event.when<FutureOr<void>>(
          emailChanged: (emailString) => _onEmailChanged(emit, emailString),
          passwordChanged: (passwordString) =>
              _onPasswordChanged(emit, passwordString),
          confirmationChanged: (confirmationString) =>
              _onConfirmationChanged(emit, confirmationString),
          obscurePasswordToggled: () => _onObscurePasswordToggled(emit),
          registerSubmitted: () => _onRegisterSubmitted(emit),
        );
      },
    );
  }

  void _onEmailChanged(Emitter<RegisterState> emit, String emailString) {
    emit(
      state.copyWith(
        emailAddress: EmailAddress(emailString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onPasswordChanged(Emitter<RegisterState> emit, String passwordString) {
    emit(
      state.copyWith(
        password: Password(passwordString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onConfirmationChanged(
      Emitter<RegisterState> emit, String confirmationString) {
    emit(
      state.copyWith(
        confirmation: Confirmation(confirmationString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onObscurePasswordToggled(Emitter<RegisterState> emit) {
    emit(state.copyWith(obscurePassword: !state.obscurePassword));
  }

  Future<void> _onRegisterSubmitted(Emitter<RegisterState> emit) async {
    final isEmailValid = state.emailAddress.value.isRight();
    final isPasswordValid = state.password.value.isRight();
    final isConfirmationValid = state.confirmation.value.isRight();

    if (isEmailValid &&
        isPasswordValid &&
        (isPasswordValid == isConfirmationValid)) {
      emit(
        state.copyWith(
          isSubmitting: true,
          authFailureOrSuccess: null,
        ),
      );

      // Perform network request to get a token.

      try {
        String valueEmail = state.emailAddress.value.fold((l) => '', (r) => r);
        String valuePassw = state.password.value.fold((l) => '', (r) => r);
        String valueConfirm =
            state.confirmation.value.fold((l) => '', (r) => r);
        if (valuePassw.compareTo(valueConfirm) != 0) {
          emit(
            state.copyWith(
                isSubmitting: false,
                showErrorMessage: true,
                authFailureOrSuccess: left(
                    const AuthFailure.invalidEmailAndPasswordCombination(
                        "Valide confirmación"))),
          );
          return;
        }

        await authRepository.createUserWithEmailAndPassword(
          email: valueEmail,
          password: valuePassw,
        );
        emit(
          state.copyWith(
            isSubmitting: false,
            showErrorMessage: true,
            authFailureOrSuccess:
                (isEmailValid && isPasswordValid) ? right(unit) : null,
          ),
        );
      } catch (e) {
        emit(
          state.copyWith(
              isSubmitting: false,
              showErrorMessage: true,
              authFailureOrSuccess: left(
                  AuthFailure.invalidEmailAndPasswordCombination(
                      e.toString()))),
        );
      }
    } else {
      emit(
        state.copyWith(
          isSubmitting: false,
          showErrorMessage: true,

          // Depending on the response received from the server after loggin in,
          // emit proper authFailureOrSuccess.

          // For now we will just see if the email and password were valid or not
          // and accordingly set authFailureOrSuccess' value.

          authFailureOrSuccess: (isEmailValid &&
                  isPasswordValid &&
                  (isPasswordValid == isConfirmationValid))
              ? right(unit)
              : null,
        ),
      );
    }
  }
}
