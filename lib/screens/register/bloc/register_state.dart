part of 'register_bloc.dart';

@freezed
class RegisterState with _$RegisterState {
  const factory RegisterState({
    required EmailAddress emailAddress,
    required Password password,
    required Confirmation confirmation,
    @Default(false) bool isSubmitting,
    @Default(false) bool showErrorMessage,
    @Default(true) bool obscurePassword,
    Either<AuthFailure, Unit>? authFailureOrSuccess,
    // Unit comes from Dartz package and is equivalent to void.
  }) = _RegisterState;

  factory RegisterState.initial() => RegisterState(
      emailAddress: EmailAddress(''),
      password: Password(''),
      confirmation: Confirmation(''));
}
