import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/screens/register/bloc/register_bloc.dart';

class ConfirmationInput extends StatelessWidget {
  const ConfirmationInput({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
      builder: (context, state) {
        return TextFormField(
          decoration: InputDecoration(
            prefixIcon: const Icon(Icons.lock),
            suffixIcon: IconButton(
              onPressed: () => context
                  .read<RegisterBloc>()
                  .add(const RegisterEvent.obscurePasswordToggled()),
              icon: state.obscurePassword
                  ? const Icon(Icons.visibility)
                  : const Icon(Icons.visibility_off),
            ),
            labelText: 'Confirmacion',
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          autocorrect: false,
          obscureText: state.obscurePassword,
          onChanged: (value) => context
              .read<RegisterBloc>()
              .add(RegisterEvent.confirmationChanged(value)),
          validator: (_) => state.confirmation.value.fold<String?>(
            (f) => f.maybeMap<String?>(
              shortPassword: (_) => 'Short Password',
              orElse: () => null,
            ),
            (_) => null,
          ),
          autovalidateMode: state.showErrorMessage == true
              ? AutovalidateMode.always
              : AutovalidateMode.disabled,
        );
      },
    );
  }
}
