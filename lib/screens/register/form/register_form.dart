import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/screens/register/bloc/register_bloc.dart';
import 'package:foreststore/screens/register/form/confirmation_input.dart';
import 'package:foreststore/screens/register/form/email_input.dart';
import 'package:foreststore/screens/register/form/loading_indicator.dart';
import 'package:foreststore/screens/register/form/password_input.dart';

class RegisterForm extends StatelessWidget {
  const RegisterForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
          final authFailureOrSuccess = state.authFailureOrSuccess;

          if (authFailureOrSuccess != null) {
            authFailureOrSuccess.fold(
              (failure) {
                // Do something to handle failure. For example, show a
                // snackbar saying "Invalid Email and Password Combination" or
                // "Server Error" depending on the failure.

                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      failure.when<String>(
                        invalidEmailAndPasswordCombination: (message) =>
                            message,
                        serverError: () => 'Server Error!',
                      ),
                    ),
                  ),
                );
              },
              (success) {
                Navigator.pop(context);
              },
            );
          }
        },
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            child: Column(
              children: [
                SvgPicture.asset("assets/icons/app_icon_color.svg"),
                const SizedBox(
                  height: 30,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: EmailInput(),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
                  child: PasswordInput(),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ConfirmationInput(),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: AppButton(
                    label: "Register",
                    fontWeight: FontWeight.w600,
                    padding: const EdgeInsets.symmetric(vertical: 25),
                    onPressed: () {
                      context.read<RegisterBloc>().add(
                            const RegisterEvent.registerSubmitted(),
                          );
                    },
                  ),
                ),
                const SizedBox(height: 15),
                const LoadingIndicator(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
