import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/repositories/explore/explore_repository_implement.dart';
import 'package:foreststore/screens/product_details/product_details_screen.dart';
import 'package:foreststore/screens/products/bloc/products_bloc.dart';
import 'package:foreststore/widgets/grocery_item_card_widget.dart';

import '../explore/filter_screen.dart';

class CategoryItemsScreen extends StatelessWidget {
  final CategoryItem categoryItem;
  const CategoryItemsScreen({super.key, required this.categoryItem});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProductsBloc>(
        create: (context) {
          return ProductsBloc(ExploreRepositoryImplement())
            ..add(ProductsEvent.started(categoryItem.id));
        },
        child: Builder(
            builder: (context) => BlocBuilder<ProductsBloc, ProductsState>(
                    builder: (context, state) {
                  return buildPage(context);
                })));
  }

  Widget buildPage(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.only(left: 25),
            child: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (contextFilter) => FilterScreen(
                          back: true,
                          onPressedForProd: (prod) {
                            context.read<ProductsBloc>().add(
                                ProductsEvent.prodFilter(
                                    categoryItem.id, prod));
                          },
                        )),
              );
            },
            child: Container(
              padding: const EdgeInsets.only(right: 25),
              child: const Icon(
                Icons.sort,
                color: Colors.black,
              ),
            ),
          ),
        ],
        title: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
          ),
          child: AppText(
            text: categoryItem.name,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: SafeArea(
        child: BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) {
            if (state.isLoading) {
              return const Center(
                  child: CircularProgressIndicator(
                backgroundColor: Colors.black26,
                valueColor: AlwaysStoppedAnimation<Color>(
                  Colors.black, //<-- SEE HERE
                ),
              ));
            } else {
              if (state.products.isEmpty) {
                return const Center(
                  child: AppText(
                    text: 'No hay productos',
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                );
              } else {
                return RefreshIndicator(
                  onRefresh: () async {
                    await _onRefresh(context);
                  },
                  child: StaggeredGridView.count(
                    crossAxisCount: 4,
                    staggeredTiles: state.products
                        .map<StaggeredTile>((_) => const StaggeredTile.fit(2))
                        .toList(),
                    mainAxisSpacing: 3.0,
                    crossAxisSpacing: 0.0,
                    // I only need two card horizontally
                    children: state.products.asMap().entries.map<Widget>((e) {
                      GroceryItem groceryItem = e.value;
                      return GestureDetector(
                        onTap: () {
                          onItemClicked(context, groceryItem);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(10),
                          child: GroceryItemCardWidget(
                            item: groceryItem,
                            heroSuffix: e.value.id,
                          ),
                        ),
                      );
                    }).toList(), // add some space
                  ),
                );
              }
            }
          },
        ),
      ),
    );
  }

  Future<void> _onRefresh(BuildContext context) async {
    //await Future.delayed(const Duration(milliseconds: 1500));
    context.read<ProductsBloc>().add(ProductsEvent.started(categoryItem.id));
  }

  void onItemClicked(BuildContext context, GroceryItem groceryItem) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductDetailsScreen(
          groceryItem,
          heroSuffix: "explore_screen",
        ),
      ),
    );
  }
}
