part of 'products_bloc.dart';

@freezed
class ProductsEvent with _$ProductsEvent {
  const factory ProductsEvent.started(String subcategorieId) = _Started;
  const factory ProductsEvent.prodFilter(
      String subcategorieId, String product) = _ProductFilter;
}
