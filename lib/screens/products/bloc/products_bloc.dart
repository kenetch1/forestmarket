import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'products_event.dart';
part 'products_state.dart';
part 'products_bloc.freezed.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ExploreRepository exploreRepository;
  ProductsBloc(this.exploreRepository) : super(ProductsState.initial()) {
    on<ProductsEvent>((event, emit) async {
      await event.when<FutureOr<void>>(
          started: (subcategoriaId) => _onStarted(emit, subcategoriaId),
          prodFilter: (subcategorieId, product) =>
              _onProductFilter(emit, subcategorieId, product));
    });
  }

  void _onStarted(Emitter<ProductsState> emit, String subcategorieId) async {
    await emit.forEach(exploreRepository.products(subcategorieId),
        onData: (List<GroceryItem> product) {
      return state.copyWith(products: product, isLoading: false);
    });
  }

  void _onProductFilter(Emitter<ProductsState> emit, String subcategorieId,
      String product) async {
    await emit
        .forEach(exploreRepository.productsForProduct(subcategorieId, product),
            onData: (List<GroceryItem> product) {
      return state.copyWith(products: product, isLoading: false);
    });
  }
}
