part of 'products_bloc.dart';

@freezed
class ProductsState with _$ProductsState {
  const factory ProductsState(
      {required List<GroceryItem> products,
      @Default(true) isLoading,
      @Default(false) showErrorMessage}) = _ProductsStateBinding;

  factory ProductsState.initial() => const _ProductsStateBinding(products: [
        GroceryItem(
            id: '', name: '', description: '', imagePath: '', price: 0.0)
      ]);
}
