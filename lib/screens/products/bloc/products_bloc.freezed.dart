// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'products_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProductsEvent {
  String get subcategorieId => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String subcategorieId) started,
    required TResult Function(String subcategorieId, String product) prodFilter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String subcategorieId)? started,
    TResult? Function(String subcategorieId, String product)? prodFilter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String subcategorieId)? started,
    TResult Function(String subcategorieId, String product)? prodFilter,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_ProductFilter value) prodFilter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_ProductFilter value)? prodFilter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_ProductFilter value)? prodFilter,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductsEventCopyWith<ProductsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductsEventCopyWith<$Res> {
  factory $ProductsEventCopyWith(
          ProductsEvent value, $Res Function(ProductsEvent) then) =
      _$ProductsEventCopyWithImpl<$Res, ProductsEvent>;
  @useResult
  $Res call({String subcategorieId});
}

/// @nodoc
class _$ProductsEventCopyWithImpl<$Res, $Val extends ProductsEvent>
    implements $ProductsEventCopyWith<$Res> {
  _$ProductsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subcategorieId = null,
  }) {
    return _then(_value.copyWith(
      subcategorieId: null == subcategorieId
          ? _value.subcategorieId
          : subcategorieId // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res>
    implements $ProductsEventCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String subcategorieId});
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$ProductsEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subcategorieId = null,
  }) {
    return _then(_$_Started(
      null == subcategorieId
          ? _value.subcategorieId
          : subcategorieId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started(this.subcategorieId);

  @override
  final String subcategorieId;

  @override
  String toString() {
    return 'ProductsEvent.started(subcategorieId: $subcategorieId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Started &&
            (identical(other.subcategorieId, subcategorieId) ||
                other.subcategorieId == subcategorieId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, subcategorieId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      __$$_StartedCopyWithImpl<_$_Started>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String subcategorieId) started,
    required TResult Function(String subcategorieId, String product) prodFilter,
  }) {
    return started(subcategorieId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String subcategorieId)? started,
    TResult? Function(String subcategorieId, String product)? prodFilter,
  }) {
    return started?.call(subcategorieId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String subcategorieId)? started,
    TResult Function(String subcategorieId, String product)? prodFilter,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(subcategorieId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_ProductFilter value) prodFilter,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_ProductFilter value)? prodFilter,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_ProductFilter value)? prodFilter,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements ProductsEvent {
  const factory _Started(final String subcategorieId) = _$_Started;

  @override
  String get subcategorieId;
  @override
  @JsonKey(ignore: true)
  _$$_StartedCopyWith<_$_Started> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ProductFilterCopyWith<$Res>
    implements $ProductsEventCopyWith<$Res> {
  factory _$$_ProductFilterCopyWith(
          _$_ProductFilter value, $Res Function(_$_ProductFilter) then) =
      __$$_ProductFilterCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String subcategorieId, String product});
}

/// @nodoc
class __$$_ProductFilterCopyWithImpl<$Res>
    extends _$ProductsEventCopyWithImpl<$Res, _$_ProductFilter>
    implements _$$_ProductFilterCopyWith<$Res> {
  __$$_ProductFilterCopyWithImpl(
      _$_ProductFilter _value, $Res Function(_$_ProductFilter) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subcategorieId = null,
    Object? product = null,
  }) {
    return _then(_$_ProductFilter(
      null == subcategorieId
          ? _value.subcategorieId
          : subcategorieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ProductFilter implements _ProductFilter {
  const _$_ProductFilter(this.subcategorieId, this.product);

  @override
  final String subcategorieId;
  @override
  final String product;

  @override
  String toString() {
    return 'ProductsEvent.prodFilter(subcategorieId: $subcategorieId, product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductFilter &&
            (identical(other.subcategorieId, subcategorieId) ||
                other.subcategorieId == subcategorieId) &&
            (identical(other.product, product) || other.product == product));
  }

  @override
  int get hashCode => Object.hash(runtimeType, subcategorieId, product);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProductFilterCopyWith<_$_ProductFilter> get copyWith =>
      __$$_ProductFilterCopyWithImpl<_$_ProductFilter>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String subcategorieId) started,
    required TResult Function(String subcategorieId, String product) prodFilter,
  }) {
    return prodFilter(subcategorieId, product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String subcategorieId)? started,
    TResult? Function(String subcategorieId, String product)? prodFilter,
  }) {
    return prodFilter?.call(subcategorieId, product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String subcategorieId)? started,
    TResult Function(String subcategorieId, String product)? prodFilter,
    required TResult orElse(),
  }) {
    if (prodFilter != null) {
      return prodFilter(subcategorieId, product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_ProductFilter value) prodFilter,
  }) {
    return prodFilter(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_ProductFilter value)? prodFilter,
  }) {
    return prodFilter?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_ProductFilter value)? prodFilter,
    required TResult orElse(),
  }) {
    if (prodFilter != null) {
      return prodFilter(this);
    }
    return orElse();
  }
}

abstract class _ProductFilter implements ProductsEvent {
  const factory _ProductFilter(
      final String subcategorieId, final String product) = _$_ProductFilter;

  @override
  String get subcategorieId;
  String get product;
  @override
  @JsonKey(ignore: true)
  _$$_ProductFilterCopyWith<_$_ProductFilter> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ProductsState {
  List<GroceryItem> get products => throw _privateConstructorUsedError;
  dynamic get isLoading => throw _privateConstructorUsedError;
  dynamic get showErrorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductsStateCopyWith<ProductsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductsStateCopyWith<$Res> {
  factory $ProductsStateCopyWith(
          ProductsState value, $Res Function(ProductsState) then) =
      _$ProductsStateCopyWithImpl<$Res, ProductsState>;
  @useResult
  $Res call(
      {List<GroceryItem> products,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class _$ProductsStateCopyWithImpl<$Res, $Val extends ProductsState>
    implements $ProductsStateCopyWith<$Res> {
  _$ProductsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? products = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      products: null == products
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      isLoading: freezed == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as dynamic,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProductsStateBindingCopyWith<$Res>
    implements $ProductsStateCopyWith<$Res> {
  factory _$$_ProductsStateBindingCopyWith(_$_ProductsStateBinding value,
          $Res Function(_$_ProductsStateBinding) then) =
      __$$_ProductsStateBindingCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<GroceryItem> products,
      dynamic isLoading,
      dynamic showErrorMessage});
}

/// @nodoc
class __$$_ProductsStateBindingCopyWithImpl<$Res>
    extends _$ProductsStateCopyWithImpl<$Res, _$_ProductsStateBinding>
    implements _$$_ProductsStateBindingCopyWith<$Res> {
  __$$_ProductsStateBindingCopyWithImpl(_$_ProductsStateBinding _value,
      $Res Function(_$_ProductsStateBinding) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? products = null,
    Object? isLoading = freezed,
    Object? showErrorMessage = freezed,
  }) {
    return _then(_$_ProductsStateBinding(
      products: null == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<GroceryItem>,
      isLoading: freezed == isLoading ? _value.isLoading! : isLoading,
      showErrorMessage: freezed == showErrorMessage
          ? _value.showErrorMessage!
          : showErrorMessage,
    ));
  }
}

/// @nodoc

class _$_ProductsStateBinding implements _ProductsStateBinding {
  const _$_ProductsStateBinding(
      {required final List<GroceryItem> products,
      this.isLoading = true,
      this.showErrorMessage = false})
      : _products = products;

  final List<GroceryItem> _products;
  @override
  List<GroceryItem> get products {
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  @JsonKey()
  final dynamic isLoading;
  @override
  @JsonKey()
  final dynamic showErrorMessage;

  @override
  String toString() {
    return 'ProductsState(products: $products, isLoading: $isLoading, showErrorMessage: $showErrorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductsStateBinding &&
            const DeepCollectionEquality().equals(other._products, _products) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.showErrorMessage, showErrorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_products),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(showErrorMessage));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProductsStateBindingCopyWith<_$_ProductsStateBinding> get copyWith =>
      __$$_ProductsStateBindingCopyWithImpl<_$_ProductsStateBinding>(
          this, _$identity);
}

abstract class _ProductsStateBinding implements ProductsState {
  const factory _ProductsStateBinding(
      {required final List<GroceryItem> products,
      final dynamic isLoading,
      final dynamic showErrorMessage}) = _$_ProductsStateBinding;

  @override
  List<GroceryItem> get products;
  @override
  dynamic get isLoading;
  @override
  dynamic get showErrorMessage;
  @override
  @JsonKey(ignore: true)
  _$$_ProductsStateBindingCopyWith<_$_ProductsStateBinding> get copyWith =>
      throw _privateConstructorUsedError;
}
