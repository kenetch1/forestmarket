import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:foreststore/repositories/explore/explore_repository_implement.dart';
import 'package:foreststore/screens/product_details/product_details_screen.dart';
import 'package:foreststore/widgets/grocery_item_card_widget.dart';

class ProductItemsScreen extends StatelessWidget {
  final ExploreRepository repository = ExploreRepositoryImplement();
  final String product;
  ProductItemsScreen({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: repository.forProduct(product),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Scaffold(
                body: Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.cyanAccent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
              ),
            ));
          } else if (snapshot.connectionState == ConnectionState.active ||
              snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return messageEvent(context, 'Hubo un error');
            } else if (snapshot.hasData) {
              if (snapshot.data!.isEmpty) {
                return messageEvent(context, 'No hay datos');
              } else {
                return buildPage(context, snapshot.data!);
              }
            }
          } else {
            return Text('State: ${snapshot.connectionState}');
          }
          return const Text('No hay datos');
        });
  }

  Scaffold messageEvent(BuildContext context, String message) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.only(left: 25),
            child: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        title: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
          ),
          child: AppText(
            text: product,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: Center(
        child: AppText(
          text: message,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, List<GroceryItem> state) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.only(left: 25),
            child: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        title: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
          ),
          child: AppText(
            text: product,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: SafeArea(
          child: StaggeredGridView.count(
        crossAxisCount: 4,
        staggeredTiles: state
            .map<StaggeredTile>((_) => const StaggeredTile.fit(2))
            .toList(),
        mainAxisSpacing: 3.0,
        crossAxisSpacing: 0.0,
        // I only need two card horizontally
        children: state.map<Widget>((e) {
          GroceryItem groceryItem = e;
          return GestureDetector(
            onTap: () {
              onItemClicked(context, groceryItem);
            },
            child: Container(
              padding: const EdgeInsets.all(10),
              child: GroceryItemCardWidget(
                item: groceryItem,
                heroSuffix: e.id,
              ),
            ),
          );
        }).toList(), // add some space
      )),
    );
  }

  void onItemClicked(BuildContext context, GroceryItem groceryItem) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProductDetailsScreen(
          groceryItem,
          heroSuffix: "explore_screen",
        ),
      ),
    );
  }
}
