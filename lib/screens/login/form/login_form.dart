import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/forgot/forgot_screen.dart';
import 'package:foreststore/screens/login/bloc/login_bloc.dart';
import 'package:foreststore/screens/login/form/email_input.dart';
import 'package:foreststore/screens/login/form/loading_indicator.dart';
import 'package:foreststore/screens/login/form/password_input.dart';
import 'package:foreststore/screens/register/register_screen.dart';
import 'package:foreststore/styles/colors.dart';

//kenetch@gmail.com | testtest

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {
          final authFailureOrSuccess = state.authFailureOrSuccess;

          if (authFailureOrSuccess != null) {
            authFailureOrSuccess.fold(
              (failure) {
                // Do something to handle failure. For example, show a
                // snackbar saying "Invalid Email and Password Combination" or
                // "Server Error" depending on the failure.

                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      failure.when<String>(
                        invalidEmailAndPasswordCombination: (message) =>
                            message,
                        serverError: () => 'Server Error!',
                      ),
                    ),
                  ),
                );
              },
              (success) {
                // ScaffoldMessenger.of(context).showSnackBar(
                //   const SnackBar(
                //     content: Text('Login exitoso...'),
                //     backgroundColor: AppColors.primaryColor,
                //   ),
                // );
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) {
                    return DashboardScreen(index: 0);
                  },
                ));
              },
            );
          }
        },
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            child: Column(
              children: [
                SvgPicture.asset("assets/icons/app_icon_color.svg"),
                const SizedBox(
                  height: 5,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
                  child: EmailInput(),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: PasswordInput(),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: AppButton(
                    label: "Login",
                    fontWeight: FontWeight.w600,
                    padding: const EdgeInsets.symmetric(vertical: 25),
                    onPressed: () {
                      context.read<LoginBloc>().add(
                            const LoginEvent.loginSubmitted(),
                          );
                    },
                  ),
                ),
                const SizedBox(height: 5),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const RegisterScreen();
                      },
                    ));
                  },
                  child: const Text('¿Ya tienes cuenta?, Registrate',
                      style: TextStyle(
                          color: AppColors.darkGrey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold)),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const ForgotScreen();
                      },
                    ));
                  },
                  child: const Text('¿Olvidates tu password?',
                      style: TextStyle(
                          color: AppColors.darkGrey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold)),
                ),
                const SizedBox(height: 15),
                const LoadingIndicator(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
