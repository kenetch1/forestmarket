import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/repositories/authentication/firebase_auth_repository.dart';
import 'package:foreststore/screens/login/bloc/login_bloc.dart';
import 'package:foreststore/screens/login/form/login_form.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => WidgetsBinding.instance.focusManager.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          automaticallyImplyLeading: false,
          // leading: GestureDetector(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Container(
          //     padding: const EdgeInsets.only(left: 25),
          //     child: const Icon(
          //       Icons.arrow_back_ios,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
          title: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: const AppText(
              text: "Login",
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        body: BlocProvider(
          create: (context) => LoginBloc(
              FirebaseAuthService(authService: FirebaseAuth.instance)),
          child: const LoginForm(),
        ),
      ),
    );
  }
}
