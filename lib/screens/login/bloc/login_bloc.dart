import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:foreststore/repositories/authentication/auth_repository.dart';
import 'package:foreststore/screens/login/domain/email_address.dart';
import 'package:foreststore/screens/login/domain/password.dart';
import 'package:foreststore/screens/login/failure/auth_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository authRepository;
  LoginBloc(this.authRepository) : super(LoginState.initial()) {
    on<LoginEvent>(
      (event, emit) async {
        await event.when<FutureOr<void>>(
          emailChanged: (emailString) => _onEmailChanged(emit, emailString),
          passwordChanged: (passwordString) =>
              _onPasswordChanged(emit, passwordString),
          obscurePasswordToggled: () => _onObscurePasswordToggled(emit),
          loginSubmitted: () => _onLoginSubmitted(emit),
        );
      },
    );
  }

  void _onEmailChanged(Emitter<LoginState> emit, String emailString) {
    emit(
      state.copyWith(
        emailAddress: EmailAddress(emailString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onPasswordChanged(Emitter<LoginState> emit, String passwordString) {
    emit(
      state.copyWith(
        password: Password(passwordString),
        authFailureOrSuccess: null,
      ),
    );
  }

  void _onObscurePasswordToggled(Emitter<LoginState> emit) {
    emit(state.copyWith(obscurePassword: !state.obscurePassword));
  }

  Future<void> _onLoginSubmitted(Emitter<LoginState> emit) async {
    final isEmailValid = state.emailAddress.value.isRight();
    final isPasswordValid = state.password.value.isRight();

    if (isEmailValid && isPasswordValid) {
      emit(
        state.copyWith(
          isSubmitting: true,
          authFailureOrSuccess: null,
        ),
      );

      // Perform network request to get a token.

      try {
        String valueEmail = state.emailAddress.value.fold((l) => '', (r) => r);
        String valuePassw = state.password.value.fold((l) => '', (r) => r);
        var user = await authRepository.signInWithEmailAndPassword(
          email: valueEmail,
          password: valuePassw,
        );
        final prefs = await SharedPreferences.getInstance();
        // Save an String value to 'user' key.
        await prefs.setString('user', user.toJsonString());
        emit(
          state.copyWith(
            isSubmitting: false,
            showErrorMessage: true,
            authFailureOrSuccess:
                (isEmailValid && isPasswordValid) ? right(unit) : null,
          ),
        );
      } catch (e) {
        emit(
          state.copyWith(
              isSubmitting: false,
              showErrorMessage: true,
              authFailureOrSuccess: left(
                  AuthFailure.invalidEmailAndPasswordCombination(
                      e.toString()))),
        );
      }
    } else {
      emit(
        state.copyWith(
          isSubmitting: false,
          showErrorMessage: true,
          authFailureOrSuccess:
              (isEmailValid && isPasswordValid) ? right(unit) : null,
        ),
      );
    }
  }
}
