import 'package:flutter/cupertino.dart';
import 'package:foreststore/common_widget/app_text.dart';

class FavouriteScreen extends StatelessWidget {
  const FavouriteScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: AppText(
          text: "No Favorite Items",
          fontWeight: FontWeight.w600,
          color: Color(0xFF7C7C7C),
          fontSize: 20,
        ),
      ),
    );
  }
}
