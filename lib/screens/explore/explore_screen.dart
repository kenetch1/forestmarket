import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/repositories/explore/explore_repository_implement.dart';
import 'package:foreststore/screens/explore/bloc/explore_bloc.dart';
import 'package:foreststore/screens/subcategories/subcategories_screen.dart';
import 'package:foreststore/widgets/category_item_card_widget.dart';

List<Color> gridColors = [
  const Color(0xff53B175),
  const Color(0xffF8A44C),
  const Color(0xffF7A593),
  const Color(0xffD3B0E0),
  const Color(0xffFDE598),
  const Color(0xffB7DFF5),
  const Color(0xff836AF6),
  const Color(0xffD73B77),
];

class ExploreScreen extends StatelessWidget {
  const ExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ExploreBloc>(
        create: (context) {
          return ExploreBloc(ExploreRepositoryImplement())
            ..add(const ExploreEvent.started());
        },
        child: Builder(builder: (context) => buildPage(context)));
  }

  Widget buildPage(BuildContext context) {
    return Scaffold(body: SafeArea(
      child: BlocBuilder<ExploreBloc, ExploreState>(
        builder: (context, state) {
          if (state.isLoading) {
            return const Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.black26,
              valueColor: AlwaysStoppedAnimation<Color>(
                Colors.black, //<-- SEE HERE
              ),
            ));
          } else {
            return Column(
              children: [
                getHeader(),
                Expanded(
                  child: getStaggeredGridView(context, state),
                ),
              ],
            );
          }
        },
      ),
    ));
  }

  Widget getHeader() {
    return Column(
      children: const [
        SizedBox(
          height: 20,
        ),
        Center(
          child: AppText(
            text: "Categorias",
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        // SizedBox(
        //   height: 20,
        // ),
        // Padding(
        //   padding: EdgeInsets.symmetric(horizontal: 10),
        //   child: SearchBarWidget(),
        // ),
      ],
    );
  }

  Widget getStaggeredGridView(BuildContext context, ExploreState state) {
    return StaggeredGridView.count(
      crossAxisCount: 4,
      //Here is the place that we are getting flexible/ dynamic card for various images
      staggeredTiles: state.categories
          .map<StaggeredTile>((_) => const StaggeredTile.fit(2))
          .toList(),
      mainAxisSpacing: 3.0,
      crossAxisSpacing: 4.0,
      children: state.categories.asMap().entries.map<Widget>((e) {
        int index = e.key;
        CategoryItem categoryItem = e.value;
        return GestureDetector(
          onTap: () {
            onCategoryItemClicked(context, categoryItem);
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            child: CategoryItemCardWidget(
              item: categoryItem,
              color: gridColors[index % gridColors.length],
            ),
          ),
        );
      }).toList(), // add some space
    );
  }

  void onCategoryItemClicked(BuildContext context, CategoryItem categoryItem) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return SubcategoriesScreen(categoryItem: categoryItem);
      },
    ));
  }
}
