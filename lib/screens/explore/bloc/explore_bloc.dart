import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'explore_event.dart';
part 'explore_state.dart';
part 'explore_bloc.freezed.dart';

class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
  final ExploreRepository exploreRepository;

  ExploreBloc(this.exploreRepository) : super(ExploreState.initial()) {
    on<ExploreEvent>((event, emit) async {
      await event.when<FutureOr<void>>(started: () => _onStarted(emit));
    });
  }

  void _onStarted(Emitter<ExploreState> emit) async {
    await emit.forEach(exploreRepository.home(),
        onData: (List<CategoryItem> categories) {
      return state.copyWith(categories: categories, isLoading: false);
    });
  }
}
