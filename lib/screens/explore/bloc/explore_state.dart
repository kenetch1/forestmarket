part of 'explore_bloc.dart';

@freezed
class ExploreState with _$ExploreState {
  const factory ExploreState(
      {required List<CategoryItem> categories,
      @Default(true) isLoading,
      @Default(false) showErrorMessage}) = _ExploreStateBinding;

  factory ExploreState.initial() => _ExploreStateBinding(
      categories: [CategoryItem(id: '', name: '', imagePath: '')]);
}
