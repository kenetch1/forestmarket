import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/notification/fcm_post_message.dart';
import 'package:foreststore/notification/notification.dart';
import 'package:foreststore/repositories/users/firebase_users_repository.dart';
import 'package:foreststore/repositories/users/users_repository.dart';
import 'package:foreststore/screens/dashboard/dashboard_delivery_screen.dart';
import 'package:foreststore/screens/dashboard/dashboard_screen.dart';
import 'package:foreststore/screens/login/login_screen.dart';
import 'package:foreststore/styles/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:foreststore/widgets/alert_accept.dart';
import 'package:foreststore/widgets/dialog_yes_or_no.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:whatsapp/whatsapp.dart';
import 'package:whatsapp_share/whatsapp_share.dart';
import 'package:http/http.dart' as http;

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final String imagePath = "assets/images/welcome.png";

  final UsersRepository usersRepository = FirebaseUsersRepository();

  WhatsApp whatsapp = WhatsApp();
  int phoneNumber = 525628087623;

  Future<void> isInstalled() async {
    final val = await WhatsappShare.isInstalled(package: Package.whatsapp);
    debugPrint('Whatsapp is installed: $val');
  }

  Future<void> share() async {
    await WhatsappShare.share(
      text: 'Example share text',
      linkUrl: 'https://flutter.dev/',
      phone: '525617233312',
    );
  }

  Future<http.Response> sendMessage(String message) {
    String responseData =
        '{ "messaging_product": "whatsapp", "to": "525617233312", "type": "text", "text": {"body": "$message"}}';
    final whatsapp = json.decode(responseData);

    return http.post(
      Uri.parse('https://graph.facebook.com/v15.0/105250985832854/messages'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            'Bearer EAANCStgFq54BABxGS1GUvJjQwZCEJEHsGOZC39VWH94FBvKBai8hNNC217nYZAxTG8CltO2XMwQru7iCE8agiWgjeSpW8M1b6CcgL46cZACKKBJYTApvZBxtWKnKvbaZAObUOZC613ZBvBHlrlNRwRcc1FHZAZBf9Oiqn9HsI8ZAkwkEbhdlVA4gLPLQfiJjd5jffuZAVXDgrHc7kQZDZD'
      },
      body: jsonEncode(whatsapp),
    );
  }

  void openWhatsapp(
      {required BuildContext context,
      required String text,
      required String number}) async {
    var whatsapp = number; //+92xx enter like this
    var whatsappURlAndroid = "whatsapp://send?phone= $whatsapp&text=$text";
    var whatsappURLIos = "https://wa.me/$whatsapp?text=${Uri.tryParse(text)}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunchUrl(Uri.parse(whatsappURLIos))) {
        await launchUrl(Uri.parse(
          whatsappURLIos,
        ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Whatsapp not installed")));
      }
    } else {
      // android , web
      if (await canLaunchUrl(Uri.parse(whatsappURlAndroid))) {
        await launchUrl(Uri.parse(whatsappURlAndroid));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Whatsapp not installed")));
      }
    }
  }

  @override
  void initState() {
    final firebaseMessaging = FCM();
    firebaseMessaging.setNotifications();

    firebaseMessaging.streamCtlr.stream.listen(_changeData);
    firebaseMessaging.bodyCtlr.stream.listen(_changeBody);
    firebaseMessaging.titleCtlr.stream.listen(_changeTitle);
    isInstalled();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      FCM.setContext(context);
    });
  }

  _changeData(NotificationContext notif) => debugPrint(notif.message);
  _changeBody(NotificationContext notif) => debugPrint(notif.message);
  _changeTitle(NotificationContext notif) async {
    // final action = await DialogsYesOrNo.yesAbortDialog(
    //     notif.context, notif.message, 'My Body');
    // if (action == DialogAction.yes) {
    //   debugPrint('YES');
    // } else {
    //   debugPrint('YES');
    // }
    showCupertinoModalPopup(
        context: notif.context,
        builder: (context) =>
            AlertAccept(title: notif.title, message: notif.message));
    // Navigator.of(context).push(MaterialPageRoute(
    //   builder: (BuildContext context) {
    //     return AlertAccept(title: 'Noficacion', message: notif.message);
    //   },
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.primaryColor,
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(imagePath),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                const Spacer(),
                icon(),
                const SizedBox(
                  height: 5,
                ),
                tone(),
                const SizedBox(
                  height: 20,
                ),
                welcomeTextWidget(context),
                const SizedBox(
                  height: 10,
                ),
                sloganText(context),
                const SizedBox(
                  height: 40,
                ),
                getButton(context),
                const SizedBox(
                  height: 40,
                )
              ],
            ),
          ),
        ));
  }

  Widget icon() {
    String iconPath = "assets/icons/manznamarket.svg";
    return SvgPicture.asset(
      iconPath,
      width: 48,
      height: 56,
    );
  }

  Widget welcomeTextWidget(BuildContext context) {
    return Column(
      children: [
        AppText(
          text: AppLocalizations.of(context)!.welcome_title,
          fontSize: 48,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
        AppText(
          text: AppLocalizations.of(context)!.welcome_subtitle,
          fontSize: 48,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
      ],
    );
  }

  Widget sloganText(BuildContext context) {
    return AppText(
      text: AppLocalizations.of(context)!.welcome_message,
      fontSize: 16,
      fontWeight: FontWeight.w600,
      color: const Color(0xffFCFCFC).withOpacity(0.7),
    );
  }

  Widget getButton(BuildContext context) {
    return AppButton(
      label: AppLocalizations.of(context)!.welcome_started,
      fontWeight: FontWeight.w600,
      padding: const EdgeInsets.symmetric(vertical: 25),
      onPressed: () {
        onGetStartedClicked(context);
      },
    );
  }

  Widget tone() {
    return SizedBox.fromSize(
      size: const Size(60, 60), // button width and height
      child: ClipOval(
        child: Material(
          color: AppColors.primaryColor, // button color
          child: InkWell(
            splashColor: Colors.white, // splash color
            onTap: () async {
              // var l = await whatsapp.messagesText(
              //   to: phoneNumber,
              //   message: "Ya estoy aqui",
              //   previewUrl: true,
              // );
              // debugPrint('$l');
              //share();
              // http.Response response = await sendMessage("Ya estoy aqui");
              // if (response.statusCode == 200) {
              //   debugPrint('${jsonDecode(response.body)}');
              // } else {
              //   throw Exception('Failed to sendMessage');
              // }
              //525628087623

              // openWhatsapp(
              //     context: context, text: "Hola", number: "525628087623");
              usersRepository.userDeliveries().listen((event) async {
                for (var item in event) {
                  debugPrint('${item.token}');
                  await FcmPostMessage()
                      .sendOrdenado("UserName", "10.00", item.token ?? '');
                }
              });
            }, // button pressed
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(Icons.notifications_rounded), // icon
                Text("Timbre"), // text
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onGetStartedClicked(BuildContext context) {
    SharedPreferences.getInstance().then((prefs) {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
        if (prefs.getString("user") != null) {
          UserModel user =
              UserModel.fromJson(jsonDecode(prefs.getString("user") ?? ""));
          return StreamBuilder<List<UsersEntity>>(
              stream: usersRepository.userUid(user.id),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                      child: CircularProgressIndicator(
                    backgroundColor: Colors.black26,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.black, //<-- SEE HERE
                    ),
                  ));
                } else if (snapshot.connectionState == ConnectionState.active ||
                    snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return const Text('Error');
                  } else if (snapshot.hasData) {
                    if (snapshot.data?.first.type.compareTo('user') == 0) {
                      return const DashboardScreen(index: 0);
                    } else {
                      return const DashboardDeliveryScreen(index: 0);
                    }
                  } else {
                    return const Text('Empty data');
                  }
                } else {
                  return Text('State: ${snapshot.connectionState}');
                }
              });
        } else {
          return const LoginScreen();
        }
      }));
    });
  }
}
