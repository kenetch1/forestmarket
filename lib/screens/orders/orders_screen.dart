import 'package:flutter/material.dart';
import 'package:foreststore/common/user_info.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/helpers/column_with_seprator.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/repositories/cart/cart_repository.dart';
import 'package:foreststore/repositories/cart/cart_repository_implement.dart';
import 'package:foreststore/widgets/order_item_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrdersScreen extends StatefulWidget {
  const OrdersScreen({super.key});

  @override
  State<OrdersScreen> createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  final CartRepository cartRepository = CartRepositoryImplement();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: FutureBuilder(
              future: SharedPreferences.getInstance(),
              builder: (context, AsyncSnapshot<SharedPreferences> snapshot) {
                // Checking if future is resolved or not
                if (snapshot.connectionState == ConnectionState.done) {
                  // If we got an error
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        '${snapshot.error} occurred',
                        style: const TextStyle(fontSize: 18),
                      ),
                    );

                    // if we got our data
                  } else if (snapshot.hasData) {
                    // Extracting data from snapshot object
                    UserModel user = UserInfo.getUserInfo(snapshot.data!);
                    return StreamBuilder(
                        stream: cartRepository.orders(user.id),
                        builder: ((context, snapshot1) {
                          if (snapshot1.connectionState ==
                                  ConnectionState.done ||
                              snapshot1.connectionState ==
                                  ConnectionState.active) {
                            // If we got an error
                            if (snapshot1.hasError) {
                              return Center(
                                child: Text(
                                  '${snapshot.error} occurred',
                                  style: const TextStyle(fontSize: 18),
                                ),
                              );

                              // if we got our data
                            } else if (snapshot1.hasData) {
                              // Extracting data from snapshot object
                              var item = snapshot1.data!;
                              return Column(
                                children: [
                                  const SizedBox(
                                    height: 25,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!.order_title,
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Column(
                                    children: getChildrenWithSeperator(
                                      addToLastChild: false,
                                      widgets: item.map((e) {
                                        return Container(
                                          height: 180,
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 25,
                                          ),
                                          width: double.maxFinite,
                                          child: OrderItemWidget(
                                            cartRepository: cartRepository,
                                            item: e,
                                            onRemoveOrder: (response) {
                                              if (response) {
                                                setState(() {});
                                              }
                                            },
                                          ),
                                        );
                                      }).toList(),
                                      seperator: const Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 10,
                                        ),
                                        child: Divider(
                                          thickness: 1,
                                        ),
                                      ),
                                    ),
                                  ),
                                  // const Divider(
                                  //   thickness: 1,
                                  // ),
                                  // getCheckoutButton(context)
                                ],
                              );
                            }
                          }

                          // Displaying LoadingSpinner to indicate waiting state
                          return const Center(
                              child: CircularProgressIndicator(
                            backgroundColor: Colors.black26,
                            valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.black, //<-- SEE HERE
                            ),
                          ));
                        }));
                  }
                }

                // Displaying LoadingSpinner to indicate waiting state
                return const Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.black26,
                  valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.black, //<-- SEE HERE
                  ),
                ));
              }),
        ),
      ),
    );
  }

  Widget getCheckoutButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      child: AppButton(
        label: "Go To Check Out",
        fontWeight: FontWeight.w600,
        padding: const EdgeInsets.symmetric(vertical: 30),
        trailingWidget: getButtonPriceWidget(),
        onPressed: () {
          //showBottomSheet(context);
        },
      ),
    );
  }

  Widget getButtonPriceWidget() {
    return Container(
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        color: const Color(0xff489E67),
        borderRadius: BorderRadius.circular(4),
      ),
      child: const Text(
        "\$12.96",
        style: TextStyle(fontWeight: FontWeight.w600),
      ),
    );
  }
}
