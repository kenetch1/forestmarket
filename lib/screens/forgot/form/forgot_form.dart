import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/screens/forgot/bloc/forgot_bloc.dart';
import 'package:foreststore/screens/forgot/form/email_input.dart';
import 'package:foreststore/screens/forgot/form/loading_indicator.dart';
import 'package:foreststore/styles/colors.dart';

class ForgotForm extends StatelessWidget {
  const ForgotForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocListener<ForgotBloc, ForgotState>(
        listener: (context, state) {
          final authFailureOrSuccess = state.authFailureOrSuccess;

          if (authFailureOrSuccess != null) {
            authFailureOrSuccess.fold(
              (failure) {
                // Do something to handle failure. For example, show a
                // snackbar saying "Invalid Email and Password Combination" or
                // "Server Error" depending on the failure.

                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      failure.when<String>(
                        invalidEmailAndPasswordCombination: () =>
                            'Invaid email and password combination!',
                        serverError: () => 'Server Error!',
                      ),
                    ),
                  ),
                );
              },
              (success) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Registro exitoso...'),
                    backgroundColor: AppColors.primaryColor,
                  ),
                );
              },
            );
          }
        },
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            child: Column(
              children: [
                SvgPicture.asset("assets/icons/app_icon_color.svg"),
                const SizedBox(
                  height: 5,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: EmailInput(),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: AppButton(
                    label: "Recuperar",
                    fontWeight: FontWeight.w600,
                    padding: const EdgeInsets.symmetric(vertical: 25),
                    onPressed: () {
                      context.read<ForgotBloc>().add(
                            const ForgotEvent.forgotSubmitted(),
                          );
                    },
                  ),
                ),
                const SizedBox(height: 15),
                const LoadingIndicator(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
