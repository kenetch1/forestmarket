part of 'forgot_bloc.dart';

@freezed
class ForgotEvent with _$ForgotEvent {
  //const factory ForgotEvent.started() = _Started;
  const factory ForgotEvent.emailChanged(String emailString) = _EmailChanged;

  const factory ForgotEvent.forgotSubmitted() = _ForgotSubmitted;
}
