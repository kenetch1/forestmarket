import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/common/user_info.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/common_widget/app_text.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/repositories/cart/cart_repository.dart';
import 'package:foreststore/repositories/cart/cart_repository_implement.dart';
import 'package:foreststore/screens/delivery_method_dialog.dart';
import 'package:foreststore/screens/order_accepted_screen.dart';
import 'package:foreststore/screens/order_failed_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckoutBottomSheet extends StatefulWidget {
  final double sumTotal;
  const CheckoutBottomSheet({super.key, required this.sumTotal});

  @override
  _CheckoutBottomSheetState createState() => _CheckoutBottomSheetState();
}

class _CheckoutBottomSheetState extends State<CheckoutBottomSheet> {
  final CartRepository repository = CartRepositoryImplement();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 30,
      ),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
      child: Wrap(
        children: <Widget>[
          Row(
            children: [
              const AppText(
                text: "Ordenar",
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
              const Spacer(),
              GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    Icons.close,
                    size: 25,
                  ))
            ],
          ),
          const SizedBox(
            height: 45,
          ),
          getDivider(),
          checkoutRow(1, "Entrega", trailingText: "Personal"),
          // getDivider(),
          // checkoutRow("Payment", trailingWidget: const Icon(Icons.payment)),
          // getDivider(),
          // checkoutRow("Promo Code", trailingText: "Pick Discount"),
          getDivider(),
          checkoutRow(2, "Costo Total",
              trailingText: "\$${widget.sumTotal.toStringAsFixed(2)}"),
          getDivider(),
          const SizedBox(
            height: 30,
          ),
          termsAndConditionsAgreement(context),
          Container(
            margin: const EdgeInsets.only(
              top: 25,
            ),
            child: AppButton(
              label: "Realizar pedido",
              fontWeight: FontWeight.w600,
              padding: const EdgeInsets.symmetric(
                vertical: 25,
              ),
              onPressed: () {
                onPlaceOrderClicked(widget.sumTotal.toStringAsFixed(2));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget getDivider() {
    return const Divider(
      thickness: 1,
      color: Color(0xFFE2E2E2),
    );
  }

  Widget termsAndConditionsAgreement(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: 'Al realizar un pedido, acepta nuestros',
          style: TextStyle(
            color: const Color(0xFF7C7C7C),
            fontSize: 14,
            fontFamily: Theme.of(context).textTheme.bodyText1?.fontFamily,
            fontWeight: FontWeight.w600,
          ),
          children: const [
            TextSpan(
                text: " Terminos",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
            TextSpan(text: " y"),
            TextSpan(
                text: " Condiciones",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ]),
    );
  }

  Widget checkoutRow(int index, String label,
      {String? trailingText, Widget? trailingWidget}) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 15,
      ),
      child: Row(
        children: [
          AppText(
            text: label,
            fontSize: 18,
            color: const Color(0xFF7C7C7C),
            fontWeight: FontWeight.w600,
          ),
          const Spacer(),
          (trailingText == null)
              ? ((trailingWidget != null) ? trailingWidget : Container())
              : AppText(
                  text: trailingText,
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
          const SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {
              switch (index) {
                case 1:
                  Navigator.pop(context);
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return const DeliveryMethodDialogue();
                      });
                  break;
                default:
              }
            },
            child: const Icon(
              Icons.arrow_forward_ios,
              size: 20,
            ),
          )
        ],
      ),
    );
  }

  void onPlaceOrderClicked(String total) {
    SharedPreferences.getInstance().then((prefs) {
      UserModel user = UserInfo.getUserInfo(prefs);
      OrdersEntity entity = OrdersEntity(
          "",
          user.id,
          "Order",
          "1",
          "En tienda",
          "1",
          "Ordenado",
          total,
          "30 min",
          "true",
          "1",
          "true",
          DateTool.getCurrentDateHour(),
          DateTool.getCurrentDateHour(),
          "Gerardo");

      repository.sendOrder(entity).then((value) {
        //Borrar todo el carrito
        repository.removeItem().then((value) {
          Navigator.pop(context);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return const OrderAcceptedScreen();
              });
        }).onError((error, stackTrace) {
          Navigator.pop(context);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return const OrderFailedDialogue();
              });
        });
      }).onError((error, stackTrace) {
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return const OrderFailedDialogue();
            });
      });
    });
  }
}
