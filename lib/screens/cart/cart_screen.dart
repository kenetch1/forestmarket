import 'package:flutter/material.dart';
import 'package:foreststore/common_widget/app_button.dart';
import 'package:foreststore/database/database_services.dart';
import 'package:foreststore/entities/cart/cart_entity.dart';
import 'package:foreststore/helpers/column_with_seprator.dart';
import 'package:foreststore/widgets/chart_item_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'checkout_bottom_sheet.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  final DatabaseService _databaseService = DatabaseService();
  double sumTotal = 0.0;
  List<CartEntity> cartItems = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: FutureBuilder(
              future: _databaseService.carts(),
              // .then((value) => value
              // .map((e) => GroceryItem(
              //     id: e.productId,
              //     name: e.name,
              //     description: e.description,
              //     imagePath: e.imagePath,
              //     price: e.price,
              //     quantity: e.quantity))
              // .toList()),
              builder: (BuildContext context,
                  AsyncSnapshot<List<CartEntity>> snapshot) {
                if (snapshot.hasData) {
                  cartItems = snapshot.data!;
                  sumTotal = cartItems.fold(
                      0,
                      (sum, element) =>
                          sum.toDouble() + (element.price * element.quantity));
                  return Column(
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      Text(
                        AppLocalizations.of(context)!.cart_title,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: getChildrenWithSeperator(
                          addToLastChild: false,
                          widgets: cartItems.map((e) {
                            return Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 25,
                              ),
                              width: double.maxFinite,
                              child: ChartItemWidget(
                                  item: e,
                                  onAmountChanged: (newAmount) {
                                    CartEntity newItem =
                                        newAmount as CartEntity;
                                    _databaseService
                                        .updateCart(newItem)
                                        .then((value) {
                                      setState(() {});
                                    });

                                    // int index = cartItems.indexWhere(
                                    //     ((item) => item.id == newItem.id));
                                    // cartItems[index] = newItem;
                                    // sumTotal = cartItems.fold(
                                    //     0,
                                    //     (sum, element) =>
                                    //         sum.toDouble() +
                                    //         (element.price *
                                    //             element.quantity));
                                  },
                                  onRemoveItem: (newItem) {
                                    CartEntity item = newItem as CartEntity;
                                    _databaseService
                                        .deleteCart(item.id ?? 0)
                                        .then((value) {
                                      setState(() {});
                                    });
                                  }),
                            );
                          }).toList(),
                          seperator: const Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 25,
                            ),
                            child: Divider(
                              thickness: 1,
                            ),
                          ),
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                      ),
                      cartItems.isNotEmpty
                          ? getCheckoutButton(context)
                          : const Center(
                              child: Text('Canasta vacia.',
                                  style: TextStyle(fontSize: 20)))
                    ],
                  );
                } else {
                  return const Center(child: Text('No hay registros'));
                }
              }),
        ),
      ),
    );
  }

  Widget getCheckoutButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      child: AppButton(
        label: AppLocalizations.of(context)!.cart_check,
        fontWeight: FontWeight.w600,
        padding: const EdgeInsets.symmetric(vertical: 30),
        trailingWidget: getButtonPriceWidget(),
        onPressed: () {
          showBottomSheet(context);
        },
      ),
    );
  }

  Widget getButtonPriceWidget() {
    return Container(
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        color: const Color(0xff489E67),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        '\$${sumTotal.toStringAsFixed(2)}',
        style: const TextStyle(fontWeight: FontWeight.w600),
      ),
    );
  }

  void showBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return CheckoutBottomSheet(sumTotal: sumTotal);
        });
  }
}
