// Assume the document contains:
// {
//   name: "Frank",
//   favorites: { food: "Pizza", color: "Blue", subject: "recess" }
//   age: 12
// }
//
// To update age and favorite color:
// db.collection("users").document("frank")
//         .update(mapOf(
//                 "age" to 13,
//                 "favorites.color" to "Red"
//         ))

// final CollectionReference collectionReference = Firestore.instance.collection("profiles");
// collectionReference.document("profile")
//                 .updateData({"isLogged": true})
//                 .whenComplete(() async {
//               print("Completed");
//             }).catchError((e) => print(e));

import 'package:foreststore/entities/orders/orders_entity.dart';

abstract class CartRepository {
  Future<void> uploadingOrders(OrdersEntity entity);
  Stream<List<OrdersEntity>> orders(String userId);
  Future<void> sendOrder(OrdersEntity entity);
  Future<void> removeItem();
  Future<bool> removerOrder(String orderId);
}
