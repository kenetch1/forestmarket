import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/repositories/cart/cart_repository.dart';
import 'package:foreststore/repositories/orders/firebase_orders_repository.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';

class CartRepositoryImplement implements CartRepository {
  final OrdersRepository ordersRepository = FirebaseOrdersRepository();

  @override
  Stream<List<OrdersEntity>> orders(String userId) {
    return ordersRepository.orders(userId);
  }

  @override
  Future<void> uploadingOrders(OrdersEntity entity) {
    //Proceso de Guardar los productos si falla borrar la transaccion
    return ordersRepository.uploadingOrders(entity);
  }

  @override
  Future<void> sendOrder(OrdersEntity entity) {
    return ordersRepository.transactionOrder(entity);
  }

  @override
  Future<void> removeItem() {
    return ordersRepository.removeItem();
  }

  @override
  Future<bool> removerOrder(String orderId) {
    return ordersRepository.removeOrder(orderId);
  }
}
