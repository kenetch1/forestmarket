import 'package:foreststore/models/explore/categories_model.dart';

abstract class CategoriesRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<Categories>>> categories();
  Stream<List<Categories>> home();
  Stream<List<Categories>> explore();
}
