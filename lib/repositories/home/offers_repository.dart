import 'package:foreststore/models/home/offers_model.dart';

abstract class OffersRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<Offers>>> offers();
  Stream<List<Offers>> offer();
}
