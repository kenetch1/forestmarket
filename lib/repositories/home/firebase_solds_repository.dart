import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/home/solds_entity.dart';
import 'package:foreststore/models/home/solds_model.dart';
import 'package:foreststore/repositories/home/solds_repository.dart';

class FirebaseSoldsRepository implements SoldsRepository {
  final soldsCollection = FirebaseFirestore.instance.collection("Solds");

  @override
  Future<Stream<List<Solds>>> solds() async {
    return soldsCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => Solds.fromEntity(SoldsEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<Solds>> sold() {
    return soldsCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => Solds.fromEntity(SoldsEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}
