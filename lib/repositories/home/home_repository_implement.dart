import 'dart:async';

import 'package:foreststore/models/home/exclusives_model.dart';
import 'package:foreststore/models/home/news_model.dart';
import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/models/product/products_model.dart';
import 'package:foreststore/repositories/home/exclusives_repository.dart';
import 'package:foreststore/repositories/home/firebase_exclusives_repository.dart';
import 'package:foreststore/repositories/home/firebase_news_repository.dart';
import 'package:foreststore/repositories/home/firebase_offers_repository.dart';
import 'package:foreststore/repositories/home/firebase_products_repository.dart';
import 'package:foreststore/repositories/home/firebase_solds_repository.dart';
import 'package:foreststore/repositories/home/home_repository.dart';
import 'package:foreststore/repositories/home/news_repository.dart';
import 'package:foreststore/repositories/home/offers_repository.dart';
import 'package:foreststore/repositories/home/products_repository.dart';
import 'package:foreststore/repositories/home/solds_repository.dart';

// https://betterprogramming.pub/async-programming-in-flutter-with-streams-c949f74c9cf9
class HomeRepositoryImplement implements HomeRepository {
  OffersRepository offerRepository = FirebaseOffersRepository();
  ProductsRepository productsRepository = FirebaseProductsRepository();
  ExclusivesRepository exclusivesRepository = FirebaseExclusivesRepository();
  NewsRepository newsRepository = FirebaseNewsRepository();
  SoldsRepository soldsRepository = FirebaseSoldsRepository();

  @override
  Future<Stream<List<GroceryItem>>> exclusives() async {
    Completer<Stream<List<GroceryItem>>> c =
        Completer<Stream<List<GroceryItem>>>();
    Stream<List<GroceryItem>> item = Stream<List<GroceryItem>>.value([]);
    List<GroceryItem> groceries = [];
    //Recuperar Exclusives

    await exclusivesRepository.exclusives().then((value) {
      //StreamSubscription<List<Exclusives>> subscription;
      value.listen((data) async {
        for (var element in data) {
          await productsRepository
              .productsId(element.subCategoryId, element.productId)
              .then((value) {
            groceries.add(GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
          });
        }
        item = Stream.value(groceries);
        c.complete(item);
      }, onDone: () {
        item = Stream.value(groceries);
        c.complete(item);
      }, onError: (error) {
        item = Stream.value(groceries);
      });
    });
    var stateToReturn = await c.future;
    return Future.value(stateToReturn);

    // await productsRepository.productsById(
    //     ['GkvQiIrZIN5XqXXnHumg'], ['DrP2033B5ptfWtmK6Y3o']).then((value) {
    //   item = value.map((i) {
    //     return i.map((e) {
    //       return GroceryItem(
    //           id: e.id,
    //           name: e.product,
    //           description: e.description,
    //           price: double.parse(e.regularPrice),
    //           imagePath: "assets/images/grocery_images/banana.png");
    //     }).toList();
    //   });
    // });
    // return Future.value(item);
    // Future<Stream<List<GroceryItem>>> stream = await productsRepository
    //     .productsById('GkvQiIrZIN5XqXXnHumg', ['DrP2033B5ptfWtmK6Y3o']).then(
    //         (value) {
    //    value.map((i) {
    //     return i.map((e) {
    //       return GroceryItem(
    //           id: e.id,
    //           name: e.product,
    //           description: "7pcs, Priceg",
    //           price: double.parse(e.regularPrice),
    //           imagePath: "assets/images/grocery_images/banana.png");
    //     }).toList();
    //   });
    // });
    //return Future.value(stream);

    // stream.listen((x) {
    //   for (var element in x) {
    //     item.add(GroceryItem(
    //         id: element.id,
    //         name: element.product,
    //         description: "7pcs, Priceg",
    //         price: 4.99,
    //         imagePath: "assets/images/grocery_images/banana.png"));
    //   }
    //   return item;
    // });
  }

  @override
  Stream<List<Future<GroceryItem>>> exclusive() {
    return exclusivesRepository.exclusive().map((event) {
      var r = event.map((e) {
        var l = productsRepository
            .productsId(e.subCategoryId, e.productId)
            .then((value) => GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
        return l;
      });
      return r.toList();
    });
  }

  @override
  Future<Stream<List<Offers>>> offers() {
    return offerRepository.offers();
  }

  @override
  Stream<List<Offers>> offer() {
    return offerRepository.offer();
  }

  @override
  Future<Stream<List<GroceryItem>>> news() async {
    Completer<Stream<List<GroceryItem>>> c =
        Completer<Stream<List<GroceryItem>>>();
    Stream<List<GroceryItem>> item = Stream<List<GroceryItem>>.value([]);
    List<GroceryItem> groceries = [];
    //Recuperar Exclusives

    await newsRepository.news().then((value) {
      //StreamSubscription<List<Exclusives>> subscription;
      value.listen((data) async {
        for (var element in data) {
          await productsRepository
              .productsId(element.subCategoryId, element.productId)
              .then((value) {
            groceries.add(GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
          });
        }
        item = item = Stream<List<GroceryItem>>.value(groceries);
      }, onDone: () {
        item = Stream.value(groceries);
        c.complete(item);
      }, onError: (error) {
        item = Stream.value(groceries);
      });
    });
    var stateToReturn = await c.future;
    return Future.value(stateToReturn);
  }

  @override
  Stream<List<Future<GroceryItem>>> newp() {
    return newsRepository.newp().map((event) {
      var r = event.map((e) {
        var l = productsRepository
            .productsId(e.subCategoryId, e.productId)
            .then((value) => GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
        return l;
      });
      return r.toList();
    });
  }

  @override
  Future<Stream<List<GroceryItem>>> solds() async {
    Completer<Stream<List<GroceryItem>>> c =
        Completer<Stream<List<GroceryItem>>>();
    Stream<List<GroceryItem>> item = Stream<List<GroceryItem>>.value([]);
    List<GroceryItem> groceries = [];
    //Recuperar Exclusives

    await soldsRepository.solds().then((value) {
      //StreamSubscription<List<Exclusives>> subscription;
      value.listen((data) async {
        for (var element in data) {
          await productsRepository
              .productsId(element.subCategoryId, element.productId)
              .then((value) {
            groceries.add(GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
          });
        }
        item = Stream.value(groceries);
        c.complete(item);
      }, onDone: () {
        item = Stream.value(groceries);
        c.complete(item);
      }, onError: (error) {
        item = Stream.value(groceries);
      });
    });
    var stateToReturn = await c.future;
    return Future.value(stateToReturn);
  }

  @override
  Stream<List<Future<GroceryItem>>> sold() {
    return soldsRepository.sold().map((event) {
      var r = event.map((e) {
        var l = productsRepository
            .productsId(e.subCategoryId, e.productId)
            .then((value) => GroceryItem(
                id: value.id,
                name: value.product,
                description: value.description,
                price: double.parse(value.regularPrice),
                offer: double.parse(value.offer),
                discount: double.parse(value.discount),
                imagePath: "assets/images/grocery_images/banana.png"));
        return l;
      });
      return r.toList();
    });
  }
}
