import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/models/home/offers_model.dart';

abstract class HomeRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<Offers>>> offers();
  Stream<List<Offers>> offer();
  Future<Stream<List<GroceryItem>>> exclusives();
  Stream<List<Future<GroceryItem>>> exclusive();
  Future<Stream<List<GroceryItem>>> solds();
  Stream<List<Future<GroceryItem>>> sold();
  Future<Stream<List<GroceryItem>>> news();
  Stream<List<Future<GroceryItem>>> newp();
}
