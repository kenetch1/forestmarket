import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/product/products_entity.dart';
import 'package:foreststore/models/product/products_model.dart';
import 'package:foreststore/repositories/home/products_repository.dart';

class FirebaseProductsRepository implements ProductsRepository {
  final productsCollection = FirebaseFirestore.instance.collection("Products");

  // @override
  // Future<Stream<List<Offers>>> offers() async {
  //   return offersCollection.snapshots().map((snapshot) {
  //     return snapshot.docs
  //         .map((doc) => Offers.fromEntity(OffersEntity.fromSnapshot(doc)))
  //         .toList();
  //   });
  // }

//   collection(“products”).where(
// “category”,
// “array-contains-any”,
// [“Appliances”,”Electronics”]
// )

  @override
  Stream<List<Products>> products(String categoryId) {
    return productsCollection
        .doc(categoryId)
        .collection("Product")
        .where('instock', isNotEqualTo: "0")
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => Products.fromEntity(ProductsEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<Stream<List<Products>>> productsById(
      List<String> foreignId, List<String> productoId) {
    return Future.value(productsCollection
        .doc('GkvQiIrZIN5XqXXnHumg')
        .collection("Product")
        // .where('foreignId', whereIn: foreignId)
        .where('productId', whereIn: productoId)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => Products.fromEntity(ProductsEntity.fromSnapshot(doc)))
          .toList();
    }));
  }

  @override
  Future<Products> productsId(String categoryId, String productoId) async {
    return productsCollection
        .doc(categoryId)
        .collection("Product")
        .doc(productoId)
        .get()
        .then((doc) => Products.fromEntity(ProductsEntity.fromSnapshot(doc)));
    // Stream<DocumentSnapshot> source = productsCollection
    //     .doc(categoryId)
    //     .collection("Product")
    //     .doc(productoId)
    //     .snapshots();
    // await for (var snapshot in source) {
    //   final Products returnVal =
    //       Products.fromEntity(ProductsEntity.fromSnapshot(snapshot));
    //   //yield returnVal;
    //   Future.value(returnVal);
    // }
  }

  @override
  Stream<List<Products>> productsForProduct(String categoryId, String product) {
    return productsCollection
        .doc(categoryId)
        .collection("Product")
        .orderBy('description')
        .startAt([product])
        .endAt(['$product\uf8ff'])
        .snapshots()
        .map((snapshot) {
          return snapshot.docs
              .map((doc) =>
                  Products.fromEntity(ProductsEntity.fromSnapshot(doc)))
              .toList();
        });
  }

  @override
  Stream<List<Products>> forProduct(String product) {
    var productsForCategory =
        FirebaseFirestore.instance.collectionGroup("Product");
    return productsForCategory
        .orderBy('description')
        .startAt([product])
        .endAt(['$product\uf8ff'])
        .snapshots()
        .map((snapshot) {
          return snapshot.docs
              .map((doc) =>
                  Products.fromEntity(ProductsEntity.fromSnapshot(doc)))
              .toList();
        });
  }
}
