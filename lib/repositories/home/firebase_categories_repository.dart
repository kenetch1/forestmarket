import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/explore/categories_entity.dart';
import 'package:foreststore/models/explore/categories_model.dart';
import 'package:foreststore/repositories/home/categories_repository.dart';

class FirebaseCategoriesRepository implements CategoriesRepository {
  final categoriesCollection =
      FirebaseFirestore.instance.collection("Categories");

  @override
  Future<Stream<List<Categories>>> categories() async {
    return categoriesCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              Categories.fromEntity(CategoriesEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<Categories>> explore() {
    return categoriesCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              Categories.fromEntity(CategoriesEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<Categories>> home() {
    return categoriesCollection
        .where("home_page", isEqualTo: "true")
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              Categories.fromEntity(CategoriesEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}
