import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/explore/subcategories_entity.dart';
import 'package:foreststore/models/explore/subcategories_model.dart';
import 'package:foreststore/repositories/home/subcategories_repository.dart';

class FirebaseSubCategoriesRepository implements SubCategoriesRepository {
  final categoriesCollection =
      FirebaseFirestore.instance.collection("SubCategories");

  @override
  Stream<List<SubCategories>> subcategories(String categoryId) {
    return categoriesCollection
        .doc(categoryId)
        .collection('SubCategorie')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              SubCategories.fromEntity(SubCategoriesEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  // @override
  // Stream<List<Categories>> home() {
  //   return categoriesCollection
  //       .where("home_page", isEqualTo: "true")
  //       .snapshots()
  //       .map((snapshot) {
  //     return snapshot.docs
  //         .map((doc) =>
  //             Categories.fromEntity(CategoriesEntity.fromSnapshot(doc)))
  //         .toList();
  //   });
  // }
}
