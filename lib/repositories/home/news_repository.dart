import 'package:foreststore/models/home/news_model.dart';

abstract class NewsRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<News>>> news();
  Stream<List<News>> newp();
}
