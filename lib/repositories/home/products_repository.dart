import 'package:foreststore/models/product/products_model.dart';

abstract class ProductsRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Stream<List<Products>> products(String categoryId);
  Future<Stream<List<Products>>> productsById(
      List<String> foreignId, List<String> productoId);
  Future<Products> productsId(String categoryId, String productoId);
  Stream<List<Products>> productsForProduct(String categoryId, String product);
  Stream<List<Products>> forProduct(String product);
}
