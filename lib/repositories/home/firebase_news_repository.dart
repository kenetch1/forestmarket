import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/home/news_entity.dart';
import 'package:foreststore/models/home/news_model.dart';
import 'package:foreststore/repositories/home/news_repository.dart';

class FirebaseNewsRepository implements NewsRepository {
  final newsCollection = FirebaseFirestore.instance.collection("News");

  @override
  Future<Stream<List<News>>> news() async {
    return newsCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => News.fromEntity(NewsEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<News>> newp() {
    return newsCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => News.fromEntity(NewsEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}
