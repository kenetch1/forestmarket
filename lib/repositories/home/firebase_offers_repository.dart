import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/home/offers_entity.dart';
import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/repositories/home/offers_repository.dart';

class FirebaseOffersRepository implements OffersRepository {
  final offersCollection = FirebaseFirestore.instance.collection("Offers");

  // @override
  // Future<Stream<List<Offers>>> offers() async {
  //   return offersCollection.snapshots().map((snapshot) {
  //     return snapshot.docs
  //         .map((doc) => Offers.fromEntity(OffersEntity.fromSnapshot(doc)))
  //         .toList();
  //   });
  // }

  @override
  Future<Stream<List<Offers>>> offers() async {
    return offersCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => Offers.fromEntity(OffersEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<Offers>> offer() {
    return offersCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) => Offers.fromEntity(OffersEntity.fromSnapshot(doc)))
          .toList();
    });
  }
}
