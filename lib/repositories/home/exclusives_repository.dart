import 'package:foreststore/models/home/exclusives_model.dart';

abstract class ExclusivesRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<Exclusives>>> exclusives();
  Stream<List<Exclusives>> exclusive();
  Future<List<String>> productId();
}
