import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/home/exclusives_entity.dart';
import 'package:foreststore/models/home/exclusives_model.dart';
import 'package:foreststore/repositories/home/exclusives_repository.dart';
//https://levelup.gitconnected.com/how-to-use-firebase-cloud-firestore-with-a-flutter-app-2110da689e08

class FirebaseExclusivesRepository implements ExclusivesRepository {
  final exclusivesCollection =
      FirebaseFirestore.instance.collection("Exclusives");

  @override
  Future<Stream<List<Exclusives>>> exclusives() async {
    return exclusivesCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              Exclusives.fromEntity(ExclusivesEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Stream<List<Exclusives>> exclusive() {
    return exclusivesCollection.snapshots().map((snapshot) {
      return snapshot.docs
          .map((doc) =>
              Exclusives.fromEntity(ExclusivesEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<List<String>> productId() async {
    return (await exclusivesCollection.get())
        .docs
        .map((item) => item.id)
        .toList();

    // final querySnapshot = await exclusivesCollection.get();
    // final products = querySnapshot.docs.map((e) {
    //   // Now here is where the magic happens.
    //   // We transform the data in to Product object.
    //   // final model = Product.fromJson(e.data());
    //   // // Setting the id value of the product object.
    //   // model.id = e.id;
    //   return e.id;
    // }).toList();
    // return products;
  }
}
