import 'package:foreststore/models/explore/subcategories_model.dart';

abstract class SubCategoriesRepository {
  Stream<List<SubCategories>> subcategories(String categoryId);
}
