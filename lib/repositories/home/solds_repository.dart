import 'package:foreststore/models/home/solds_model.dart';

abstract class SoldsRepository {
  //Stream<List<Home>> home();
  //Future<Stream<List<Offers>>> offers();
  Future<Stream<List<Solds>>> solds();
  Stream<List<Solds>> sold();
}
