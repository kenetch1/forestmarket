import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/models/explore/categories_model.dart';
import 'package:foreststore/models/grocery_item.dart';

abstract class ExploreRepository {
  Stream<List<CategoryItem>> home();
  Stream<List<CategoryItem>> categories();
  Stream<List<CategoryItem>> subcategories(String categoryId);
  Stream<List<GroceryItem>> products(String subcategoryId);
  Stream<List<GroceryItem>> productsForProduct(
      String subcategoryId, String product);
  Stream<List<GroceryItem>> forProduct(String product);
}
