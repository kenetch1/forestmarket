import 'package:foreststore/models/category_item.dart';
import 'package:foreststore/models/grocery_item.dart';
import 'package:foreststore/repositories/explore/explore_repository.dart';
import 'package:foreststore/repositories/home/categories_repository.dart';
import 'package:foreststore/repositories/home/firebase_categories_repository.dart';
import 'package:foreststore/repositories/home/firebase_products_repository.dart';
import 'package:foreststore/repositories/home/firebase_subcategories_repository.dart';
import 'package:foreststore/repositories/home/products_repository.dart';
import 'package:foreststore/repositories/home/subcategories_repository.dart';

class ExploreRepositoryImplement implements ExploreRepository {
  CategoriesRepository categoriesRepository = FirebaseCategoriesRepository();
  SubCategoriesRepository subCategoriesRepository =
      FirebaseSubCategoriesRepository();
  ProductsRepository productsRepository = FirebaseProductsRepository();

  @override
  Stream<List<CategoryItem>> home() {
    return categoriesRepository.home().map((event) => event
        .map((e) => CategoryItem(
            id: e.id,
            name: e.name,
            imagePath: "assets/images/categories_images/fruit.png"))
        .toList());
  }

  @override
  Stream<List<CategoryItem>> categories() {
    return categoriesRepository.explore().map((event) => event
        .map((e) => CategoryItem(
            id: e.id,
            name: e.name,
            imagePath: "assets/images/categories_images/fruit.png"))
        .toList());
  }

  @override
  Stream<List<CategoryItem>> subcategories(String categoryId) {
    return subCategoriesRepository.subcategories(categoryId).map((event) =>
        event
            .map((e) => CategoryItem(
                id: e.id,
                name: e.name,
                imagePath: "assets/images/categories_images/fruit.png"))
            .toList());
  }

  @override
  Stream<List<GroceryItem>> products(String subcategoryId) {
    return productsRepository.products(subcategoryId).map((event) => event
        .map((e) => GroceryItem(
            id: e.id,
            name: e.product,
            description: e.description,
            price: double.parse(e.regularPrice),
            offer: double.parse(e.offer),
            discount: double.parse(e.discount),
            imagePath: "assets/images/grocery_images/banana.png"))
        .toList());
  }

  @override
  Stream<List<GroceryItem>> productsForProduct(
      String subcategoryId, String product) {
    return productsRepository.productsForProduct(subcategoryId, product).map(
        (event) => event
            .map((e) => GroceryItem(
                id: e.id,
                name: e.product,
                description: e.description,
                price: double.parse(e.regularPrice),
                offer: double.parse(e.offer),
                discount: double.parse(e.discount),
                imagePath: "assets/images/grocery_images/banana.png"))
            .toList());
  }

  @override
  Stream<List<GroceryItem>> forProduct(String product) {
    return productsRepository.forProduct(product).map((event) => event
        .map((e) => GroceryItem(
            id: e.id,
            name: e.product,
            description: e.description,
            price: double.parse(e.regularPrice),
            offer: double.parse(e.offer),
            discount: double.parse(e.discount),
            imagePath: "assets/images/grocery_images/banana.png"))
        .toList());
  }
}
