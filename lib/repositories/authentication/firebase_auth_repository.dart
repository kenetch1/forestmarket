import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/common/tools/date_tool.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/models/authentication/auth_error.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/repositories/authentication/auth_repository.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:foreststore/repositories/users/firebase_users_repository.dart';
import 'package:foreststore/repositories/users/users_repository.dart';

class FirebaseAuthService implements AuthRepository {
  final usersCollection = FirebaseFirestore.instance.collection("Users");
  final UsersRepository usersRepository = FirebaseUsersRepository();
  final _firebaseMessaging = FirebaseMessaging.instance;
  FirebaseAuthService({
    required auth.FirebaseAuth authService,
  }) : _firebaseAuth = authService;

  final auth.FirebaseAuth _firebaseAuth;

  Future<UserModel> _mapCreateFirebaseUser(auth.User? user, bool add) async {
    if (user == null) {
      return UserModel.empty();
    }

    final token = await _firebaseMessaging.getToken();

    var splittedName = ['Name ', 'LastName'];
    if (user.displayName != null) {
      splittedName = user.displayName!.split(' ');
    }

    UsersEntity usersEntity = UsersEntity(
        id: 0,
        code: user.uid,
        username: 'userName',
        phoneNumber: '5555555555',
        email: user.email ?? '',
        image: user.photoURL ?? '',
        uid: user.uid,
        active: 'true',
        createAt: DateTool.getCurrentDate(),
        updateAt: DateTool.getCurrentDate(),
        user: 'app',
        type: 'user',
        token: token);

    await addUsers(usersEntity);

    final map = <String, dynamic>{
      'id': user.uid,
      'firstName': splittedName.first,
      'lastName': splittedName.last,
      'email': user.email ?? '',
      'emailVerified': user.emailVerified,
      'imageUrl': user.photoURL ?? '',
      'isAnonymous': user.isAnonymous,
      'age': 0,
      'phoneNumber': '',
      'address': '',
      'token': token
    };
    return UserModel.fromJson(map);
  }

  Future<UserModel> _mapFirebaseUser(auth.User? user, bool add) async {
    if (user == null) {
      return UserModel.empty();
    }

    var splittedName = ['Name ', 'LastName'];
    if (user.displayName != null) {
      splittedName = user.displayName!.split(' ');
    }

    Completer<List<UsersEntity>> complete = Completer();
    usersRepository.userUid(user.uid).listen((event) {
      complete.complete(event);
    });

    var response = await complete.future;

    final map = <String, dynamic>{
      'id': user.uid,
      'firstName': splittedName.first,
      'lastName': splittedName.last,
      'email': user.email ?? '',
      'emailVerified': user.emailVerified,
      'imageUrl': user.photoURL ?? '',
      'isAnonymous': user.isAnonymous,
      'age': 0,
      'phoneNumber': '',
      'address': '',
      'token': response.first.token
    };
    return UserModel.fromJson(map);
  }

  @override
  Future<UserModel> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    try {
      final userCredential = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      return _mapFirebaseUser(userCredential.user!, false);
    } on auth.FirebaseAuthException catch (e) {
      throw _determineError(e);
    }
  }

  @override
  Future<UserModel> createUserWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      return _mapCreateFirebaseUser(_firebaseAuth.currentUser!, true);
    } on auth.FirebaseAuthException catch (e) {
      throw _determineError(e);
    }
  }

  AuthError _determineError(auth.FirebaseAuthException exception) {
    switch (exception.code) {
      case 'invalid-email':
        return AuthError.invalidEmail;
      case 'user-disabled':
        return AuthError.userDisabled;
      case 'user-not-found':
        return AuthError.userNotFound;
      case 'wrong-password':
        return AuthError.wrongPassword;
      case 'email-already-in-use':
      case 'account-exists-with-different-credential':
        return AuthError.emailAlreadyInUse;
      case 'invalid-credential':
        return AuthError.invalidCredential;
      case 'operation-not-allowed':
        return AuthError.operationNotAllowed;
      case 'weak-password':
        return AuthError.weakPassword;
      case 'ERROR_MISSING_GOOGLE_AUTH_TOKEN':
      default:
        return AuthError.error;
    }
  }

  /**
   *  Seccion registro de user 
   */

  Future addUsers(UsersEntity user) async {
    try {
      await usersCollection.add(user.toFirestoreMap());
      return true;
    } catch (e) {
      return e.toString();
    }
  }
}
