import 'package:foreststore/models/authentication/user_model.dart';

abstract class AuthRepository {
  Future<UserModel> signInWithEmailAndPassword({
    required String email,
    required String password,
  });

  Future<UserModel> createUserWithEmailAndPassword({
    required String email,
    required String password,
  });
}
