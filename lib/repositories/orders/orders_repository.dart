// Assume the document contains:
// {
//   name: "Frank",
//   favorites: { food: "Pizza", color: "Blue", subject: "recess" }
//   age: 12
// }
//
// To update age and favorite color:
// db.collection("users").document("frank")
//         .update(mapOf(
//                 "age" to 13,
//                 "favorites.color" to "Red"
//         ))

// final CollectionReference collectionReference = Firestore.instance.collection("profiles");
// collectionReference.document("profile")
//                 .updateData({"isLogged": true})
//                 .whenComplete(() async {
//               print("Completed");
//             }).catchError((e) => print(e));

import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/entities/orders/ordersproducts_entity.dart';

abstract class OrdersRepository {
  Future<void> uploadingOrders(OrdersEntity entity);
  Stream<List<OrdersEntity>> orders(String userId);
  Future<void> transactionOrder(OrdersEntity entity);
  Future<void> removeItem();
  Future<bool> removeOrder(String orderId);
  Future<Set<UsersEntity>> orderByUser();
  Stream<List<OrdersProductsEntity>> orderProduct(String orderId);
  Future<bool> updateState(
      UsersEntity client, OrdersEntity order, String status, String message);
  Future<void> updateInStock(List<OrdersProductsEntity> orderProduct);
}
