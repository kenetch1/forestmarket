import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:foreststore/common/user_info.dart';
import 'package:foreststore/database/database_services.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/entities/cart/cart_entity.dart';
import 'package:foreststore/entities/orders/orders_entity.dart';
import 'package:foreststore/entities/orders/ordersproducts_entity.dart';
import 'package:foreststore/entities/product/products_entity.dart';
import 'package:foreststore/models/authentication/user_model.dart';
import 'package:foreststore/models/product/products_model.dart';
import 'package:foreststore/repositories/orders/orders_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FirebaseOrdersRepository implements OrdersRepository {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final ordersCollection = FirebaseFirestore.instance.collection("Orders");
  final ordersProductsCollection =
      FirebaseFirestore.instance.collection("OrdersProducts");
  final DatabaseService _databaseService = DatabaseService();
  final ordersCollectionGroup =
      FirebaseFirestore.instance.collectionGroup("Order");
  final userCollection = FirebaseFirestore.instance.collection("Users");
  final productsGroup = FirebaseFirestore.instance.collectionGroup("Product");

  @override
  Future<void> uploadingOrders(OrdersEntity entity) async {
    ordersCollection
        .doc(entity.foreignId)
        .collection(entity.orderId)
        .add(entity.toJson())
        .then((value) async {
      await value.update({'${entity.orderId}Id': value.id});
    });
  }

  @override
  Future<Set<UsersEntity>> orderByUser() async {
    List<UsersEntity> lstUsers = [];
    var orders = await ordersCollectionGroup
        .where("orderStatusId", isNotEqualTo: "3")
        .get();
    //debugPrint('${orders.docs.length}');
    for (var order in orders.docs) {
      var users = await userCollection
          .where('uid', isEqualTo: '${order['foreignId']}')
          .get();
      //debugPrint('${users.docs.length}');
      for (var user in users.docs) {
        lstUsers.add(UsersEntity.fromSnapshot(user));
      }
    }
    return lstUsers.toSet();
  }

  @override
  Stream<List<OrdersEntity>> orders(String userId) {
    return ordersCollection
        .doc(userId)
        .collection("Order")
        .where("orderStatusId", isNotEqualTo: "3")
        .orderBy('orderStatusId')
        .orderBy('create_at')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => OrdersEntity.fromSnapshot(doc))
          .toList();
    });
  }

  @override
  Stream<List<OrdersProductsEntity>> orderProduct(String orderId) {
    //List<Products> lstUsers = [];
    return ordersProductsCollection
        .doc(orderId)
        .collection('OrdersProduct')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => OrdersProductsEntity.fromSnapshot(doc))
          .toList();
    });
    // debugPrint('${orders.docs.length}');
    // for (var order in orders.docs) {
    //   debugPrint('${order['productId']}');
    //   var product = await productsGroup
    //       .where('productId', isEqualTo: '${order['productId']}')
    //       .get();
    //   //debugPrint('${users.docs.length}');
    //   for (var user in product.docs) {
    //     lstUsers.add(Products.fromEntity(ProductsEntity.fromSnapshot(user)));
    //   }
    // }
    // return lstUsers;
  }

  @override
  Future<void> transactionOrder(OrdersEntity entity) async {
    await _firestore.runTransaction((transaction) async {
      // DocumentSnapshot acc1snap = await transaction.get(accountsRef);
      // transaction.set(accountsRef, entity.toJson());

      DocumentReference orderRef = _firestore
          .collection("Orders")
          .doc(entity.foreignId)
          .collection(entity.orderId)
          .doc();
      //DocumentSnapshot snapshot = await transaction.get(postRef);
      //int likesCount = snapshot.data['likes'];
      transaction.set(orderRef, entity.toJson());
      debugPrint(orderRef.id);
      //DocumentSnapshot snapshot = await transaction.get(postRef);
      //transaction.update(postRef, {'${entity.orderId}Id': snapshot.id});
      DocumentReference orderRefUpd = _firestore
          .collection("Orders")
          .doc(entity.foreignId)
          .collection(entity.orderId)
          .doc(orderRef.id);
      transaction.update(
          orderRefUpd, {'${entity.orderId.toLowerCase()}Id': orderRef.id});

      WriteBatch writeBatch = _firestore.batch();
// set data

      List<CartEntity> value = await _databaseService.carts();
      for (var item in value) {
        DocumentReference orderProdRef = _firestore
            .collection("OrdersProducts")
            .doc(orderRef.id)
            .collection("OrdersProduct")
            .doc();
        writeBatch.set(orderProdRef, {
          'productId': item.productId,
          'foreignId': orderRef.id,
          "product": item.name,
          'description': item.description,
          'discount': item.discount.toString(),
          'offer': item.offer.toString(),
          'quantity': item.quantity.toString(),
          'regularPrice': item.price.toString()
        });
      }

      // unless commit is called, nothing happens.
      writeBatch.commit();
    });
  }

  @override
  Future<void> removeItem() {
    return _databaseService.deleteAllCart();
  }

  @override
  Future<bool> removeOrder(String orderId) async {
    SharedPreferences value = await SharedPreferences.getInstance();
    UserModel user = UserInfo.getUserInfo(value);
    bool updated = false;
    try {
      await _firestore.runTransaction((transaction) async {
        try {
          WriteBatch writeBatch = _firestore.batch();
          QuerySnapshot<Map<String, dynamic>> order =
              await ordersCollection.doc(user.id).collection('Order').get();
          for (var doc in order.docs) {
            OrdersEntity ordersEntity = OrdersEntity.fromSnapshot(doc);
            QuerySnapshot<Map<String, dynamic>> orderProd =
                await ordersProductsCollection
                    .doc(ordersEntity.orderId)
                    .collection('OrdersProduct')
                    .get();
            for (var doc2 in orderProd.docs) {
              //debugPrint(doc2.id);
              writeBatch.delete(ordersProductsCollection
                  .doc(ordersEntity.orderId)
                  .collection('OrdersProduct')
                  .doc(doc2.id));
            }
          }
          var laRef =
              ordersCollection.doc(user.id).collection('Order').doc(orderId);
          writeBatch.delete(laRef);
          writeBatch.commit();
          updated = true;
        } catch (exception) {
          updated = false;
        }
      });
    } catch (exception) {
      updated = false;
    }
    return updated;
  }

  @override
  Future<bool> updateState(
      UsersEntity client, OrdersEntity order, String status, String message) {
    return ordersCollection
        .doc(client.uid)
        .collection('Order')
        .doc(order.id)
        .update({"orderStatusId": status, "orderStatus": message}).then(
            (value) => true,
            onError: (e) => false);
  }

  @override
  Future<void> updateInStock(List<OrdersProductsEntity> orderProduct) async {
    for (var item in orderProduct) {
      var querySnapshot = await productsGroup
          .where('productId', isEqualTo: item.productId)
          .get();
      for (var product in querySnapshot.docs) {
        //debugPrint(product.data()['instock']);
        int op1 = int.parse(product.data()['instock']);
        int op2 = int.parse(item.quantity);
        int resta = op1 - op2;
        product.reference.update({"instock": resta.toString()});
        //debugPrint('$resta');
      }
    }
  }
}


//Apuntes
    // var l =  ordersCollectionGroup
    //     .where("orderStatusId", isNotEqualTo: "4")
    //     .snapshots()
    //     .map((snapshot) {
    //   return snapshot.docs.map((doc) {
    //     return userCollection
    //         .where('uid', isEqualTo: '${doc['foreignId']}')
    //         .snapshots()
    //         .map((event) {
    //       return event.docs.map((doc1) => '${doc1['email']}').toList();
    //     });
    //   }).toList();
    // });

    // });

    // return userCollection
    //     .where('uid', isEqualTo: '2fGb5rFMOROqPZStwRge2sb1SW32')
    //     .snapshots()
    //     .map((event) {
    //   return event.docs.map((doc1) => '${doc1['email']}').toList();
    // });

    // ordersCollectionGroup
    //     .where("orderStatusId", isNotEqualTo: "4")
    //     .get()
    //     .then((querySnapshot) {
    //   // Do something with these reviews!
    //   for (var item in querySnapshot.docs) {
    //     debugPrint('${item['foreignId']} ');
    //   }
    // });


    // await _firestore.runTransaction((transaction) async {
    //   WriteBatch writeBatch = _firestore.batch();
    //   var laRef =
    //       ordersCollection.doc(user.id).collection('Order').doc(orderId);
    //   writeBatch.delete(laRef);
    //   writeBatch.commit();
    // });

    // return true;

    //  SharedPreferences.getInstance().then((value) async {
    //   UserModel user = UserInfo.getUserInfo(value);
    //    await _firestore.runTransaction((transaction) async {
    //     WriteBatch writeBatch = _firestore.batch();
    //     ordersCollection
    //         .doc(user.id)
    //         .collection('Order')
    //         .snapshots()
    //         .map((snapshot) {
    //       for (var doc in snapshot.docs) {
    //         OrdersEntity ordersEntity = OrdersEntity.fromSnapshot(doc);
    //         ordersProductsCollection
    //             .doc(ordersEntity.orderId)
    //             .collection('OrdersProduct')
    //             .snapshots()
    //             .map((snapshot) {
    //           for (var doc in snapshot.docs) {
    //             writeBatch.delete(ordersProductsCollection
    //                 .doc(ordersEntity.orderId)
    //                 .collection('OrdersProduct')
    //                 .doc(doc.id));
    //           }
    //         });
    //       }
    //       var laRef =
    //           ordersCollection.doc(user.id).collection('Order').doc(orderId);
    //       writeBatch.delete(laRef);
    //       writeBatch.commit();
    //       return true;
    //     });

    //     // return ordersCollection
    //     //     .doc(user.id)
    //     //     .collection('Order')
    //     //     .doc(orderId)
    //     //     .delete()
    //     //     .then(
    //     //       (doc) => true,
    //     //       onError: (e) => false,
    //     //     );
    //   });
    // });