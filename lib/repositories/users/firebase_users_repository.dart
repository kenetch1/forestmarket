import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foreststore/entities/account/users_entity.dart';
import 'package:foreststore/repositories/users/users_repository.dart';

class FirebaseUsersRepository implements UsersRepository {
  final usersCollection = FirebaseFirestore.instance.collection("Users");

  @override
  Stream<List<UsersEntity>> users() {
    return usersCollection.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) => UsersEntity.fromSnapshot(doc)).toList();
    });
  }

  @override
  Stream<List<UsersEntity>> userUid(String uid) {
    return usersCollection
        .where('uid', isEqualTo: uid)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => UsersEntity.fromSnapshot(doc)).toList();
    });
  }

  @override
  Stream<List<UsersEntity>> userDeliveries() {
    return usersCollection
        .where('type', isEqualTo: 'delivery')
        .where('working', isEqualTo: '1')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => UsersEntity.fromSnapshot(doc)).toList();
    });
  }

  @override
  Future<bool> updateUser(UsersEntity user) {
    return usersCollection
        .doc(user.uid)
        .update(user.toFirestoreMap())
        .then((value) => true, onError: (e) => false);
  }

  @override
  Future<bool> addUser(UsersEntity user) {
    return usersCollection
        .add(user.toFirestoreMap())
        .then((value) => true, onError: (e) => false);
  }
}
