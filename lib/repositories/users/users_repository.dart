import 'package:foreststore/entities/account/users_entity.dart';

abstract class UsersRepository {
  Stream<List<UsersEntity>> users();
  Stream<List<UsersEntity>> userUid(String uid);
  Stream<List<UsersEntity>> userDeliveries();
  Future<bool> updateUser(UsersEntity user);
  Future<bool> addUser(UsersEntity user);
}
