import 'package:flutter/material.dart';

class AppText extends StatelessWidget {
  final String text;
  final double fontSize;
  final FontWeight fontWeight;
  final Color color;
  final TextAlign textAlign;
  const AppText(
      {super.key,
      required this.text,
      required this.fontSize,
      required this.fontWeight,
      this.color = Colors.black,
      this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: color,
      ),
    );
  }
}

// class AppText extends StatelessWidget {
//   final String text;
//   final double fontSize;
//   final FontWeight fontWeight;
//   final Color color;
//   final TextAlign textAlign;

//   const AppText({
//     required Key key,
//     required this.text,
//     this.fontSize = 18,
//     this.fontWeight = FontWeight.normal,
//     this.color = Colors.black,
//     required this.textAlign,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Text(
//       text,
//       textAlign: textAlign == null ? null : TextAlign.center,
//       style: TextStyle(
//         fontSize: fontSize,
//         fontWeight: fontWeight,
//         color: color,
//       ),
//     );
//   }
// }
