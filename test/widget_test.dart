// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:foreststore/main.dart';

import 'package:foreststore/models/home/offers_model.dart';
import 'package:foreststore/repositories/home/firebase_offers_repository.dart';
import 'package:foreststore/repositories/home/offers_repository.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  OffersRepository repository;

  setUp(() {
    repository = FirebaseOffersRepository();
  });
  test(
      "If no data is stored in the repository, return an Experience with zero in it.",
      () {});
  test("If data is stored in the repository, return that data.", () {});
  test(
      "If I store data in the repository, I should then be able to retrieve that data.",
      () {
    // repository = FirebaseOffersRepository();
    // Stream<List<Offers>> stream = repository.offers();
    // stream.forEach((offer) {
    //   debugPrint('${offer.length}');
    // });
  });
}
